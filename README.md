#code\_essence -- exploring function keywords
- - -

## Table of Contents
1. Introduction
2. Dependencies
3. Installation
4. Usage
5. Development
6. APPENDIX A: MASM32 and wine

- - -

## 1. Introduction 
How much information is needed to uniquely identify a function?

To identify a function we use a set of keywords, analagous to keyword-based search
used by most code and general-purpose search engines.

What is a keyword?

Current definition, keyword = {lexeme, type, count}. 

For more information see the paper notes in our paper repository.

## 2. Dependencies

### To develop

#### Ubuntu

* eclipse (3.7.2)
* openjdk-7-jdk

#### Mac OS 10.6 (You's laptop)

* eclipse
* openjdk-6-jdk

#### To run

TODO

## 3. Installation

### Developing

#### Import Project into Eclipse
1. Start eclipse
2. File --> Import...
3. General --> Existing Projects into Workspace
4. Next
5. Select root directory...
6. Browse to /path/to/code\_essence/dev/CodeEssence
7. Finish

### Running

TODO

### Installation Problems
This project requires java 7 (=1.7).  It is specified in the .project file.

#### Ubuntu
Ensure that you are using openjdk-7-jdk.
1. Window -> Preference -> Java --> Compiler Compliance Level = 1.7

In my Ubuntu 12.04 system, Eclipse automatically uses Sun's Java 6 JRE.
You may get the message: "The selected JRE does not support the current 
compiler compliance level of 1.7"

To select the appropriate JRE:
1. Click on "Search..."
2. Browse to /usr/lib/jvm/
3. Select java-7-openjdk-amd64
4. Check the java-7-openjdk-amd64 jre


Eclipse will warn you if java 7 (=1.7) is not configured properly.

#### Mac OS
Ensure that you are using at least java 6.

Set your workspace and project Compiler Compliance Level appropriately.

## 4. Usage

TODO

## 5. Development
* Author:	Dong Qiu, You Zhou, [Martin Velez](http://www.martinvelez.com), Earl Barr, Zhendong Su
* Copyright: 	Copyright (C) 2012 
* License: 	[GPL](http://www.gnu.org/copyleft/gpl.html)

### Source 
We are privatley hosting this repository.
	
	argo.cs.ucdavis.edu:code_essence/dev

### Issues
Provide feedback, get help, request features, and report bugs here:

	TODO: URL 


