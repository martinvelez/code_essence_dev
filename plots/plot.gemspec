Gem::Specification.new do |s|
	s.name = 'plot'
	s.version = '0.0.0'
	s.date = '2012-10-04'
	s.summary = 'Plots generator'
	s.description = 'Plot generates the plots for '
	s.authors = ['Dong Qiu, You Zhou, Martin Velez, Earl Barr, Zhendong Su']
	s.email = 'mvelez999@gmail.com'
	s.files = Dir["{lib,bin,conf,ext}/**/*"] + ["README.rdoc","LICENSE"]
	s.homepage = 'argo.cs.ucdavis.edu:code_essence/dev/plot'
	s.require_paths = ["bin","lib"]
	s.add_dependency('rake','>=0.9.0')
	s.executables << 'plot'
end
