package cn.edu.seu.ist.main;

import org.apache.log4j.Logger;

import cn.edu.seu.ist.conf.Log4jConfig;
import cn.edu.seu.ist.conf.ProjConfig;



public class Main {
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		// load log4j configuration file
		Log4jConfig.initLog4j();
		
		if(args.length < 1) {
			logger.error("Error: The path of conf.properties must be specified ... ");
			System.exit(-1);
		}
		String projectPath = null;
		String outputDirPath = null;
		if (args.length == 3){
			projectPath = args[1];
			outputDirPath = args[2];
		} 
		
		// load the project configuration file
		new ProjConfig(args[0].trim()).initProjConfig(projectPath, outputDirPath);
		 
		// perform the extraction
		new Extractor().extract();
		
		// generate the output 
		Reporter.generate();
	}
}
