package cn.edu.seu.ist.main;

import org.apache.log4j.Logger;

import cn.edu.seu.ist.conf.ProjConfig;
import cn.edu.seu.ist.global.Settings;
import cn.edu.seu.ist.util.CollectionUtil;
import cn.edu.seu.ist.util.TimeUtil;
import cn.edu.seu.ist.util.report.CSVOutput;
import cn.edu.seu.ist.util.report.Output;
import cn.edu.seu.ist.util.statistics.GlobalStat;

/**
 * Output all generated data 
 * @author Dong Qiu
 *
 */
public class Reporter {
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(Reporter.class.getName());
	
	public static void generate() {
		generateSummary();
		generateStat();
		generateHistory();
	}
	
	/**
	 * Output global statistical results
	 */
	private static void generateStat() {
		// output the line distribution 
		CSVOutput.outputMap(ProjConfig.outputPath, Settings.LOC_DIS_OUTPUT, 
				GlobalStat.getLocDis());
		
		// output the line distribution (lines only in method body) 
		CSVOutput.outputMap(ProjConfig.outputPath, Settings.LOC_INBODY_DIS_OUTPUT, 
				GlobalStat.getLocInBodyDis());
		
		// output the method feature distribution
		CSVOutput.outputMap(ProjConfig.outputPath, Settings.FEATURE_DIS_OUTPUT, 
				GlobalStat.getFeatureDis());
		
		// output the method feature distribution (feature only in method body)
		CSVOutput.outputMap(ProjConfig.outputPath, Settings.FEATURE_INBODY_DIS_OUTPUT, 
				GlobalStat.getFeatureInBodyDis());
		
		//output the possible duplicated method id
		CSVOutput.<Long>outputCollections(ProjConfig.outputPath, Settings.POSSIBLE_DUPS_OUTPUT, 
				CollectionUtil.filter(GlobalStat.getPossibleDups().values(), 1));
		
		//output the statistics of projects under analysis
		CSVOutput.<String>outputCollections(ProjConfig.outputPath, Settings.PROJECT_STATS_OUTPUT, 
				GlobalStat.getProjectStats());
		
		// output the runtime of parsing each project
		CSVOutput.outputMap(ProjConfig.outputPath, Settings.PROJECT_RUNTIME_OUTPUT, 
				GlobalStat.getRunTimes());
	}
	
	/**
	 * Output summary information of results
	 */
	private static void generateSummary() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("-------------SUMMARY-------------").append("\n");
		buffer.append("Total number of projects under analysis is ")
			.append(GlobalStat.getPid()).append("\n");
		buffer.append("Total runtime of the whole process ")
			.append(TimeUtil.formatIntoHHMMSS(GlobalStat.getTotalRuntime())).append("\n");
		buffer.append("Total number of methods is ").append(GlobalStat.getMid()).append("\n");
		Output.outputString(ProjConfig.log_output_path, Settings.SUMMARY_OUTPUT, buffer.toString());
	}
	
	/**
	 * Output history information
	 */
	private static void generateHistory() {
		// record the names of projects that have been parsed by feature extractor
		CSVOutput.outputCollection(ProjConfig.log_output_path, Settings.FINISHED_OUTPUT, 
				GlobalStat.getFinishedProjs());
		
		// record the names of projects that have not been parsed by feature extractor
		CSVOutput.outputCollection(ProjConfig.log_output_path, Settings.UNFINISHED_OUTPUT, 
				GlobalStat.getUnFinishedProjs());
	}
}
