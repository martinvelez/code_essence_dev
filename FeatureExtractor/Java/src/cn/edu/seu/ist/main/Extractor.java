package cn.edu.seu.ist.main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import cn.edu.seu.ist.conf.ProjConfig;
import cn.edu.seu.ist.global.CurrentFilePaths;
import cn.edu.seu.ist.global.Settings;
import cn.edu.seu.ist.model.project.Project;
import cn.edu.seu.ist.parser.AbstractParser;
import cn.edu.seu.ist.parser.ProjectParser;
import cn.edu.seu.ist.util.CollectionUtil;
import cn.edu.seu.ist.util.PathUtil;
import cn.edu.seu.ist.util.report.AbstractReporter;
import cn.edu.seu.ist.util.statistics.GlobalStat;

public class Extractor {
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(Extractor.class.getName());

	/* current feature parser under analysis */
	private AbstractParser currentParser;
	
	/**
	 * Perform feature extraction
	 */
	public void extract() {
		analyze();
	}
	
	/**
	 * process of analyzing projects involving both corpus and projects
	 */
	private void analyze() {
		logger.info("analyze()");
		logger.info("ProjConfig.corpusPaths = " + ProjConfig.corpusPaths);
		logger.info("ProjConfig.projectPaths = " + ProjConfig.projectPaths);
		// Analyze all projects in corpus
		if(ProjConfig.corpusPaths.size() != 0) {
			for(String corpusPath : ProjConfig.corpusPaths) {
				analyzeCorpus(new File(corpusPath));
			}
		}
		if(ProjConfig.projectPaths.size() != 0) {
			for(String projPath : ProjConfig.projectPaths) {
				File projFile = new File(projPath);
				if(needAnalysis(projFile.getName())) {
					logger.info("needAnalysis(\"" + projFile.getName() + "\") == true");
					analyzeProject(projFile, ProjConfig.outputPath);
				}
			}
		}
	}

	/**
	 * Analyze all the projects under corpusDir
	 * @param corpusDir
	 */
	private void analyzeCorpus(File corpusDir) {
		
		File ouputCorpusFile = FileUtils.getFile(ProjConfig.outputPath, corpusDir.getName());
		if(!ouputCorpusFile.exists()) {
			ouputCorpusFile.mkdirs();
		}
		for(File subDir : corpusDir.listFiles()) {
			if(subDir.isDirectory()) {
				if(needAnalysis(subDir.getName())) {
					analyzeProject(subDir, ouputCorpusFile.getAbsolutePath());
				}
			}
		}
	}
	
	/**
	 * process of analyzing single project
	 * @param projPath
	 * @param outputPath
	 */
	private void analyzeProject(File projFile, String outputPath) {
		logger.info("analyzeProject(File " + projFile + ", String \"" + outputPath + "\")");
		long currentMid = GlobalStat.getMid();
		GlobalStat.increasePid();
		// set flag show whether the project is parsed correctly
		boolean parseCorrectly = true;
		String projName = projFile.getName();
		// add the jars inside the project into classpathEntries
		List<String> classpathEntries = new ArrayList<String>(ProjConfig.classpaths);
		classpathEntries.addAll(PathUtil.listFilePaths(projFile, new String[]{Settings.JAR_EXTENSION}));
		// get all the java source file paths
		CurrentFilePaths.setFilePaths(PathUtil.listFilePaths(projFile, new String[]{Settings.JAVA_EXTENSION}));
		int numOffiles = CurrentFilePaths.size();
		logger.info("numOffiles = " + numOffiles);
		// prepare the output dir for the project
		File outputDir = PathUtil.prepareDir(outputPath, projName);
		
		long startTime = System.currentTimeMillis();
		// create the feature parser
		
		int parseNumber = -1;
		while(CurrentFilePaths.size() > 0 && parseCorrectly) {
			parseNumber ++;
			this.currentParser = new ProjectParser(projName, new String[]{projFile.getAbsolutePath()}, 
					classpathEntries, CurrentFilePaths.getFilePaths());
			
			CurrentFilePaths.newFilePaths();
			try {
				this.currentParser.parse();
			} catch(Exception e) {
				logger.fatal("Fatal: Can't parse project " + projName, e);
				// recover the project id and method id
				GlobalStat.decreasePid();
				GlobalStat.setMid(currentMid);
				// add the project that is not parsed correctly into the unfinished list
				GlobalStat.addUnfinishedProjs(projName);
				// project does not parse correctly
				parseCorrectly = false;
			}
			
			if(parseCorrectly) {
				logger.info("parseCorrectly == true");
				Project projectInfo = currentParser.getRequestor().getProjectInfo();
				// output the results of feature extractor on project
				outputProject(outputDir.getAbsolutePath(), projectInfo,	ProjConfig.outputFormat);
				// store the statistical information of project
				if(parseNumber > 0) {
					projectInfo.setName(projName + "_" + parseNumber);
				}
				GlobalStat.addProjectStat(projectInfo.projSummaryInfo());
			}
		}
		
		long runtime = System.currentTimeMillis() - startTime;
		
		if(parseCorrectly) {
			// store the analyzed project into the finished list
			GlobalStat.addFinishedProj(projName);
			
			// store the runtime of parsing the project
			GlobalStat.AddRunTime(projName, runtime);
			GlobalStat.updateTotalRuntime(runtime);
			
			// log info
			logger.info("Info: Project " + projName + " has been parsed correctly... ");
			logger.info("Info: The number of Java files is " + numOffiles);
			logger.info("Info: The number of parsing the project is " + (parseNumber+1));
			logger.info("Info: The execution time is  " + runtime/1000.0d + " seconds");
			logger.info("Info: The number of analyzed projects is " + GlobalStat.getPid());
		}
	}

    /**
     * Output results generated by feature parser
     * @param outputPath
     * @param requestor
     * @param format
     */
	private void outputProject(String outputPath, Project project, int format) {
		AbstractReporter reporter = AbstractReporter.prepareClass(ProjConfig.executionMode, 
				new Object[]{outputPath, project, format});
		reporter.report();
	}

	/**
	 * Check whether the project need to analyze
	 * @param projName
	 * @return
	 */
	private boolean needAnalysis(String projName) {
		logger.info("needAnalysis()");
		boolean flag = true;
		if(CollectionUtil.<String>isInList(projName, GlobalStat.getFinishedProjs())) {
			flag = false;
		}
		if(CollectionUtil.<String>isInList(projName, ProjConfig.ignoredProjects)) {
			flag = false;
		}
		if(!flag) {
			logger.info("Project " + projName + " is ignored...");
		}
		return flag;
	}
}