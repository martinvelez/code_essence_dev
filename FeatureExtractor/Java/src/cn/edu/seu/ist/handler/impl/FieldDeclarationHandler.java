package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.FieldDeclaration;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class FieldDeclarationHandler implements NodeHandler {
	
	private FieldDeclaration node;
	
	public FieldDeclarationHandler(FieldDeclaration node) {
		this.node = node;
	}

	/**
	 *  FieldDeclaration:
     *		[Javadoc] { ExtendedModifier } Type VariableDeclarationFragment
     *    	{ , VariableDeclarationFragment } ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, isInBody);
		return features;
	}

}
