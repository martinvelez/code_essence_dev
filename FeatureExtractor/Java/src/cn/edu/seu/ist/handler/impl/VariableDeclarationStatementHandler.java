package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;


public class VariableDeclarationStatementHandler implements NodeHandler {
	
	private VariableDeclarationStatement node;
	
	public VariableDeclarationStatementHandler(VariableDeclarationStatement node) {
		this.node = node;
	}

	/**
	 * Local variable declaration statement AST node type.
	 * VariableDeclarationStatement:
     *		{ ExtendedModifier } Type VariableDeclarationFragment
     *   	{ , VariableDeclarationFragment } ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Type is handler in Type-related handlers
		
		// ExtendedModifiers is handled in ModifierHandler
		
		// add the feature of separator "," if it exists
		if(node.fragments().size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), node.fragments().size() - 1, true);
		}
		
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		return features;
	}
}
