package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.NumberLiteral;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.checker.NumericChecker;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class NumberLiteralHandler implements NodeHandler {

	private NumberLiteral node;
	
	public NumberLiteralHandler(NumberLiteral node) {
		this.node = node;
	}

	/**
	 * Number literal nodes
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of constant number literal
		//features.addLiteralNumerical(node.getToken(), isInBody);
		String numericalValue = node.getToken();
		TokenType type = NumericChecker.getNumericType(numericalValue);
		features.addNumericLiteral(numericalValue, type, isInBody);
		return features;
	}
}