package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.AssertStatement;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class AssertStatementHandler implements NodeHandler {
	
	private AssertStatement node;
	
	public AssertStatementHandler(AssertStatement node) {
		this.node = node;
	}
	
	/**
	 *  AssertStatement:
     *		assert Expression [ : Expression ] ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "assert"
		features.addKeyword(TokenType.ASSERT.toString(), 1, true);
		
		// add the feature of operator ":" if exist in message part of assert statement
		if(node.getMessage() != null) {
			features.addOperator(TokenType.COLON.toString(), 1, true);
		}
		
		// expression part is handled in other handlers
		
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, true);;
		
		return features;
	}

}
