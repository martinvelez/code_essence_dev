package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InfixExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class InfixExpressionHandler implements NodeHandler {
	
	private InfixExpression node;
	
	public InfixExpressionHandler(InfixExpression node) {
		this.node = node;
	}

	/**
	 * InfixExpression:
     *		Expression InfixOperator Expression { InfixOperator Expression }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// count the number of operator
		int numOfOperator = 1;
		// left expression and right expression are handled in other handlers
		// if has extended operands 
		if(node.hasExtendedOperands()) {
			List<Expression> extendedOperands = node.extendedOperands();
			numOfOperator += extendedOperands.size();
			// expression is handled in other handlers
		}
		features.addOperator(node.getOperator().toString(), numOfOperator, isInBody);
		// expressions are handled in other places 
		return features;
	}
}
