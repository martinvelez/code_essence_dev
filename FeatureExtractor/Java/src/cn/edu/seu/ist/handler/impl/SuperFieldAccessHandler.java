package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.SuperFieldAccess;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class SuperFieldAccessHandler implements NodeHandler {
	
	private SuperFieldAccess node;
	
	public SuperFieldAccessHandler(SuperFieldAccess node) {
		this.node = node;
	}

	/**
	 *  SuperFieldAccess:
     *		[ ClassName . ] super . Identifier
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "." if ClassName exists
		if(node.getQualifier() != null) {
			features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
			// ClassName is handled in name-related handlers
		}
		// add the feature of keyword "super" 
		features.addKeyword(TokenType.SUPER.toString(), 1, isInBody);
		// add the feature of separator "."
		features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		// Identifier is handled in name-related handlers
		return features;
	}
}
