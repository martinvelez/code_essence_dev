package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.UnionType;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class UnionTypeHandler implements NodeHandler {

	private UnionType node;
	
	public UnionTypeHandler(UnionType node) {
		this.node = node;
	}

	/**
	 *  UnionType:
     *		Type | Type { | Type }
     *  This kind of node is used inside a catch clause's formal parameter type.
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		int numOfOr = node.types().size() - 1;
		if(numOfOr > 0) {
			// add the feature of operator "|"
			features.addOperator(TokenType.BAR.toString(), numOfOr, isInBody);
		}
		return features;
	}
}
