package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.TypeLiteral;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class TypeLiteralHandler implements NodeHandler {

	private TypeLiteral node;
	
	public TypeLiteralHandler(TypeLiteral node) {
		this.node = node;
	}

	/**
	 *  TypeLiteral:
     *		( Type | void ) . class
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// Type or void are handled in type-related handlers
		// add the feature of separator "."
		features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		// add feature of language keyword "class" 
		features.addKeyword(TokenType.CLASS.toString(), 1, isInBody);
		return features;
	}
}
