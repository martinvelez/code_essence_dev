package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.ParenthesizedExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ParenthesizedExpressionHandler implements NodeHandler {
	
	private ParenthesizedExpression node;
	
	/**
	 * Constructor
	 * @param node
	 */
	public ParenthesizedExpressionHandler(ParenthesizedExpression node) {
		this.node = node;
	}

	/**
	 * ParenthesizedExpression:
     *		( Expression )
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
		features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		return features;
	}
}
