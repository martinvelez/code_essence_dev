package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.PackageDeclaration;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;

public class CompilationUnitHandler implements NodeHandler {
	
	private CompilationUnit node;
	private String packageName;
	private String publicClassName;
	
	
	public CompilationUnitHandler(CompilationUnit node) {
		this.node = node;
		this.packageName = null;
		this.publicClassName = null;
	}

	/**
	 *  CompilationUnit:
     *		[ PackageDeclaration ]
     *   	{ ImportDeclaration }
     *   	{ TypeDeclaration | EnumDeclaration | AnnotationTypeDeclaration | ; }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		PackageDeclaration packageDeclaration = node.getPackage();
		if(packageDeclaration != null) {
			this.packageName = packageDeclaration.getName().getFullyQualifiedName();
		}
		// set the public class name
		List<AbstractTypeDeclaration> abstractTypeDeclarations = node.types();
		for(AbstractTypeDeclaration abstractTypeDeclaration : abstractTypeDeclarations) {
			List<IExtendedModifier> imodifiers = abstractTypeDeclaration.modifiers();
			for(IExtendedModifier imodifier : imodifiers) {
				if(imodifier.isModifier()) {
					Modifier modifier = (Modifier)imodifier;
					if(modifier.isPublic()) {
						this.publicClassName = abstractTypeDeclaration.getName().getIdentifier();
						break;
					}
				}
			}
		} 
		return features;
	}

	/**
	 * Get the package name
	 * @return
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * get the public class name
	 * @return
	 */
	public String getPublicClassName() {
		return publicClassName;
	}
	
}
