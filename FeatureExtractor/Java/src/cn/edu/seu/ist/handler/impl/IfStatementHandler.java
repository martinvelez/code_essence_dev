package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.Statement;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class IfStatementHandler implements NodeHandler {
	
	private IfStatement node;
	
	public IfStatementHandler(IfStatement node) {
		this.node = node;
	}

	/**
	 *  IfStatement:
	 * 		if ( Expression ) Statement [ else Statement]
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "if"
		features.addKeyword(TokenType.IF.toString(), 1, true);
		//add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		// expression and statement after if is handled in other handlers
		// if else statement exist 
		Statement elseStatement = node.getElseStatement();
		if(elseStatement != null) {
			//add feature of language keyword "else"
			features.addKeyword(TokenType.ELSE.toString(), 1, true);
			// else statement is handled in other handlers
		}
		return features;
	}
}
