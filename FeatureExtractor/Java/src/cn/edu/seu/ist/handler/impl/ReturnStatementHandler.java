package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class ReturnStatementHandler implements NodeHandler {

	/**
	 *  ReturnStatement:
     * 		return [ Expression ] ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "return"
		features.addKeyword(TokenType.RETURN.toString(), 1, true);
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		// expression is handled in other handlers
		return features;
	}
}
