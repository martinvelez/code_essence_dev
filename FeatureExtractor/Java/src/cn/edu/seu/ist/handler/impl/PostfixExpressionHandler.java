package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.PostfixExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class PostfixExpressionHandler implements NodeHandler {
	
	private PostfixExpression node;
	
	public PostfixExpressionHandler(PostfixExpression node) {
		this.node = node;
	}

	/**
	 * PostfixExpression:
     *		Expression PostfixOperator
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add operator in PostfixExpression
		features.addOperator(node.getOperator().toString(), 1, isInBody);
		// expression is handled in other handlers
		return features;
	}
}
