package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class CatchClauseHandler implements NodeHandler {
	
	/**
	 * CatchClause : 
	 * 		catch ( FormalParameter ) Block
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "catch"
		features.addKeyword(TokenType.CATCH.toString(), 1, true);
		
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		
		// FormalParameter is handled in singleVariableHandler and simpleNameHandler
		
		// block is handled by other handlers
		return features;
	}
}
