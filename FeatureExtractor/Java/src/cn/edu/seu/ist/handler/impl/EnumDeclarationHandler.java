package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class EnumDeclarationHandler implements NodeHandler {
	
	private EnumDeclaration node;
	
	public EnumDeclarationHandler(EnumDeclaration node) {
		this.node = node;
	}

	/**
	 * EnumDeclaration: 
	 * 		[ Javadoc ] { ExtendedModifier } enum Identifier [
	 * 		implements Type { , Type } ] 
	 * 		{ 
	 * 		[ EnumConstantDeclaration { , EnumConstantDeclaration } ] [ , ] 
	 * 		[ ; { ClassBodyDeclaration | ; } ] }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// modifier is handler in ModifierHandler and Annotation-related handlers
		// add the feature of keyword "enum"
		features.addKeyword(TokenType.ENUM.toString(), 1, true);
		// handle the implements part
		List<Type> interfaces = node.superInterfaceTypes();
		if(interfaces != null) {
			if(interfaces.size() > 0) {
				// add the feature of keyword "implements"
				features.addKeyword(TokenType.IMPLEMENTS.toString(), 1, true);
			}
			if(interfaces.size() > 1) {
				// add the feature of separator ","
				features.addSeparator(TokenType.COMMA.toString(), interfaces.size() - 1, true);
			}
		}
		// EnumConstantDeclaration is handled in EnumConstantDeclarationHandler
		List<EnumConstantDeclaration> enumConstantDeclaration = node.enumConstants();
		if(enumConstantDeclaration != null) {
			if(enumConstantDeclaration.size() > 0) {
				// add the separator of ";"
				features.addSeparator(TokenType.SEMI.toString(), 1, true);
			}
			if(enumConstantDeclaration.size() > 1) {
				// add the feature of separator ","
				features.addSeparator(TokenType.COMMA.toString(), enumConstantDeclaration.size() - 1, true);
			}
		}
		// add the feature of separator "{" and "}" 
		features.addSeparator(TokenType.LBRACE.toString(), 1, true);
		features.addSeparator(TokenType.RBRACE.toString(), 1, true);
		return features;
	}

}
