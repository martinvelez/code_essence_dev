package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.Expression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ArrayInitializerHandler implements NodeHandler {

	private ArrayInitializer node;
	
	public ArrayInitializerHandler(ArrayInitializer node) {
		this.node = node;
	}

	/**
	 *  ArrayInitializer:
     *          { [ Expression { , Expression} [ , ]] }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "{" and "}"
		features.addSeparator(TokenType.LBRACE.toString(), 1, isInBody);
		features.addSeparator(TokenType.RBRACE.toString(), 1, isInBody);
		List<Expression> expressions = node.expressions();
		if(expressions != null && expressions.size() > 1) {
			// add feature of separator ","
			features.addSeparator(TokenType.COMMA.toString(), expressions.size() - 1, isInBody);
		}
		return features;
	}

}
