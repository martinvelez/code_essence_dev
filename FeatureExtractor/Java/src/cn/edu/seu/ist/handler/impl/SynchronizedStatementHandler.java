package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class SynchronizedStatementHandler implements NodeHandler {
	
	/**
	 *  SynchronizedStatement:
     * 		synchronized ( Expression ) Block
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "synchronized"
		features.addKeyword(TokenType.SYNCHRONIZED.toString(), 1, true);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		// expression and body is handled in other handlers
		return features;
	}

}
