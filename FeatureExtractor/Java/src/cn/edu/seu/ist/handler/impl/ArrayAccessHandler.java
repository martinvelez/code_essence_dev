package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.ArrayAccess;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ArrayAccessHandler implements NodeHandler {

	private ArrayAccess node;
	
	public ArrayAccessHandler(ArrayAccess node) {
		this.node = node;
	}

	/**
	 *  ArrayAccess:
     *		Expression [ Expression ]
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "[" and "]"
		features.addSeparator(TokenType.LBRACKET.toString(), 1, isInBody);
		features.addSeparator(TokenType.RBRACKET.toString(), 1, isInBody);
		return features;
	}

}
