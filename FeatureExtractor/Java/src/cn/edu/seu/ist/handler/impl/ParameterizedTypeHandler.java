package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.ParameterizedType;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ParameterizedTypeHandler implements NodeHandler {
	
	private ParameterizedType node;
	
	public ParameterizedTypeHandler(ParameterizedType node) {
		this.node = node;
	}

	/**
	 * ParameterizedType:
     * 		Type < Type { , Type } >
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);		
		// add the feature of operator "<" and ">"
		// Note: operator "<" ">" are considered as separator here
		features.addSeparator(TokenType.LT.toString(), 1, isInBody);
		features.addSeparator(TokenType.GT.toString(), 1, isInBody);
		
		// add the feature of separator "," if number of type arguments is larger than 1
		List<Type> typeArguments = node.typeArguments();
		if(typeArguments != null && typeArguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), typeArguments.size() - 1, isInBody);
		}
		// Type in ParameterizedType is handled iterative
		return features;
	}

}
