package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class SwitchStatementHandler implements NodeHandler {

	/**
	 *  SwitchStatement:
     *		switch ( Expression )
     *		{ { SwitchCase | Statement } } }
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "switch"
		features.addKeyword(TokenType.SWITCH.toString(), 1, true);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		
		// Expression is handled in other handlers
		
		// add the feature of separator "{" and "}"
		features.addSeparator(TokenType.LBRACE.toString(), 1, true);
		features.addSeparator(TokenType.RBRACE.toString(), 1, true);
		
		// SwitchCase is handled in SwitchCaseHandler
		// Statement is handled in other handlers
		return features;
	}

}
