package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.ThisExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ThisExpressionHandler implements NodeHandler {

	private ThisExpression node;
	
	public ThisExpressionHandler(ThisExpression node) {
		this.node = node;
	}

	/**
	 *  ThisExpression:
     *		[ ClassName . ] this	
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		if(node.getQualifier() != null) {
			// ClassName is handled in name-related handlers
			// add the feature of separator "." if it exists
			features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		}
		// add the feature of language keyword "this"
		features.addKeyword(TokenType.THIS.toString(), 1, isInBody);
		return features;
	}
}
