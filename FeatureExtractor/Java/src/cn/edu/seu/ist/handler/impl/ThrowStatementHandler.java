package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class ThrowStatementHandler implements NodeHandler {

	/**
	 * 	 ThrowStatement:
     *		throw Expression ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "throw" 
		features.addKeyword(TokenType.THROW.toString(), 1, true);
		// add the features of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		// expression is handled in other handlers
		return features;
	}
}
