package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class SingleVariableDeclarationHandler implements NodeHandler {

	private SingleVariableDeclaration node;
	
	public SingleVariableDeclarationHandler(SingleVariableDeclaration node) {
		this.node = node;
	}
	
	/**
	 * Single variable declaration nodes are used in a limited number of places, 
	 * including "formal parameter" lists and "catch clauses". They are not used 
	 * for field declarations and regular variable declaration statements
	 * 
	 * SingleVariableDeclaration:
     * 		{ ExtendedModifier } Type [ ... ] Identifier { [] } [ = Expression ]
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		
		// check whether singleVariableDeclaration is in anonymous class
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of modifier
		/*List<IExtendedModifier> modifiers = node.modifiers();
		if(modifiers != null && modifiers.size() > 0) {
			for(IExtendedModifier imodifier : modifiers) {
				if(imodifier.isModifier()) {
					Modifier modifier = (Modifier)imodifier;
					features.addKeyword(modifier.getKeyword().toString(), 1, isInBody);
				}
				
			}
		}*/
		// Type is handled in type-related handlers
		
		// add the feature of variable arity indicator "..." if it exists
		if(node.isVarargs()) {
			features.addSeparator(TokenType.ELLIPSIS.toString(), 1, isInBody);
		} 
		
		// Identifier is handled in name-related handlers
		
		// add the feature of separator "[" and "]" if they exist
		if(node.getExtraDimensions() > 0) {
			features.addSeparator(TokenType.LBRACKET.toString(), node.getExtraDimensions(), isInBody);
			features.addSeparator(TokenType.RBRACKET.toString(), node.getExtraDimensions(), isInBody);
		}
		// add the feature of operator "=" if expression exists
		if(node.getInitializer() != null) {
			features.addOperator(TokenType.EQ.toString(), 1, isInBody);
		}
		return features;
	}
}
