package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class BlockHandler implements NodeHandler {
	
	/**
	 * Block:
     *		{ { Statement } }
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of separator "{" and "}"
		features.addSeparator(TokenType.LBRACE.toString(), 1, true);
		features.addSeparator(TokenType.RBRACE.toString(), 1, true);
		return features;
	}

}
