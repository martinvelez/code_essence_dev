package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.SingleMemberAnnotation;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class SingleMemberAnnotationHandler implements NodeHandler {

	private SingleMemberAnnotation node;

	public SingleMemberAnnotationHandler(SingleMemberAnnotation node) {
		this.node = node;
	}

	/**
	 *  SingleMemberAnnotation:
   	 *		@ TypeName ( Expression  )
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Check whether the annotation is in method signature or method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of operator "@"
		features.addOperator(TokenType.MONKEYS_AT.toString(), 1, isInBody);
		// add the feature of separator "(" and ")" if expression exists
		if(node.getValue() != null) {
			features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
			features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		}
		return features;
	}

}
