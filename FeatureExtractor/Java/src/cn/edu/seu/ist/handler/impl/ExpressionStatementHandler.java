package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class ExpressionStatementHandler implements NodeHandler {

	/**
	 * ExpressionStatement:
     *		StatementExpression ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		return features;
	}

}
