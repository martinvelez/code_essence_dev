package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.ArrayType;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ArrayTypeHandler implements NodeHandler {
	
	private ArrayType node;

	public ArrayTypeHandler(ArrayType node) {
		this.node = node;
	}

	/**
	 *  ArrayType:
     *		Type [ ]
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Check whether the type is in anonymous class
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "[" and "]"
		features.addSeparator(TokenType.LBRACKET.toString(), 1, isInBody);
		features.addSeparator(TokenType.RBRACKET.toString(), 1, isInBody);
		
		// Type in ArrayType is handled iterative
		return features;
	}

}
