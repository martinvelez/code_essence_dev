package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.CharacterLiteral;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class CharacterLiteralHandler implements NodeHandler {
	
	private CharacterLiteral node;
	
	public CharacterLiteralHandler(CharacterLiteral node) {
		this.node = node;
	}

	/**
	 * Character literal nodes
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of constant char literal
		features.addCharLiteral(node.getEscapedValue(), isInBody);
		return features;
	}

}
