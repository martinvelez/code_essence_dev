package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.Expression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class EnumConstantDeclarationHandler implements NodeHandler {
	private EnumConstantDeclaration node;
	
	public EnumConstantDeclarationHandler(EnumConstantDeclaration node) {
		this.node = node;
	}

	/**
	 *  EnumConstantDeclaration:
     *		[ Javadoc ] { ExtendedModifier } Identifier
     * 		[ ( [ Expression { , Expression } ] ) ]
     *    	[ AnonymousClassDeclaration ]
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Modifier is handler in ModifierHandler and Annotation-related handlers
		// Identifier is handler in simple name
		List<Expression> arguments = node.arguments();
		if(arguments != null) {
			if(arguments.size() > 0) {
				// add the feature of separator "(" and ")"
				features.addSeparator(TokenType.LPAREN.toString(), 1, true);
				features.addSeparator(TokenType.RPAREN.toString(), 1, true);
				// Expressions are handled in expression-related handlers
			}
			if(arguments.size() > 1) {
				// add the feature of separator ","
				features.addSeparator(TokenType.COMMA.toString(), arguments.size() - 1, true);
			}
		}
		// Anonymous class is handler in AnonymousClassHandler
		return features;
	}

}
