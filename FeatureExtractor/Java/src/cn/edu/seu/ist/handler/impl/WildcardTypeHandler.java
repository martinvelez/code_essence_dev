package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.WildcardType;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class WildcardTypeHandler implements NodeHandler {
	
	private WildcardType node;
	
	public WildcardTypeHandler(WildcardType node) {
		this.node = node;
	}
	
	/**
	 * WildcardType:
     *		? [ ( extends | super) Type ]
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// check whether the type is in method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of operator "?"
		features.addOperator(TokenType.QUES.toString(), 1, isInBody);
		// if the WildcardType has a bound, check whether is a upper or lower bound
		if(node.getBound() != null) {
			if(node.isUpperBound()) {
				// add feature of language keyword "extends" if exists
				features.addKeyword(TokenType.EXTENDS.toString(), 1, isInBody);
			} else {
				// add feature of language keyword "super" if exists
				features.addKeyword(TokenType.SUPER.toString(), 1, isInBody);
			}
		}
		// Type in WildcardType is handled iteratively
		return features;
	}
}
