package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.MemberValuePair;
import org.eclipse.jdt.core.dom.NormalAnnotation;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class NormalAnnotationHandler implements NodeHandler {
	
	private NormalAnnotation node;
	
	public NormalAnnotationHandler(NormalAnnotation node) {
		this.node = node;
	}
	
	/**
	 * NormalAnnotation:
   	 *	@ TypeName ( [ MemberValuePair { , MemberValuePair } ] )
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Check whether the annotation is in method signature or method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of operator "@"
		features.addOperator(TokenType.MONKEYS_AT.toString(), 1, isInBody);
		// add the feature of separator "(" and ")" if expression exists
		List<MemberValuePair> valuePairs = node.values();
		if(valuePairs != null) {
			features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
			features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
			if(valuePairs.size() > 1) {
				// add the feature of separator ","
				features.addSeparator(TokenType.COMMA.toString(), valuePairs.size() - 1, isInBody);
			}
		}
		return features;
	}

}
