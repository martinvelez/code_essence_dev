package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.ConditionalExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ConditionalExpressionHandler implements NodeHandler {

	private ConditionalExpression node;
	
	public ConditionalExpressionHandler(ConditionalExpression node) {
		this.node = node;
	}

	/** 
	 * ConditionalExpression: 
	 * 		Expression ? Expression : Expression
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of operator "?" and ":" 
		features.addOperator(TokenType.QUES.toString(), 1, isInBody);
		features.addOperator(TokenType.COLON.toString(), 1, isInBody);
		// expression is handled in the other handlers
		return features;
	}
}
