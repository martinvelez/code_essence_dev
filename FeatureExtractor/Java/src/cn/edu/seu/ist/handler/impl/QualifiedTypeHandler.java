package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.QualifiedType;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class QualifiedTypeHandler implements NodeHandler {
	
	private QualifiedType node;
	
	public QualifiedTypeHandler(QualifiedType node) {
		this.node = node;
	}

	/**
	 * QualifiedType:
     * 		Type . SimpleName
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Check whether the type is in anonymous class
		Boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// Type is handled in type-related handlers
		features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		// simpleName is handled in name-related handlers
		return features;
	}
}
