package cn.edu.seu.ist.handler.impl;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.checker.JDKChecker;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class SimpleTypeHandler implements NodeHandler {
	// add the logger
	static Logger logger = Logger.getLogger(SimpleTypeHandler.class.getName());
	
	private SimpleType node;
	
	public SimpleTypeHandler(SimpleType node) {
		this.node = node;
	}

	/**
	 * Type node for a named class type, a named interface type, 
	 * or a type variable. This kind of node is used to convert 
	 * a name (Name) into a type (Type) by wrapping it.
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// check whether the type is in method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		
		String lexeme = null;
		IBinding binding = null;
		
		Name name = node.getName();
		// handle the situation that if simpleType is qualified name
		while(name.isQualifiedName()) {
			QualifiedName qualifiedName = (QualifiedName)name;
			lexeme = qualifiedName.getName().toString();
			try {
				binding = qualifiedName.resolveBinding();
			} catch (Exception e) {
				logger.error(ASTNodeHelper.getExceptionMessage(node), e);
			}
			getFeatureFromBinding(features, lexeme, binding, isInBody);
			name = qualifiedName.getQualifier();
		}
		
		// handle the situation if simpleType is simple name 
		// or the local name in the qualified name
		if(name != null) {
			SimpleName simpleName = (SimpleName)name;
			lexeme = simpleName.getIdentifier();
			try {
				binding = simpleName.resolveBinding();
			} catch (Exception e) {
				logger.error(ASTNodeHelper.getExceptionMessage(node), e);
			}
			getFeatureFromBinding(features, lexeme, binding, isInBody);
		}
		return features;
	}

	/**
	 * Add features from type binding information
	 * @param features
	 * @param lexeme
	 * @param binding
	 * @param isInBody
	 */
	private void getFeatureFromBinding(FeatureSet features, String lexeme, 
			IBinding binding, boolean isInBody) {
		if(binding != null) {
			if(binding.getKind() == IBinding.TYPE) {
				features.addIType(lexeme, getRefinedInfo(binding), isInBody);
			} else if(binding.getKind() == IBinding.VARIABLE) { 
				features.addIVariable(lexeme, getRefinedInfo(binding), isInBody);
			} else if(binding.getKind() == IBinding.PACKAGE) {
				features.addIOthers(lexeme, isInBody);
			} 
		} else {
			features.addIType(lexeme, TokenType.LOCAL_TYPE.toString(), isInBody);
		}
	}

	/**
	 * Get refined information from binding of type or variable
	 * @param binding
	 * @return
	 */
	private String getRefinedInfo(IBinding binding) {
		String refinedInfo = null;
		ITypeBinding typeBinding = null;
		if(binding.getKind() == IBinding.VARIABLE) {
			IVariableBinding varBinding = (IVariableBinding)binding;
			typeBinding = varBinding.getType();
		} else if(binding.getKind() == IBinding.TYPE) {
			typeBinding = (ITypeBinding)binding;
		}
		// Get the refined info from binding
		if(typeBinding.isTypeVariable()) {
			refinedInfo = TokenType.GENERIC_TYPE.toString();
		} else {
			String qName = typeBinding.getErasure().getQualifiedName();
			if(JDKChecker.isFromJDK(qName)) {
				refinedInfo = qName;
			} else {
				refinedInfo = TokenType.LOCAL_TYPE.toString();
			}
		}
		return refinedInfo;
	}
}
