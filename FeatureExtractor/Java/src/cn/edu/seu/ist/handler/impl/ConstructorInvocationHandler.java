package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class ConstructorInvocationHandler implements NodeHandler {

	private ConstructorInvocation node;
	
	public ConstructorInvocationHandler(ConstructorInvocation node) {
		this.node = node;
	}

	/**
	 * 	ConstructorInvocation:
	 *		[ < Type { , Type } > ] this ( [ Expression { , Expression } ] ) ;
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		
		List<Type> typeArguments = node.typeArguments();
		if(typeArguments != null && typeArguments.size() > 1) {
			// add the feature of operator "<" and ">" 
			// Note: operator "<" ">" are considered as separator here
			features.addSeparator(TokenType.LT.toString(), 1, true);
			features.addSeparator(TokenType.GT.toString(), 1, true);
			// add the feature of separator of "," if exists in type arguments
			features.addSeparator(TokenType.COMMA.toString(), typeArguments.size() - 1, true);
			// type arguments are handled in Typer-related handlers
		}
		// add the feature of language keyword "this"
		features.addKeyword(TokenType.THIS.toString(), 1, true);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		
		/* Note: currently we do not translate this() into a API call */
		
		List<Expression> arguments = node.arguments();
		if(arguments != null && arguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), arguments.size() - 1, true);
		}
		// add the feature of separator ";" 
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		return features;
	}
}
