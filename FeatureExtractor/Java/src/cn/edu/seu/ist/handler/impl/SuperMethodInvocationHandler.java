package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.checker.JDKChecker;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;
import cn.edu.seu.ist.visitor.helper.BindingHelper;

public class SuperMethodInvocationHandler implements NodeHandler {
	
	// add the logger
	static Logger logger = Logger.getLogger(SimpleTypeHandler.class.getName());

	private SuperMethodInvocation node;
	
	public SuperMethodInvocationHandler(SuperMethodInvocation node) {
		this.node = node;
	}
	
	/**
	 *  SuperMethodInvocation:
     *		[ ClassName . ] super . [ < Type { , Type } > ] Identifier ( [ Expression { , Expression } ] )
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "." if exist
		if(node.getQualifier() != null) {
			//ClassName is handled in name-related handlers
			features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		}
		// add the feature of language keyword "super"
		features.addKeyword(TokenType.SUPER.toString(), 1, isInBody);
		// add the feature of separator "." after super
		features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		
		List<Type> typeArguments = node.typeArguments();
		if(typeArguments!= null && typeArguments.size() > 1) {
			// add the features of operator "<" and ">"
			// Note: operator "<" ">" are considered as separator here
			features.addSeparator(TokenType.LT.toString(), 1, isInBody);
			features.addSeparator(TokenType.GT.toString(), 1, isInBody);
			// add the feature of separator "," if exists in Type Arguments
			features.addSeparator(TokenType.COMMA.toString(), typeArguments.size() - 1, isInBody);
			// type argument are handled in Type-related handlers
		}
		// add the feature of API call 
		String superMCallLexeme = node.getName().getIdentifier();
		String superMCallRefined = null;
		// generation of fully qualified method name of API call
		IMethodBinding apiCallBinding = null;
		try {
			apiCallBinding = node.resolveMethodBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		
		if(apiCallBinding != null) {
			// append the qualified class name to API call
			String className = null;
			ITypeBinding declaringClass = apiCallBinding.getDeclaringClass();
			if(declaringClass != null) {
				className = declaringClass.getErasure().getQualifiedName();
			} 
			//Check if it is public API call
			StringBuffer qualifedMethod = new StringBuffer();
			if(JDKChecker.isFromJDK(className)) {
				qualifedMethod.append(className).append(TokenType.DOT);
				// append the method arguments
				ITypeBinding[] arguments = apiCallBinding.getTypeArguments();
				if(arguments != null && arguments.length > 0) {
					qualifedMethod.append(TokenType.LT.toString());
					for(ITypeBinding argument : arguments) {
						if(argument != null) {
							qualifedMethod.append(BindingHelper.getRefinedInfo(argument))
							 	.append(TokenType.COMMA);
						}
					}
					qualifedMethod.deleteCharAt(qualifedMethod.length() - 1);
					qualifedMethod.append(TokenType.GT.toString());
				}
				// append the method name
				IMethodBinding methodBinding = apiCallBinding.getMethodDeclaration();
				if(methodBinding != null) {
					qualifedMethod.append(methodBinding.getName());
				} else {
					qualifedMethod.append(node.getName().getIdentifier());
				}
				qualifedMethod.append(TokenType.LPAREN);
				// append the method parameters
				ITypeBinding[] paramsType = apiCallBinding.getParameterTypes();
				if(paramsType!= null && paramsType.length > 0) {
					for(ITypeBinding paramType : paramsType) {
							qualifedMethod.append(BindingHelper.getRefinedInfo(paramType))
								.append(TokenType.COMMA);
					}
					qualifedMethod.deleteCharAt(qualifedMethod.length() - 1);
				}
				qualifedMethod.append(TokenType.RPAREN);
				superMCallRefined = qualifedMethod.toString();
			} else {
				superMCallRefined = TokenType.LOCAL_CALL.toString();
			}
		}
		// add the features of super API call
		features.addIMethodCall(superMCallLexeme, superMCallRefined, isInBody);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
		features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		
		// add feature of separator "," if exist in arguments
		List<Expression> arguments = node.arguments();
		if(arguments != null && arguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), arguments.size() - 1, isInBody);
		}
		return features;
	}
}
