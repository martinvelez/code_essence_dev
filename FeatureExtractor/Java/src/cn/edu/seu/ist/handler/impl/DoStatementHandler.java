package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class DoStatementHandler implements NodeHandler {
	
	/**
	 * 	DoStatement:
     *		do Statement while ( Expression ) ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "do"
		features.addKeyword(TokenType.DO.toString(), 1, true);
		// add the feature of language keyword "while"
		features.addKeyword(TokenType.WHILE.toString(), 1, true);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		// statement and expression are handled in other handlers
		// add the feature of separator ";" 
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		return features;
	}
}
