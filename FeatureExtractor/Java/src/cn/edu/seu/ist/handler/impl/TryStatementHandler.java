package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.TryStatement;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class TryStatementHandler implements NodeHandler {
	
	private TryStatement node;
	
	public TryStatementHandler(TryStatement node) {
		this.node = node;
	}

	/**
	 *  TryStatement:
     *		try Block
     *    	[ { CatchClause } ]
     *    	[ finally Block ]
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "try"
		features.addKeyword(TokenType.TRY.toString(), 1, true);
		// CatchClause is handled in CatchClauseHandler	
		Block finallyBlock = node.getFinally();
		if(finallyBlock != null) {
			// add feature of language keyword "finally"
			features.addKeyword(TokenType.FINALLY.toString(), 1, true);
			// block is handled in other handlers
		}
		return features;
	}
}
