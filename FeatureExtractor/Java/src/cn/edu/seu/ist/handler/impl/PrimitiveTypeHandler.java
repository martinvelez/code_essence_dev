package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.PrimitiveType;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class PrimitiveTypeHandler implements NodeHandler {
	
	private PrimitiveType node;
	
	public PrimitiveTypeHandler(PrimitiveType node) {
		this.node = node;
	}

	/**
	 *  PrimitiveType:
     *		byte
     * 		short
     * 		char
     * 		int
     *		long
     *		float
     *		double
     *		boolean
     *		void
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// check whether the type is in anonymous class
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of language keyword, with refined info for public type
		features.addKeyword(node.getPrimitiveTypeCode().toString(), 1, isInBody);
		return features;
	}

}
