package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.StringLiteral;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class StringLiteralHandler implements NodeHandler {

	private StringLiteral node;
	
	public StringLiteralHandler(StringLiteral node) {
		this.node = node;
	}

	/**
	 * String literal nodes.
	 * In order to distinguish with character, string is stored 
	 * surround with "". For example, hello is stored as "hello"  
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// we do not collect String literal in annotation
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of constant string literal
		features.addStringLiteral(node.getEscapedValue(), isInBody);
		return features;
	}
}
