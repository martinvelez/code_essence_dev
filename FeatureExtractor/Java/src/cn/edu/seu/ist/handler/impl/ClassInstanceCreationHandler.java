package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ClassInstanceCreationHandler implements NodeHandler {

	private ClassInstanceCreation node;
	
	public ClassInstanceCreationHandler(ClassInstanceCreation node) {
		this.node = node;
	}

	/**
	 * ClassInstanceCreation:
     *  	[ Expression . ] new [ < Type { , Type } > ] Type ( [ Expression { , Expression } ] )
     *      [ AnonymousClassDeclaration ]
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "." if exists before new
		if(node.getExpression() != null) {
			features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		}
		// add the feature of language keyword of "new"
		features.addKeyword(TokenType.NEW.toString(), 1, isInBody);
		// add the features of separator "," and operator "<" ">" if exists in type arguments
		List<Type> typeArguments = node.typeArguments();
		if (typeArguments != null && typeArguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), typeArguments.size() - 1, isInBody);
			// Note: operator "<" ">" are considered as separator here
			features.addSeparator(TokenType.LT.toString(), 1, isInBody);
			features.addSeparator(TokenType.GT.toString(), 1, isInBody);
			// Type(s) in type arguments are handled in Type-related handlers
		}
		
		/* NOTE: currently Type is handled in Type-related handlers we do not consider it as an API call here. */
		
		// add the feature of separator "(" and ")" 
		features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
		features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		// add the feature of separator "," if exists in parameters
		List<Expression> arguments = node.arguments();
		if(arguments != null && arguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), arguments.size() - 1, isInBody);
			// parameters are handled by variable-related handlers 
		}
		/* NOTE: if the anonymous class C is used in the definition of method A, 
		 * we regard all features in C as part of features in A */
		return features;
	}
}
