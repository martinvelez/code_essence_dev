package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.InstanceofExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class InstanceofExpressionHandler implements NodeHandler {
	
	private InstanceofExpression node;
	
	public InstanceofExpressionHandler(InstanceofExpression node) {
		this.node = node;
	}

	/**
	 * InstanceofExpression:
     * 		Expression instanceof Type
	 * @param node
	 * @return
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of language keyword "instanceof"
		features.addKeyword(TokenType.INSTANCEOF.toString(), 1, isInBody);
		// type is handled in type-related handlers
		// left expression is handled in other handlers
 		return features;
	}
}
