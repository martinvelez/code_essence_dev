package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.SwitchCase;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class SwitchCaseHandler implements NodeHandler {

	private SwitchCase node;
	
	public SwitchCaseHandler(SwitchCase node) {
		this.node = node;
	}

	/**
	 *  SwitchCase:
     *		case Expression  :
     * 		default :
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		if(node.getExpression() != null) {
			// add the feature of language keyword "case"
			features.addKeyword(TokenType.CASE.toString(), 1, true);
			// add the feature of operator ":" 
			features.addOperator(TokenType.COLON.toString(), 1, true);
		} 
		if(node.isDefault()) {
			// add the feature of language keyword "default"
			features.addKeyword(TokenType.DEFAULT.toString(), 1, true);
			// add the feature of operator ":" 
			features.addOperator(TokenType.COLON.toString(), 1, true);
		}
		// expression after "case" is handled in other handlers
		return features;
	}
}
