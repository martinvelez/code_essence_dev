package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.FieldAccess;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class FieldAccessHandler implements NodeHandler {
	
	private FieldAccess node;
	
	public FieldAccessHandler(FieldAccess node) {
		this.node = node;
	}
	/**
	 * FieldAccess:
     * 		Expression . Identifier
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "."
		features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		// expression is handled in other handlers
		// identifier is handled in name-related handlers
		return features;
	}
}
