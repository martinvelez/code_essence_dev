package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class EnhancedForStatementHandler implements NodeHandler {

	/**
	 * EnhancedForStatement:
     *		for ( FormalParameter : Expression )
     *                  Statement
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		//add feature of language keyword "for"
		features.addKeyword(TokenType.FOR.toString(), 1, true);
		// add the feature of operator ":" 
		features.addOperator(TokenType.COLON.toString(), 1, true);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		// FormalParameter, expression and statement are handled in other handlers
		return features;
	}
}
