package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.MarkerAnnotation;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class MarkerAnnotationHandler implements NodeHandler {
	
	private MarkerAnnotation node;
	
	public MarkerAnnotationHandler(MarkerAnnotation node) {
		this.node = node;
	}

	/**
	 *  MarkerAnnotation:
   	 *		@ TypeName
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Check whether the annotation is in method signature or method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);;
		// add the feature of operator "@"
		features.addOperator(TokenType.MONKEYS_AT.toString(), 1, isInBody);
		return features;
	}

}
