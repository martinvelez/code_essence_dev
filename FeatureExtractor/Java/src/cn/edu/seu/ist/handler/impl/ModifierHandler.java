package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.Modifier;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ModifierHandler implements NodeHandler {

	private Modifier node;
	
	public ModifierHandler(Modifier node) {
		this.node = node;
	}

	/**
	 *  Modifier:
     *		public
     *		protected
     * 		private
     *		static
     *		abstract
     *		final
     *		native
     *		synchronized
     *		transient
     *		volatile
     *		strictfp
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of keyword
		features.addKeyword(node.getKeyword().toString(), 1, isInBody);
		return features;
	}

}
