package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ForStatement;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class ForStatementHandler implements NodeHandler {
	
	private ForStatement node;
	
	public ForStatementHandler(ForStatement node) {
		this.node = node;
	}

	/**
	 * ForStatement:
     *		for ( [ ForInit ] ; [ Expression ] ; [ ForUpdate ] )
     *			Statement
 	 * ForInit:
     *		Expression { , Expression }
 	 * ForUpdate:
     *      Expression { , Expression }
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "for"
		features.addKeyword(TokenType.FOR.toString(), 1, true);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 2, true);
		// add the feature of separator "," if ForInit contains more than one expression
		List<Expression> initializers = node.initializers();
		if(initializers != null && initializers.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), initializers.size() - 1, true);
		}
		// add the features of punctuation "," if ForUpdate contains more than one expression
		List<Expression> updaters = node.updaters();
		if(updaters != null && updaters.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), updaters.size() - 1, true);
		}
		// Expressions in ForInit, ForUpdate and Statement are handled in other handlers
		return features;
	}

}
