package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class VariableDeclarationFragmentHandler implements NodeHandler {

	private VariableDeclarationFragment node;
	
	public VariableDeclarationFragmentHandler(VariableDeclarationFragment node) {
		this.node = node;
	}

	/**
	 *  Variable declaration fragment AST node type, used in field declarations, 
	 *  local variable declarations, and ForStatement initializers. It contrast 
	 *  to SingleVariableDeclaration, fragments are missing the modifiers and the 
	 *  type; these are located in the fragment's parent node.
	 *  
	 *  VariableDeclarationFragment:
     *		Identifier { [] } [ = Expression ]
	 */
	
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// identifier is handled by name-related handlers
		// add the feature of separator "[" and "]" if exist
		int extraDimension = node.getExtraDimensions();
		if(extraDimension > 0) {
			features.addSeparator(TokenType.LBRACKET.toString(), extraDimension, true);
			features.addSeparator(TokenType.RBRACKET.toString(), extraDimension, true);
		}
		// add operator "=" if there is expression 
		if(node.getInitializer() != null) {
			features.addOperator(TokenType.EQ.toString(), 1, true);
			// expression is handled in other handlers
		}
		// collection of identifier is handled in VariableDeclarationStatementHandler 
		return features;
	}
}
