package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeParameter;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class TypeDeclarationHandler implements NodeHandler {
	
	private TypeDeclaration node;
	
	public TypeDeclarationHandler(TypeDeclaration node) {
		this.node = node;
	}

	/**
 	  *	TypeDeclaration:
      *          ClassDeclaration
      *          InterfaceDeclaration
 	  *	ClassDeclaration:
      *		[ Javadoc ] { ExtendedModifier } class Identifier
      *                 [ < TypeParameter { , TypeParameter } > ]
      *                 [ extends Type ]
      *                 [ implements Type { , Type } ]
      *                 { { ClassBodyDeclaration | ; } }
 	  *	InterfaceDeclaration:
      *		[ Javadoc ] { ExtendedModifier } interface Identifier
      *                 [ < TypeParameter { , TypeParameter } > ]
      *                 [ extends Type { , Type } ]
      *                 { { InterfaceBodyDeclaration | ; } }	
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		//Modifier is handled in ModifierHandler
		if(node.isInterface()) {
			// if the node is an interface declaration
			// add the feature of keyword "interface"
			features.addKeyword(TokenType.INTERFACE.toString(), 1, true);
		} else {
			// if the node is a class declaration
			features.addKeyword(TokenType.CLASS.toString(), 1, true);
		}
		// handle the type parameters
		List<TypeParameter> typeParameters = node.typeParameters();
		if(typeParameters != null) {
			if(typeParameters.size() > 0) {
				// add the feature of operator "<" and ">"
				// Note: operator "<" ">" are considered as separator here
				features.addSeparator(TokenType.LT.toString(), 1, true);
				features.addSeparator(TokenType.GT.toString(), 1, true);
			}
			if(typeParameters.size() > 1) {
				// add the feature of separator ","
				features.addSeparator(TokenType.COMMA.toString(), typeParameters.size() - 1, true);
			}
		}
		// handle the extends
		if(node.getSuperclassType() != null) {
			// add the feature of keyword "extends"
			features.addKeyword(TokenType.EXTENDS.toString(), 1, true);
		}
		// handle the implements
		List<Type> interfaces = node.superInterfaceTypes();
		if(interfaces != null) {
			if(interfaces.size() > 0) {
				// add the feature of keyword "extends" or "implements"
				if(node.isInterface()) {
					features.addKeyword(TokenType.EXTENDS.toString(), 1, true);
				} else {
					features.addKeyword(TokenType.IMPLEMENTS.toString(), 1, true);
				}
			}
			if(interfaces.size() > 1) {
				// add the feature of separator ","
				features.addSeparator(TokenType.COMMA.toString(), interfaces.size() - 1, true);
			}
		}
		// add the feature of separator "{" and "}" 
		features.addSeparator(TokenType.LBRACE.toString(), 1, true);
		features.addSeparator(TokenType.RBRACE.toString(), 1, true);
		return features;
	}
}
