package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.PrefixExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class PrefixExpressionHandler implements NodeHandler {
	
	private PrefixExpression node;
	
	public PrefixExpressionHandler(PrefixExpression node) {
		this.node = node;
	}

	/**
	 * PrefixExpression:
     * 		PrefixOperator Expression
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add operator in PrefixExpression
		features.addOperator(node.getOperator().toString(), 1, isInBody);
		// expression is handled in other handlers
		return features;
	}
}
