package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class SuperConstructorInvocationHandler implements NodeHandler {

	private SuperConstructorInvocation node;
	
	public SuperConstructorInvocationHandler(SuperConstructorInvocation node) {
		this.node = node;
	}

	/**
	 *  SuperConstructorInvocation:
	 *  	[ Expression . ] [ < Type { , Type } > ] super ( [ Expression { , Expression } ] );
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of separator "." if exists
		if(node.getExpression() != null) {
			features.addSeparator(TokenType.DOT.toString(), 1, true);
		}
		List<Type> typeArguments = node.typeArguments();
		if(typeArguments != null && typeArguments.size() > 1) {
			// add the feature of operator ">" and "<"
			// Note: operator "<" ">" are considered as separator here
			features.addSeparator(TokenType.LT.toString(), 1, true);
			features.addSeparator(TokenType.GT.toString(), 1, true);
			// add the feature of separator "," if exists
			features.addSeparator(TokenType.COMMA.toString(), typeArguments.size() - 1, true);
			// type arguments are handled in Type-related handlers
		}
 		// add the feature of language keyword of "super"
		features.addKeyword(TokenType.SUPER.toString(), 1, true);
		
		/* NOTE: here we do not translate super() into a API call */
		
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, true);
		features.addSeparator(TokenType.RPAREN.toString(), 1, true);
		
		// add feature of separator "," if exist in arguments
		List<Expression> arguments = node.arguments();
		if(arguments != null && arguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), arguments.size() - 1 , true);
		}
		// add the feature of separator ";"
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		return features;
	}

}
