package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.QualifiedName;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class QualifiedNameHandler implements NodeHandler {
	
	private QualifiedName node;
	
	public QualifiedNameHandler(QualifiedName node) {
		this.node = node;
	}

	/**
	 * QualifiedName:
     *		Name . SimpleName
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// Name and simpleName are handled in name-related handlers
		// add the feature of separator "."
		features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		return features;
	}
}
