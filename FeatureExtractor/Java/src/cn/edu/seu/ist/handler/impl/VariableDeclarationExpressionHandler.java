package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.VariableDeclarationExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class VariableDeclarationExpressionHandler implements NodeHandler {

	private VariableDeclarationExpression node;
	
	public VariableDeclarationExpressionHandler(VariableDeclarationExpression node) {
		this.node = node;
	}

	/**
	 * This kind of node collects together several variable declaration 
	 * fragments (VariableDeclarationFragment) into a single expression 
	 * (Expression), all sharing the same modifiers and base type. This 
	 * type of node can be used as the initializer of a "ForStatement", 
	 * or wrapped in an ExpressionStatement to form the equivalent of a 
	 * "VariableDeclarationStatement".
	 * 
	 * Local variable declaration expression AST node type (important)
	 * VariableDeclarationExpression:
     *		{ ExtendedModifier } Type VariableDeclarationFragment
     *    	{ , VariableDeclarationFragment }
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// local variable name is handled in name-related handlers
		// ExtendedModifiers is handled in ModifierHandler
		// add the feature of separator "," if it exists
		if(node.fragments().size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), node.fragments().size() - 1, isInBody);
		}
		return features;
	}
}
