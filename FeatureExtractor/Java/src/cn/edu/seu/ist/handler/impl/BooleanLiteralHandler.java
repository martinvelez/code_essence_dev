package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.BooleanLiteral;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class BooleanLiteralHandler implements NodeHandler {
	
	private BooleanLiteral node;
	
	public BooleanLiteralHandler(BooleanLiteral node) {
		this.node = node;
	}

	/**
	 *  BooleanLiteral:
     *  	true
     *      false
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of boolean literal
		features.addBooleanLiteral(node.booleanValue(), isInBody);	
		return features;
	}

}
