package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.compiler.IScanner;
import org.eclipse.jdt.core.compiler.ITerminalSymbols;
import org.eclipse.jdt.core.compiler.InvalidInputException;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenList;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class MethodDeclarationHandler implements NodeHandler {
	// add the logger
	static Logger logger = Logger.getLogger(SimpleTypeHandler.class.getName());
	
	private MethodDeclaration node;
	private CompilationUnit cu;
	private String methodName;
	private String className;
	private TokenList tokens;
	private TokenList inBodyTokens;
	
	/**
	 * Constructor
	 * @param node
	 */
	public MethodDeclarationHandler(MethodDeclaration node, CompilationUnit cu) {
		this.node = node;
		this.methodName = null;
		this.className = null;
		this.cu = cu;
		this.tokens = getListOfTokens(node.toString());
		if(node.getBody() != null) {
			this.inBodyTokens = getListOfTokens(node.getBody().toString());
		} else {
			this.inBodyTokens = new TokenList();
		}
	}
	
	public MethodDeclarationHandler(MethodDeclaration node) {
		this(node, null);
	}

	/**
	 *  MethodDeclaration:
	    	[ Javadoc ] { ExtendedModifier }
	                  [ < TypeParameter { , TypeParameter } > ]
	        ( Type | void ) Identifier (
	        [ FormalParameter
	                     { , FormalParameter } ] ) {[ ] }
	        [ throws TypeName { , TypeName } ] ( Block | ; )
 		ConstructorDeclaration:
    		[ Javadoc ] { ExtendedModifier }
                  [ < TypeParameter { , TypeParameter } > ]
        	Identifier (
                  [ FormalParameter
                         { , FormalParameter } ] )
        	[throws TypeName { , TypeName } ] Block
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// check whether the method declaration is in anonymous class
		// if isInBody is true which means method declaration is in anonymous
		// class, then method signature is also considered in method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// Modifier is handled in ModifierHandler and annotation-related handlers 

		// add the feature of separator of "," and operator "<" ">" if exist in type parameters
		List<Type> typeParameters = node.typeParameters();
		if(typeParameters != null) {
			if(typeParameters.size() > 0) {
				// Note: operator "<" ">" are considered as separator here
				features.addSeparator(TokenType.LT.toString(), 1, isInBody);
				features.addSeparator(TokenType.GT.toString(), 1, isInBody);
			}
			if(typeParameters.size() > 1) {
				features.addSeparator(TokenType.COMMA.toString(), typeParameters.size(), isInBody);
			}
			// Type parameter is handled in TypeParameterHandler
		}
		
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
		features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		
		// add the feature of separator of "," if exists in parameters
		List<SingleVariableDeclaration> varDeclarations = node.parameters();
		if(varDeclarations.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), varDeclarations.size() - 1, isInBody);
		}
		
		// add the feature of language keyword "throws" if exists
		List<Name> exceptions = node.thrownExceptions();
		if(exceptions != null) {
			if(exceptions.size() > 0) {
				features.addKeyword(TokenType.THROWS.toString(), 1, isInBody);
			}
			if(exceptions.size() > 1) {
				features.addSeparator(TokenType.COMMA.toString(), exceptions.size() - 1, isInBody);
			}
		}
		
		// if there is no body after method declaration
		if(node.getBody() == null) {
			// add the feature of separator ";"
			features.addSeparator(TokenType.SEMI.toString(), 1, isInBody);
		}
		
		// get the fully qualified class name that contain the method 
		IMethodBinding methodBinding = null;
		try {
			methodBinding = node.resolveBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		if(methodBinding != null) {
			 ITypeBinding declaringClass = null;
			 try {
				 declaringClass = methodBinding.getDeclaringClass();
			 } catch(Exception e) {
				 logger.error(ASTNodeHelper.getExceptionMessage(node), e);
			 }
			 if(declaringClass != null) {
				 String qualifiedClassName = declaringClass.getQualifiedName();
				 if(qualifiedClassName != null && !"".equals(qualifiedClassName)) {
					 this.className = qualifiedClassName;
				 }
			 } 
		}
		// get the method name
		StringBuffer buffer = new StringBuffer();
		buffer.append(node.getName().getIdentifier());
		buffer.append("(");
		List<SingleVariableDeclaration> variableDeclarations = node.parameters();
		for(SingleVariableDeclaration variable : variableDeclarations) {
			SimpleName variableName = variable.getName();
			IBinding binding  = null; 
			try {
				binding = variableName.resolveBinding();
			} catch (Exception e) {
				logger.error(ASTNodeHelper.getExceptionMessage(node), e);
			}
			if(binding != null) {
				if(binding.getKind() == IBinding.VARIABLE) {
					IVariableBinding variableBinding = (IVariableBinding)binding;
					ITypeBinding variableTypeBinding = variableBinding.getType();
					if(variableTypeBinding != null) {
						buffer.append(variableTypeBinding.getQualifiedName()).append(",");
					}
				}
			}
		}
		if(buffer.lastIndexOf(",") == buffer.length() - 1) {
			buffer.deleteCharAt(buffer.length() - 1);
		}
		buffer.append(")");
		
		// set the method name
		this.methodName = buffer.toString();
		
		return features;
	}
	
	/**
	 * Get the class Name
	 * @return
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Get the method name
	 * @return
	 */
	public String getClassName() {
		return className;
	}
	
	/**
	 * Get the start line of the method
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int getStartLine() {
		int start = 0;
		if(node.modifiers() != null && node.modifiers().size() > 0) {
			List<IExtendedModifier> iModifiers = node.modifiers();
			IExtendedModifier firstModifier = iModifiers.get(0);
			if(firstModifier.isAnnotation()) {
				Annotation annotation = (Annotation)firstModifier;
				start = cu.getLineNumber(annotation.getStartPosition());
			} else {
				Modifier modifier = (Modifier)firstModifier;
				start = cu.getLineNumber(modifier.getStartPosition());
			}
		} else if(node.getReturnType2() != null) {
			start = cu.getLineNumber(node.getReturnType2().getStartPosition());
		} else {
			start = cu.getLineNumber(node.getName().getStartPosition());
		}
		return start;
	}

	/**
	 * Get the end line of the method 
	 * @return
	 */
	public int getEndLine() {
		return cu.getLineNumber(node.getStartPosition() + node.getLength());
	}

	/**
	 * Get the end line of the method signature
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int getEndLineOfSignature() {
		int end = 0;
		if(node.thrownExceptions() != null && node.thrownExceptions().size() > 0) {
			List<Name> exceptions = node.thrownExceptions();
			Name firstException = exceptions.get(0);
			end = cu.getLineNumber(firstException.getStartPosition());
		} else if(node.parameters() != null && node.parameters().size() > 0) {
			List<SingleVariableDeclaration> formalParameters = node.parameters();
			SingleVariableDeclaration formalParameter = formalParameters.get(0);
			end = cu.getLineNumber(formalParameter.getStartPosition());
		} else {
			end = cu.getLineNumber(node.getName().getStartPosition());
		}
		return end;
	}

	/**
	 * Get the start line of the method body
	 * If there is no method body, return -1
	 * @return
	 */
	public int getStartLineOfBody() {
		int start = -1;
		if(node.getBody() != null) {
			start = cu.getLineNumber(node.getBody().getStartPosition());
		}
		return start;
	}

	/**
	 * Get the list of tokens in the whole method
	 * @return
	 */
	public TokenList getTokens() {
		return this.tokens;
	}
	
	/**
	 * Get the list of tokens in method body
	 * @return
	 */
	public TokenList getInBodyTokens() {
		return this.inBodyTokens;
	}

	/**
	 * Get the number of tokens
	 * @return
	 */
	public int getNumOfTokens() {
		int number = 0;
		if(this.tokens != null) {
			number = this.tokens.size();
		}
		return number;
	}
	
	/**
	 * Get the number of tokens in the method body only
	 * @return
	 */
	public int getNumOfTokensInBody() {
		int number = 0;
		if(this.inBodyTokens != null && this.inBodyTokens.size() > 0) {
			number = this.inBodyTokens.size();
		}
		return number;
	}
	
	/**
	 * Get the number of tokens in method signature
	 * @return
	 */
	public int getNumOfTokensInSignature() {
		return getNumOfTokens() - getNumOfTokensInBody();
	}
	
	/**
	 * Get number of tokens in given char array 
	 * Note: There is some counting problem when scanner meet ">>". 
	 * For example, when it meet java.lang.Iterable<java.util.Comparator<java.io.File>>, 
	 * it will regard ">>" as a token while not double ">". It is a compiler issue
	 * @param arrays
	 * @return
	 */
	private TokenList getListOfTokens(String content) {
		TokenList tokens = new TokenList();
		IScanner scanner = ToolFactory.createScanner(/*tokenizeComments*/false, 
				/*tokenizeWhiteSpace*/false, /*recordLineSeparator*/false, 
				/*sourceLevel*/"1.6", /*complianceLevel*/"1.6");
		scanner.setSource(content.toCharArray());
		while (true) {
		    int tokenID = 0;
			try {
				tokenID = scanner.getNextToken();
			} catch (InvalidInputException e) {
				e.printStackTrace();
			}
			
		    if (tokenID != ITerminalSymbols.TokenNameEOF) {
		    	tokens.addToken(new String(scanner.getRawTokenSource()));
		    } else {
		    	break;
		    }
		}
		return tokens;
	}
}

