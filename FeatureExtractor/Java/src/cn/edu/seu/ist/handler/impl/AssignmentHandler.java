package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.Assignment;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class AssignmentHandler implements NodeHandler {
	
	private Assignment node;
	
	public AssignmentHandler(Assignment node) {
		this.node = node;
	}

	/**
	 *  Assignment:
     *		Expression AssignmentOperator Expression
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of operator
		features.addOperator(node.getOperator().toString(), 1, isInBody);
		// expression part is handled in other handlers
		return features;
	}

}
