package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.TypeParameter;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class TypeParameterHandler implements NodeHandler {
	
	private TypeParameter node;

	public TypeParameterHandler(TypeParameter node) {
		this.node = node;
	}

	/**
	 * TypeParameter:
     *		TypeVariable [ extends Type { & Type } ] 
     * Note: typeParameter only exist in method declaration and type declaration
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of type variable
		int bounds = node.typeBounds().size();
		if(bounds > 0) {
			// add the feature of keyword "extends"
			features.addKeyword(TokenType.EXTENDS.toString(), 1, isInBody);
		}
		if(bounds > 1) {
			// add the feature of operator "&" 
			features.addOperator(TokenType.AMP.toString(), bounds - 1, isInBody);
		}
		// Type is handled in Type-related handlers
		return features;
	}

}
