package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.ArrayCreation;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class ArrayCreationHandler implements NodeHandler {
	
	private ArrayCreation node;

	public ArrayCreationHandler(ArrayCreation node) {
		this.node = node;
	}

	/**
	 *  ArrayCreation:
     * 		new PrimitiveType [ Expression ] { [ Expression ] } { [ ] }
     *		new TypeName [ < Type { , Type } > ] [ Expression ] { [ Expression ] } { [ ] }
     * 		new PrimitiveType [ ] { [ ] } ArrayInitializer
     *		new TypeName [ < Type { , Type } > ] [ ] { [ ] } ArrayInitializer
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of language keyword "new"
		features.addKeyword(TokenType.NEW.toString(), 1, isInBody);
		// ArrayType is handled in Type-related handlers
		// Array initializers is handled in ArrayInitializerHandler
		return features;
	}
}
