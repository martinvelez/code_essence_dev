package cn.edu.seu.ist.handler;

import cn.edu.seu.ist.model.feature.FeatureSet;

/**
 * Interface of AST node handler
 * @author Dong Qiu
 *
 */
public interface NodeHandler {
	
	/**
	 * Handle the AST node 	
	 * @return feature set of node under analysis
	 */
	public FeatureSet handle();
	
}
