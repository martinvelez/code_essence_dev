package cn.edu.seu.ist.handler.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Type;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.checker.JDKChecker;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;
import cn.edu.seu.ist.visitor.helper.BindingHelper;

public class MethodInvocationHandler implements NodeHandler {
	
	// add the logger
	static Logger logger = Logger.getLogger(SimpleTypeHandler.class.getName());

	private MethodInvocation node;
	
	public MethodInvocationHandler(MethodInvocation node) {
		this.node = node;
	}

	/**
	 * MethodInvocation:
     * 		[ Expression . ] [ < Type { , Type } > ] Identifier ( [ Expression { , Expression } ] )
     * 
     * Note: we redesign the structure of APIcall as:
     * ClassName.methodName(parameterType, ...)
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "." if exists
		if(node.getExpression() != null) {
			// Expression is handle in the other handlers
			features.addSeparator(TokenType.DOT.toString(), 1, isInBody);
		}
		
		// add the feature of separator "," if type arguments exist
		List<Type> typeArguments = node.typeArguments();
		if(typeArguments != null && typeArguments.size() > 0) {
			// add the feature of operator "<" and ">"
			// Note: operator "<" ">" are considered as separator here
			features.addSeparator(TokenType.LT.toString(), 1, isInBody);
			features.addSeparator(TokenType.GT.toString(), 1, isInBody);
			if(typeArguments.size() > 1) {
				features.addSeparator(TokenType.COMMA.toString(), typeArguments.size() - 1, isInBody);
			}
			// Type is handled in Type-related handlers
		}
		
		String MCallLexeme = node.getName().getIdentifier();
		String MCallRefined = null;
		// generation of fully qualified method name of API call
		IMethodBinding apiCallBinding = null;
		try {
			apiCallBinding = node.resolveMethodBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		
		StringBuffer qualifedMethod = new StringBuffer();
		if(apiCallBinding != null) {
			// append the qualified class name to API call
			String className = null;
			ITypeBinding declaringClass = apiCallBinding.getDeclaringClass();
			if(declaringClass != null) {
				className = declaringClass.getErasure().getQualifiedName();
			} 
			//Check if it is public API call
			if(JDKChecker.isFromJDK(className)) {
				qualifedMethod.append(className);
				qualifedMethod.append(TokenType.DOT.toString());
				// append the method arguments
				 ITypeBinding[] arguments = apiCallBinding.getTypeArguments();
				 if(arguments != null && arguments.length > 0) {
					 qualifedMethod.append(TokenType.LT);
					 for(ITypeBinding argument : arguments) {
						 if(argument != null)
						 qualifedMethod.append(BindingHelper.getRefinedInfo(argument))
						 	.append(TokenType.COMMA);
					 }
					 qualifedMethod.deleteCharAt(qualifedMethod.length() - 1);
					 qualifedMethod.append(TokenType.GT);
				 }
				// append the method name
				IMethodBinding methodBinding = apiCallBinding.getMethodDeclaration();
				if(methodBinding != null) {
					qualifedMethod.append(methodBinding.getName());
				} else {
					qualifedMethod.append(node.getName().getIdentifier());
				}
				
				qualifedMethod.append(TokenType.LPAREN);
				// append the method parameters
				ITypeBinding[] paramsType = apiCallBinding.getParameterTypes();
				if(paramsType!= null && paramsType.length > 0) {
					for(ITypeBinding paramType : paramsType) {
							qualifedMethod.append(BindingHelper.getRefinedInfo(paramType))
								.append(TokenType.COMMA);
					}
					qualifedMethod.deleteCharAt(qualifedMethod.length() - 1);
				}
				qualifedMethod.append(TokenType.RPAREN);
				MCallRefined = qualifedMethod.toString();
			} else {
				MCallRefined = TokenType.LOCAL_CALL.toString();
			}
		} 
		// add the feature of API Call (public or local)
		features.addIMethodCall(MCallLexeme, MCallRefined, isInBody);
		
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
		features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		
		// add feature of separator "," if exist in arguments
		List<Expression> arguments = node.arguments();
		if(arguments != null && arguments.size() > 1) {
			features.addSeparator(TokenType.COMMA.toString(), arguments.size() - 1, isInBody);
		}
		return features;
	}
}
