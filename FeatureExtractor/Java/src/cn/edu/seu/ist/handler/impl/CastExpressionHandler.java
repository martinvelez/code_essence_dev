package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.CastExpression;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class CastExpressionHandler implements NodeHandler {
	
	private CastExpression node;
	
	public CastExpressionHandler(CastExpression node) {
		this.node = node;
	}

	/**
	 * CastExpression : 
	 * 		( Type ) Expression
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of separator "(" and ")"
		features.addSeparator(TokenType.LPAREN.toString(), 1, isInBody);
		features.addSeparator(TokenType.RPAREN.toString(), 1, isInBody);
		// Type is handled in other Type-related handlers
		// Expression is handled in other handlers
		return features;
	}

}
