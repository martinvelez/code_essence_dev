package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.NullLiteral;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class NullLiteralHandler implements NodeHandler {
	
	private NullLiteral node;
	
	public NullLiteralHandler(NullLiteral node) {
		this.node = node;
	}

	/**
	 * Null literal node
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of constant null literal 
		features.addNullLiteral(isInBody);
		return features;
	}
}
