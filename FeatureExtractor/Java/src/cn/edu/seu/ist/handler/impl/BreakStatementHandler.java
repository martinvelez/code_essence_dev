package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class BreakStatementHandler implements NodeHandler {
	
	/**
	 * BreakStatement:
     *		break [ Identifier ] ;
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// add the feature of language keyword "break"
		features.addKeyword(TokenType.BREAK.toString(), 1, true);
		// label is handled in simpleNameHandler
		// add the feature of separator ";" 
		features.addSeparator(TokenType.SEMI.toString(), 1, true);
		
		return features;
	}
}
