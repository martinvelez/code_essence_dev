package cn.edu.seu.ist.handler.impl;

import org.eclipse.jdt.core.dom.MemberValuePair;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;

public class MemberValuePairHandler implements NodeHandler {

	private MemberValuePair node;
	
	public MemberValuePairHandler(MemberValuePair node) {
		this.node = node;
	}

	/**
	 *  MemberValuePair:
   	 * 		SimpleName = Expression
	 * @param node
	 * @return
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// Check whether the annotation is in method signature or method body
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		// add the feature of operator "="
		features.addOperator(TokenType.EQ.toString(), 1, isInBody);
		return features;
	}

}
