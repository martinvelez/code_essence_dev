package cn.edu.seu.ist.handler.impl;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenType;

public class LabeledStatementHandler implements NodeHandler {
	
	/**
	 *  LabeledStatement:
     *		Identifier : Statement
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		// the feature of label is handled in simpleName handler
		// add the feature of operator ":" 
		features.addOperator(TokenType.COLON.toString(), 1, true);
		// statement is handled in other handlers
		return features;
	}
}
