package cn.edu.seu.ist.handler.impl;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.SimpleName;

import cn.edu.seu.ist.handler.NodeHandler;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.method.Method;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.visitor.helper.ASTNodeHelper;
import cn.edu.seu.ist.visitor.helper.BindingHelper;
import cn.edu.seu.ist.visitor.helper.VariableHelper;

public class SimpleNameHandler implements NodeHandler {
	
	// add the logger
	static Logger logger = Logger.getLogger(SimpleTypeHandler.class.getName());
	
	private SimpleName node; 
	private Method method;
	
	public SimpleNameHandler(SimpleName node, Method method) {
		this.node = node;
		this.method = method;
	}
	/**
	 * SimpleName:
     * 		Identifier
	 */
	@Override
	public FeatureSet handle() {
		FeatureSet features = new FeatureSet();
		boolean isInBody = ASTNodeHelper.isInMethodBody(node);
		
		String lexeme = node.getIdentifier();
		String refinedInfo = null;
		
		// get the category of the simpleName
		TokenType tokenType = VariableHelper.getNameTokenType(node, this.method);
		ITypeBinding typeBinding = null;
		if(tokenType == TokenType.TYPE) {
			try {
				typeBinding = node.resolveTypeBinding();
			} catch (Exception e) {
				logger.error(ASTNodeHelper.getExceptionMessage(node), e);
			}
		}
		if(tokenType == TokenType.VARIABLE) {
			IBinding binding = null;
			try {
				binding = node.resolveBinding();
			} catch (Exception e) {
				logger.error(ASTNodeHelper.getExceptionMessage(node), e);
			}
			if(binding != null && binding.getKind() == IBinding.VARIABLE) {
				IVariableBinding varBinding = (IVariableBinding)binding;
				typeBinding =  varBinding.getType();
			}
		}
		
		if(typeBinding != null) {
			refinedInfo = BindingHelper.getRefinedInfo(typeBinding);
		}
		
		if(tokenType != null) {
			if(tokenType == TokenType.OTHER) {
				features.addIOthers(lexeme, isInBody);
			} else if(tokenType == TokenType.VARIABLE) {
				features.addIVariable(lexeme, refinedInfo, isInBody);
			} else if(tokenType == TokenType.TYPE) {
				features.addIType(lexeme, refinedInfo, isInBody);
			} else if(tokenType == TokenType.UNDEFINED) {
				features.addIUndefined(lexeme, isInBody);
			}
		}
		return features;
	}
	
	
}
