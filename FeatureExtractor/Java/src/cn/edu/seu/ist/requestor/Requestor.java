package cn.edu.seu.ist.requestor;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FileASTRequestor;

import cn.edu.seu.ist.conf.ProjConfig;
import cn.edu.seu.ist.global.CurrentFilePaths;
import cn.edu.seu.ist.model.file.JavaFile;
import cn.edu.seu.ist.model.project.Project;
import cn.edu.seu.ist.visitor.AbstractVisitor;

public class Requestor extends FileASTRequestor{
	
	Project project;
	
	/**
	 * Constructor
	 */
	public Requestor(String projName) {
		project = new Project(projName);
	}
	
	/**
	 * Get the project information
	 * @return
	 */
	public Project getProjectInfo() {
		return project;
	}
	
	public void addJavaFile(JavaFile javaFile) {
		project.addJavaFile(javaFile);
	}

	@Override
	public void acceptAST(String sourceFilePath, CompilationUnit ast) {
		if(isDuplicates(ast)) {
			// record the bad paths
			CurrentFilePaths.addFilePath(sourceFilePath);
		} else {
			AbstractVisitor visitor = AbstractVisitor.prepareClass(ProjConfig.executionMode, 
					new Object[]{sourceFilePath});
			//visit the ast tree
			ast.accept(visitor);
			addJavaFile(visitor.getJavaFile());
		}
		super.acceptAST(sourceFilePath, ast);
	}
	
	/**
	 * Check whether the java file is duplicates within the project
	 * @param ast
	 * @return
	 */
	private boolean isDuplicates(CompilationUnit ast) {
		IProblem[] problems = ast.getProblems();
		for(IProblem problem : problems) {
			if(problem.isError()) {
				if(problem.getID() == IProblem.DuplicateTypes) {
					return true;
				}
			}
		}
		return false;
	}
}
