package cn.edu.seu.ist.model.feature;

import cn.edu.seu.ist.model.token.TokenType;

/**
 * The representation of feature
 * Feature = {lexeme, high-level token type, low-level token type, refined category, count, isInBody}
 * @author Dong Qiu
 *
 */
public class Feature {
	/* lexeme of the feature */
	private String lexeme;
	/* High level token type */
	private TokenType highLevel;
	/* Low level token type */
	private TokenType lowLevel;
	/* record the category of feature */
	private TokenType refinedLevel;
	/* refined info for feature category */
	private String refinedInfo;
	/* record the number of features */
	private int count;
	/* check whether the feature is in method body */
	private boolean isInBody;
	
	/**
	 * Constructor
	 * @param lexeme
	 * @param highLevel
	 * @param lowLevel
	 * @param refinedLevel
	 * @param refinedInfo
	 * @param count
	 * @param isInBody
	 */
	public Feature(String lexeme, TokenType highLevel, TokenType lowLevel, TokenType refinedLevel, 
			String refinedInfo, int count, boolean isInBody) {
		this.lexeme = lexeme;
		this.highLevel = highLevel;
		this.lowLevel = lowLevel;
		this.refinedLevel = refinedLevel;
		this.refinedInfo = refinedInfo;
		this.count = count;
		this.isInBody = isInBody;
	}
	
	/* Setters and getters */
	public String getLexeme() {
		return lexeme;
	}

	public void setLexeme(String lexeme) {
		this.lexeme = lexeme;
	}

	public TokenType getHighLevel() {
		return highLevel;
	}

	public void setHighLevel(TokenType highLevel) {
		this.highLevel = highLevel;
	}

	public TokenType getLowLevel() {
		return lowLevel;
	}

	public void setLowLevel(TokenType lowLevel) {
		this.lowLevel = lowLevel;
	}

	public TokenType getRefinedLevel() {
		return refinedLevel;
	}

	public void setRefinedLevel(TokenType refinedLevel) {
		this.refinedLevel = refinedLevel;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public String getRefinedInfo() {
		return refinedInfo;
	}

	public void setRefinedInfo(String refinedInfo) {
		this.refinedInfo = refinedInfo;
	}

	public boolean isInBody() {
		return isInBody;
	}

	public void setInBody(boolean isInBody) {
		this.isInBody = isInBody;
	}

	/**
	 * increase the count of feature
	 * @param count
	 */
	public void increase(int count) {
		this.count += count;
	}
	
	/**
	 * increase the count by one
	 * @param count
	 */
	public void increase() {
		this.count += 1;
	}
		
	/**
	 * Rewrite the equals for feature comparison 
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(this.getClass() != obj.getClass()) {
			return false;
		}
		boolean flag = false;
		Feature feature = (Feature)obj;
		if(feature.getLexeme().equals(this.lexeme) && feature.getRefinedLevel() == this.getRefinedLevel()
				&& feature.getHighLevel() == this.highLevel && feature.getLowLevel() == this.lowLevel 
				&& feature.isInBody == this.isInBody) {
			if(feature.getRefinedInfo() == null && this.refinedInfo == null) {
				flag = true;
			}
			if(feature.getRefinedInfo() != null && this.refinedInfo != null) {
				if(feature.getRefinedInfo().equals(this.refinedInfo)) {
					flag = true;
				}
			}
		}
		return flag;
	}
	
	@Override
	public int hashCode() {
		int hashcode = 17;
		hashcode = 31 * hashcode + this.lexeme.hashCode();
		hashcode = 31 * hashcode + this.highLevel.hashCode();
		hashcode = 31 * hashcode + this.lowLevel.hashCode();
		if(this.refinedLevel != null) {
			hashcode = 31 * hashcode + this.refinedLevel.hashCode();
		}
		if(this.refinedInfo != null) {
			hashcode = 31 * hashcode + this.refinedInfo.hashCode();
		}
		return hashcode;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if(this.isInBody) {
			buffer.append(true);
		} else {
			buffer.append(false);
		}
		buffer.append(" ");
		
		buffer.append(this.lexeme).append(" ");
		buffer.append(this.highLevel).append(" ");
		buffer.append(this.lowLevel).append(" ");
		if(this.refinedLevel != null) {
			buffer.append(this.refinedLevel);
		} else {
			buffer.append("null");
		}
		if(this.refinedInfo != null) {
			buffer.append(this.refinedInfo);
		} else {
			buffer.append("null");
		}
		buffer.append(" ");
		buffer.append(this.count).append(" ");
		return buffer.toString();
	}
}
