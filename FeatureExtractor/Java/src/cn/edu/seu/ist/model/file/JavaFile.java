package cn.edu.seu.ist.model.file;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.commons.io.FilenameUtils;

import cn.edu.seu.ist.model.method.Method;

/**
 * Representation of Java files
 * @author Dong Qiu
 *
 */
public class JavaFile implements Cloneable {
	/* source code path of the java file */
	private String sourceFilePath;
	/* name of java file */
	private String fileName;
	/* package name of the java file */
	private String packageName;
	/* public class name of the java file */
	private String publicClassName;
	/* all class names in the java file */
	private Collection<String> classNames;
	/* source code of the java file without comments and blanks */
	private StringBuffer sourceCode;
	/* all methods in the java file */
	private Collection<Method> methods;
	
	/**
	 * Constructor
	 * @param sourcePath
	 */
	public JavaFile(String sourcePath) {
		this.sourceFilePath = sourcePath;
		this.fileName = FilenameUtils.getBaseName(this.sourceFilePath);
		this.packageName = null;
		this.publicClassName = null;
		this.classNames = new ArrayList<String>();
		this.sourceCode = new StringBuffer();
		this.methods = new LinkedList<Method>();
	}
	/* Setters and Getters */
	public String getSourceFilePath() {
		return sourceFilePath;
	}

	public void setSourceFilePath(String sourceFilePath) {
		this.sourceFilePath = sourceFilePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		if(packageName != null) {
			this.packageName = packageName;
		}
	}

	public String getPublicClassName() {
		return publicClassName;
	}

	public void setPublicClassName(String publicClassName) {
		if(publicClassName != null) {
			this.publicClassName = publicClassName;
		}
	}
	
	public Collection<Method> getMethods() {
		return methods;
	}
	public Collection<String> getClassNames() {
		return classNames;
	}
	
	public String getMethodSourceCode() {
		return sourceCode.toString();
	}

	/**
	 * Get the compilantUnit name of the java file
	 * e.g. fully qualified package name + public class name
	 * @return
	 */
	public String getCompilationUnitName() {
		StringBuffer buffer = new StringBuffer();
		if(this.packageName != null) {
			buffer.append(this.packageName).append(".");
		}
		if(this.publicClassName != null) {
			buffer.append(this.publicClassName);
		} else {
			buffer.append(this.fileName);
		}
		return buffer.toString();
	}
	public void appendMethodSourceCode(String sourceCode) {
		this.sourceCode.append(sourceCode);
	}

	/**
	 * Check whether the naming of java file is valid 
	 * (e.g. public class name should be equal with java file name)
	 * @return
	 */
	public boolean isValid() {
		boolean isValid = true;
		if(this.publicClassName != null) {
			if(this.publicClassName.equals(this.fileName)) {
				isValid = false;
			}
		}
		return isValid;
	}
	
	/**
	 * Add a method into the method list of the java file
	 * @param method
	 */
	public void addMethod(Method method) {
		this.methods.add(method);
	}
	
	/**
	 * Add className into class name list of the java file
	 * @param className
	 */
	public void addClassName(String className) {
		this.classNames.add(className);
	}

	@Override
	public int hashCode() {
		int hashcode = 17;
		hashcode = 31 * hashcode + this.packageName.hashCode();
		hashcode = 31 * hashcode + this.fileName.hashCode();
		return hashcode;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		} 
		if(obj instanceof JavaFile) {
			JavaFile javaFile = (JavaFile)obj;
			if(javaFile.getCompilationUnitName().equals(this.getCompilationUnitName())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return this.sourceFilePath;
	}
}

