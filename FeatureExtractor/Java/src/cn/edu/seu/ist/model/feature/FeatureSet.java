package cn.edu.seu.ist.model.feature;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import cn.edu.seu.ist.model.token.TokenType;

/**
 * The representation of feature set in one method 
 * @author Dong Qiu
 *
 */
public class FeatureSet {
	
	/* Set of features */
	private Set<Feature> features;

	/**
	 * Constructor
	 */
	public FeatureSet() {
		this.features = new LinkedHashSet<Feature>();
	}

	/**
	 * Constructor
	 * @param features
	 */
	public FeatureSet(Set<Feature> features) {
		this.features = features;
	}
	
	/* Getter and Setter */
	public Set<Feature> getFeatures() {
		return features;
	}
	
	/**
	 * Return the number of all features in a method 
	 * @return
	 */
	public int getNumOfFeatures() {
		int number = 0;
		for(Feature feature : this.features) {
			number += feature.getCount();
		}
		return number;
	}
	
	/**
	 * Get the number of features in method body
	 * @return
	 */
	public int getNumOfFeaturesInBody() {
		int number = 0;
		for(Feature feature : this.features) {
			if(feature.isInBody()) {
				number += feature.getCount();
			}
		}
		return number;
	}
	
	/**
	 * Get the number of features in method signature
	 * @return
	 */
	public int getNumOfFeaturesInSignature() {
		return getNumOfFeatures() - getNumOfFeaturesInBody();
	}
	
	/**
	 * Add feature 
	 * if the feature exists in the set, update the count 
	 * @param feature
	 */
	public void addFeature(Feature feature) {
		if(feature != null) {
			boolean exist = this.features.add(feature);
			if(!exist) {
				updateFeature(feature);
			} 
		}
	}
	
	/**
	 * Add feature
	 * @param lexeme
	 * @param highLevel
	 * @param lowLevel
	 * @param refinedLevel
	 * @param refinedInfo
	 * @param count
	 * @param isInBody
	 */
	public void addFeature(String lexeme, TokenType highLevel, TokenType lowLevel, TokenType refinedLevel, 
			String refinedInfo, int count, boolean isInBody) {
		addFeature(new Feature(lexeme, highLevel, lowLevel, refinedLevel, refinedInfo, count, isInBody));
		// testing scripts
		// System.out.println("Add feature " + lexeme);
	}
	
	/**
	 * Add the separator into the feature set 
	 * highLevel = TokenType.SEPARATOR
	 * lowLevel = TypeType.SEPARATOR.concreteType
	 * refinedLevel = null
	 * refinedInfo = null
	 * @param lexeme
	 * @param count
	 * @param isInBody
	 */
	public void addSeparator(String lexeme, int count, boolean isInBody) {
		addFeature(lexeme, TokenType.SEPARATOR, TokenType.findSeparator(lexeme), null, null, count, isInBody);
	}
	
	/**
	 * Add the operator into the feature set 
	 * highLevel = TokenType.OPERATOR
	 * lowLevel = TypeType.OPERATOR.concreteType
	 * refinedLevel = null
	 * refinedInfo = null
	 * @param lexeme
	 * @param count
	 * @param isInBody
	 */
	public void addOperator(String lexeme, int count, boolean isInBody) {
		addFeature(lexeme, TokenType.OPERATOR, TokenType.findOperator(lexeme), null, null, count, isInBody);
	}
	
	/**
	 * Add the language keyword into the feature set
	 * highLevel = TokenType.KEYWORD
	 * lowLevel = TokenType.KEYWORD.concreteType
	 * refinedLevel = TokenType.KEYWORD.refinedLevel
	 * refinedInfo = null
	 * @param lexeme
	 * @param lowLevel
	 * @param refinedLevel
	 * @param count
	 * @param isInBody
	 */
	public void addKeyword(String lexeme, int count, boolean isInBody) {
		addFeature(lexeme, TokenType.KEYWORD, TokenType.findKeyword(lexeme), 
				TokenType.findRefinedKeyword(lexeme), null, count, isInBody);
	}
	
	/**
	 * Add refined category of identifier methodCall in feature set 
	 * @param lexeme
	 * @param refinedInfo
	 */
	public void addIMethodCall(String lexeme, String refinedInfo, boolean isInBody) {
		if(refinedInfo == null) {
			refinedInfo = TokenType.LOCAL_CALL.toString();
		}
		addFeature(lexeme, TokenType.IDENTIFIER, TokenType.IDENTIFIER, TokenType.METHODCALL, refinedInfo, 1, isInBody);
	}
	
	/**
	 * Add refined category of identifier variables in feature set 
	 * @param lexeme
	 * @param refinedInfo
	 * @param isInBody
	 */
	public void addIVariable(String lexeme, String refinedInfo, boolean isInBody) {
		if(refinedInfo == null) {
			refinedInfo = TokenType.LOCAL_TYPE.toString();
		}
		addFeature(lexeme, TokenType.IDENTIFIER, TokenType.IDENTIFIER, TokenType.VARIABLE, refinedInfo, 1, isInBody);
	}
	
	/**
	 * Add refined category of identifier types in feature set 
	 * @param lexeme
	 * @param refinedInfo
	 */
	public void addIType(String lexeme, String refinedInfo, boolean isInBody) {
		if(refinedInfo == null) {
			refinedInfo = TokenType.LOCAL_TYPE.toString();
		}
		addFeature(lexeme, TokenType.IDENTIFIER, TokenType.IDENTIFIER, TokenType.TYPE, refinedInfo, 1, isInBody);
	}
	
	/**
	 * Add refined category of identifier others in feature set 
	 * @param lexeme
	 * @param isInBody
	 */
	public void addIOthers(String lexeme, boolean isInBody) {
		addFeature(lexeme, TokenType.IDENTIFIER, TokenType.IDENTIFIER, TokenType.OTHER, null, 1, isInBody);
	}
	
	/**
	 * Add refined category of identifier undefined in feature set 
	 * @param lexeme
	 * @param isInBody
	 */
	public void addIUndefined(String lexeme, boolean isInBody) {
		addFeature(lexeme, TokenType.IDENTIFIER, TokenType.IDENTIFIER, TokenType.UNDEFINED, null, 1, isInBody);
	}
	
	/**
	 * Add the numerical literal into the feature set 
	 * @param lexeme
	 */
	public void addNumericLiteral(String lexeme, TokenType lowLevel, boolean isInBody) {
		addFeature(lexeme, TokenType.LITERAL, lowLevel, null, null, 1, isInBody);
	}
	
	/**
	 * Add the char literal into the feature set 
	 * @param lexeme
	 */
	public void addCharLiteral(String lexeme, boolean isInBody) {
		addFeature(lexeme, TokenType.LITERAL, TokenType.CHARLITERAL, null, null, 1, isInBody);
	}
	
	/**
	 * Add the string literal into the feature set 
	 * @param lexeme
	 */
	public void addStringLiteral(String lexeme, boolean isInBody) {
		addFeature(lexeme, TokenType.LITERAL, TokenType.STRINGLITERAL, null, null, 1, isInBody);
	}
	
	/**
	 * Add the boolean literal into the feature set 
	 * @param lexeme
	 */
	public void addBooleanLiteral(boolean lexeme, boolean isInBody) {
		String lexemeContent = TokenType.FALSE.toString();
		TokenType lowLevel = TokenType.FALSE;
		if(lexeme) {
			lexemeContent = TokenType.TRUE.toString();
			lowLevel = TokenType.TRUE;
		} 
		addFeature(lexemeContent, TokenType.LITERAL, lowLevel, null, null, 1, isInBody);
	}
	
	/**
	 * Add the null literal into the feature set 
	 */
	public void addNullLiteral(boolean isInBody) {
		addFeature(TokenType.NULL.toString(), TokenType.LITERAL, TokenType.NULL, null, null, 1, isInBody);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		for(Feature feature : this.features) {
			if(feature.isInBody()) {
				result = prime * result + ((feature == null) ? 0 : feature.hashCode());
			}
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		boolean flag = true;
		FeatureSet featureSet = (FeatureSet) obj;
		// get all in-body features from this object
		Set<Feature> thisInBodyFeatures = new LinkedHashSet<Feature>();
		for(Feature feature : this.features) {
			if(feature.isInBody()) {
				thisInBodyFeatures.add(feature);
			}
		}
		// get all in-body features from obj
		Set<Feature> InBodyFeatures = new LinkedHashSet<Feature>();
		for(Feature feature : featureSet.getFeatures()) {
			if(feature.isInBody()) {
				InBodyFeatures.add(feature);
			}
		}
		// first compare the size of in-body features
		if(thisInBodyFeatures.size() != InBodyFeatures.size()) {
			flag = false;
		} else {
			// then compare each in-body feature one-by-one
			for(Feature feature : thisInBodyFeatures) {
				if(!InBodyFeatures.contains(feature)) {
					flag = false;
					break;
				}
			}
			for(Feature feature : InBodyFeatures) {
				if(!thisInBodyFeatures.contains(feature)) {
					flag = false;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * If the feature exists in the set, update the count
	 * @param feature
	 * @param num
	 */
	private void updateFeature(Feature feature) {
		Iterator<Feature> iterator = this.features.iterator();
		while(iterator.hasNext()) {
			Feature existFeature = iterator.next();
			if(existFeature.equals(feature)) {
				existFeature.increase(feature.getCount());
				break;
			}
		}
	}
}
