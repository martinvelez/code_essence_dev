package cn.edu.seu.ist.model.method;

import java.util.ArrayList;
import java.util.List;

import cn.edu.seu.ist.model.feature.Feature;
import cn.edu.seu.ist.model.feature.FeatureSet;
import cn.edu.seu.ist.model.token.TokenList;

/**
 * Representation of Java Mehtod
 * @author Dong Qiu
 *
 */
public class Method {
	/* An unique id for method */
	private long mid;
	/* The name of the method */
	private String methodName;
	/* The name of class which contain the method */
	private String className;
	/* Path of source file that contains the method */
	private String sourceFilePath;
	/* start line of method in the java file */
	private int startLine;
	/* end line of method in the java file */
	private int endLine;
	/* start line of the method body in the java file */
	private int startLineOfBody;
	/* end line of the method signature in the java file */
	private int endLineOfSignature;
	/* number of tokens in the method body */
	private int tokensInBody;
	/* List of total features involved in current method */	
	private FeatureSet featureSet;
	/* List of total tokens in current method */
	private TokenList tokenList;
	/* Store the local variables of the method*/
	private List<String> localVariables;

	/**
	 * Constructor
	 */
	public Method(long mid) {
		this.mid = mid;
		this.methodName = null;
		this.className = null;
		this.sourceFilePath = null;
		this.featureSet = new FeatureSet();
		this.tokenList = new TokenList();
		this.startLine = 0;
		this.endLine = 0;
		this.startLineOfBody = 0;
		this.endLineOfSignature = 0;
		this.tokensInBody = 0;
		this.localVariables = new ArrayList<String>();
	}
	
	/* Getters and Setters */
	public long getMid() {
		return mid;
	}

	public void setMid(long mid) {
		this.mid = mid;
	}
	
	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getSourceFilePath() {
		return sourceFilePath;
	}

	public void setSourceFilePath(String sourceFilePath) {
		this.sourceFilePath = sourceFilePath;
	}

	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}

	public int getStartLineOfBody() {
		return startLineOfBody;
	}

	public void setStartLineOfBody(int startLineOfBody) {
		this.startLineOfBody = startLineOfBody;
	}

	public int getEndLineOfSignature() {
		return endLineOfSignature;
	}

	public void setEndLineOfSignature(int endLineOfSignature) {
		this.endLineOfSignature = endLineOfSignature;
	}

	public int getLines() {
		return this.endLine - this.startLine + 1;
	}

	public int getLinesInBody() {
		return this.endLine - this.startLineOfBody + 1;
	}

	/**
	 * Get Fully Qualified Method Name
	 * @return
	 */
	public String getQualifiedMethodName() {
		StringBuffer buffer = new StringBuffer();
		if(this.className != null && !"".equals(className)) {
			buffer.append(this.className).append(".");
		}
		buffer.append(methodName);
		return buffer.toString();
	}
	
	/**
	 * Get number of whole tokens in method (body and signature)
	 * @return
	 */
	public int numOfTokens() {
		return this.tokenList.size();
	}
	
	public int numOfTokensInBody() {
		return this.tokensInBody;
	}
	
	public int numOfTokensInSignature() {
		return numOfTokens() - numOfTokensInBody();
	}

	/**
	 * Get number of all features in method (body and signature)
	 * @return
	 */
	public int numOfFeatures() {
		return this.featureSet.getNumOfFeatures();
	}
	
	/**
	 * Get the number of features in method body only
	 * @return
	 */
	public int numOfFeaturesInBody() {
		return this.featureSet.getNumOfFeaturesInBody();
	}
	
	/**
	 * Get the number of features in method signature only
	 * @return
	 */
	public int numOfFeaturesInSignature() {
		return this.featureSet.getNumOfFeaturesInSignature();
	}

	public void setTokensInBody(int tokensInBody) {
		this.tokensInBody = tokensInBody;
	}

	public FeatureSet getFeatureSet() {
		return featureSet;
	}
	
	public TokenList getTokenList() {
		return tokenList;
	}
	
	public void setTokenList(TokenList tokenList) {
		this.tokenList = tokenList;
	}

	public List<String> getLocalVariables() {
		return localVariables;
	}
	
	/**
	 * Add feature 
	 * if the feature exists in the set, update the count 
	 * @param feature
	 */
	public void addFeature(Feature feature) {
		this.featureSet.addFeature(feature);
	}
	
	/**
	 * Add features
	 * @param featureSet
	 */
	public void addFeatures(FeatureSet featureSet) {
		if(featureSet != null && featureSet.getFeatures().size() > 0) {
			for(Feature feature : featureSet.getFeatures()) {
				addFeature(feature);
			}
		}
	}
	
	/**
	 * Add token
	 * @param token
	 */
	public void addToken(String token) {
		this.tokenList.addToken(token);
	}
	
	/**
	 * Add tokens
	 * @param tokenList
	 */
	public void addTokens(TokenList tokenList) {
		if(tokenList != null && tokenList.size() > 0) {
			for(String token : tokenList.getTokens()) {
				addToken(token);
			}
		}
	}
	
	/**
	 * Check whether variable is in the local variable list 
	 * @param variable
	 * @return
	 */
	public boolean isLocalVariableUse(String variable) {
		boolean isIn = false;
		for(String localVariable : localVariables) {
			if(localVariable.equals(variable)) {
				isIn = true;
				break;
			}
		}
		return isIn;
	}

	/**
	 * Add local variables 
	 * @param local
	 */
	public void addLocalVariable(String local) {
		this.localVariables.add(local);
	}

	/**
	 * Current implementation of hash code and equals
	 * use list of tokens extracted from method to calculate 
	 * hash code
	 */
	@Override
	public int hashCode() {
		return this.tokenList.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Method method = (Method) obj;
		return this.tokenList.equals(method.getTokenList());
	}

	/**
	 * Fully Qualified Method Name <tokens in method signature, tokens in method body>
	 * Feature
	 * Feature
	 * ...
	 */
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(getMethodName()).append("\n");
		for(Feature feature : featureSet.getFeatures()) {
			buffer.append(feature);
		}
		return buffer.toString();
	}
}
