package cn.edu.seu.ist.model.token;

import java.util.ArrayList;
import java.util.List;

/**
 * The representation of token list 
 * @author Dong Qiu
 *
 */
public class TokenList {
	private List<String> tokens;
	
	public TokenList() {
		this.tokens = new ArrayList<String>();
	}

	public TokenList(List<String> tokens) {
		this.tokens = tokens;
	}

	/* Getter and Setter */
	public List<String> getTokens() {
		return tokens;
	}

	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}
	
	/**
	 * Add tokens
	 * @param token
	 */
	public void addToken(String token) {
		this.tokens.add(token);
	}
	
	/**
	 * Get the size of tokens in the list
	 * @return
	 */
	public int size() {
		int size = 0;
		if(this.tokens != null) {
			size = this.tokens.size();
		}
		return size;
	}
	
	/**
	 * Calculate the hash code of the method
	 * Note: The hash code is calculated based on hash code of 
	 * tokens in both method signature and method body. No blank
	 * lines, whitespace and comments are included.   
	 * @return
	 */
	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TokenList other = (TokenList) obj;
		if (this.tokens == null) {
			if (other.tokens != null)
				return false;
		} 
		if(other.tokens == null) {
			if(this.tokens != null)
				return false;
		}
		if(this.tokens != null && other.tokens != null) {
			if(this.tokens.size() != other.tokens.size()) {
				return false;
			}
			else {
				for(int i=0; i<this.tokens.size(); i++) {
					if(!this.tokens.get(i).equals(other.tokens.get(i))) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for(String token : this.tokens) {
			buffer.append(token);
		}
		return buffer.toString();
	}
}
