/*
 * Copyright (c) 1999, 2008, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package cn.edu.seu.ist.model.token;

/**
 * Token Types 
 * @author Dong Qiu
 *
 */

public enum TokenType {
    //EOF,
    //ERROR,
	/* Identifier */
    IDENTIFIER("ID"),
    /* Keyword */
    KEYWORD("KW"),			// This is for high-level token types
    ABSTRACT("abstract"),
    ASSERT("assert"),
    BOOLEAN("boolean"),
    BREAK("break"),
    BYTE("byte"),
    CASE("case"),
    CATCH("catch"),
    CHAR("char"),
    CLASS("class"),
    CONST("const"),
    CONTINUE("continue"),
    DEFAULT("default"),
    DO("do"),
    DOUBLE("double"),
    ELSE("else"),
    ENUM("enum"),
    EXTENDS("extends"),
    FINAL("final"),
    FINALLY("finally"),
    FLOAT("float"),
    FOR("for"),
    GOTO("goto"),
    IF("if"),
    IMPLEMENTS("implements"),
    IMPORT("import"),
    INSTANCEOF("instanceof"),
    INT("int"),
    INTERFACE("interface"),
    LONG("long"),
    NATIVE("native"),
    NEW("new"),
    PACKAGE("package"),
    PRIVATE("private"),
    PROTECTED("protected"),
    PUBLIC("public"),
    RETURN("return"),
    SHORT("short"),
    STATIC("static"),
    STRICTFP("strictfp"),
    SUPER("super"),
    SWITCH("switch"),
    SYNCHRONIZED("synchronized"),
    THIS("this"),
    THROW("throw"),
    THROWS("throws"),
    TRANSIENT("transient"),
    TRY("try"),
    VOID("void"),
    VOLATILE("volatile"),
    WHILE("while"),
    /* Literal */
    LITERAL("LIT"),			// This is for high-level token types
    NUMERICLITERAL("NUMLIT"),		// Currently, we can not distinguish int, long, float, double yet
    INTLITERAL("INTLIT"),
    LONGLITERAL("LONGLIT"),
    FLOATLITERAL("FLLIT"),
    DOUBLELITERAL("DLIT"),
    CHARLITERAL("CHARLIT"),
    STRINGLITERAL("STRLIT"),
    TRUE("true"),
    FALSE("false"),
    NULL("null"),
    /* Separator */
    SEPARATOR("SEP"),			// This is for high-level token types
    LPAREN("("),
    RPAREN(")"),
    LBRACE("{"),
    RBRACE("}"),
    LBRACKET("["),
    RBRACKET("]"),
    SEMI(";"),
    COMMA(","),
    DOT("."),
    ELLIPSIS("..."),
    /* Operator*/
    OPERATOR("OP"),			// This is for high-level token types
    EQ("="),
    GT(">"),
    LT("<"),
    BANG("!"),
    TILDE("~"),
    QUES("?"),
    COLON(":"),
    EQEQ("=="),
    LTEQ("<="),
    GTEQ(">="),
    BANGEQ("!="),
    AMPAMP("&&"),
    BARBAR("||"),
    PLUSPLUS("++"),
    SUBSUB("--"),
    PLUS("+"),
    SUB("-"),
    STAR("*"),
    SLASH("/"),
    AMP("&"),
    BAR("|"),
    CARET("^"),
    PERCENT("%"),
    LTLT("<<"),
    GTGT(">>"),
    GTGTGT(">>>"),
    PLUSEQ("+="),
    SUBEQ("-="),
    STAREQ("*="),
    SLASHEQ("/="),
    AMPEQ("&="),
    BAREQ("|="),
    CARETEQ("^="),
    PERCENTEQ("%="),
    LTLTEQ("<<="),
    GTGTEQ(">>="),
    GTGTGTEQ(">>>="),
    MONKEYS_AT("@"),
    //CUSTOM;
    
    /* Refined Categories for Identifiers */
    METHODCALL("MCALL"),
    VARIABLE("VAR"),
    TYPE("TYPE"),
    OTHER("OTHER"),
    UNDEFINED("UNDEF"), 
    /* Refined Categories for keywords */
    CONTROL("CTRL"),
    REFERENCE("REF"),
    /* RefinedInfo for refinedLevel Type */
    LOCAL_TYPE("LTYPE"),
    GENERIC_TYPE("GTYPE"),
    LOCAL_CALL("LMCALL");
    
    TokenType() {
        this(null);
    }
    
    TokenType(String name) {
        this.name = name;
    }

    public final String name;
    
    /**
     * Find the low level token type from name
     * @param name
     * @return
     */
    public static TokenType findOperator(String name) {
    	TokenType type = null;
    	if(name.equals("=")) {
    		type = EQ;
    	} else if(name.equals(">")){
    		type = GT;
    	} else if(name.equals("<")){
    		type = LT;
    	} else if(name.equals("!")){
    		type = BANG;
    	} else if(name.equals("~")){
    		type = TILDE;
    	} else if(name.equals("?")){
    		type = QUES;
    	} else if(name.equals(":")){
    		type = COLON;
    	} else if(name.equals("==")){
    		type = EQEQ;
    	} else if(name.equals("<=")){
    		type = LTEQ;
    	} else if(name.equals(">=")){
    		type = GTEQ;
    	} else if(name.equals("!=")){
    		type = BANGEQ;
    	} else if(name.equals("&&")){
    		type = AMPAMP;
    	} else if(name.equals("||")){
    		type = BARBAR;
    	} else if(name.equals("++")){
    		type = PLUSPLUS;
    	} else if(name.equals("--")){
    		type = SUBSUB;
    	} else if(name.equals("+")){
    		type = PLUS;
    	} else if(name.equals("-")){
    		type = SUB;
    	} else if(name.equals("*")){
    		type = STAR;
    	} else if(name.equals("/")){
    		type = SLASH;
    	} else if(name.equals("&")){
    		type = AMP;
    	} else if(name.equals("|")){
    		type = BAR;
    	} else if(name.equals("^")){
    		type = CARET;
    	} else if(name.equals("%")){
    		type = PERCENT;
    	} else if(name.equals("<<")){
    		type = LTLT;
    	} else if(name.equals(">>")){
    		type = GTGT;
    	} else if(name.equals(">>>")){
    		type = GTGTGT;
    	} else if(name.equals("+=")){
    		type = PLUSEQ;
    	} else if(name.equals("-=")){
    		type = SUBEQ;
    	} else if(name.equals("*=")){
    		type = STAREQ;
    	} else if(name.equals("/=")){
    		type = SLASHEQ;
    	} else if(name.equals("&=")){
    		type = AMPEQ;
    	} else if(name.equals("|=")){
    		type = BAREQ;
    	} else if(name.equals("^=")){
    		type = CARETEQ;
    	} else if(name.equals("%=")){
    		type = PERCENTEQ;
    	} else if(name.equals("<<=")){
    		type = LTLTEQ;
    	} else if(name.equals(">>=")){
    		type = GTGTEQ;
    	} else if(name.equals(">>>=")){
    		type = GTGTGTEQ;
    	} else if(name.equals("@")){
    		type = MONKEYS_AT;
    	} 
    	return type;
    }
    
	/* Note: "<" and ">" are special operators here, if they are 
	 * used like List<String>, they should be considered as separators */
    public static TokenType findSeparator(String name) {
    	TokenType type = null;
    	if(name.equals("(")) {
    		type = LPAREN;
    	} else if(name.equals(")")) {
    		type = RPAREN;
    	} else if(name.equals("{")) {
    		type = LBRACE;
    	} else if(name.equals("}")) {
    		type = RBRACE;
    	} else if(name.equals("[")) {
    		type = LBRACKET;
    	} else if(name.equals("]")) {
    		type = RBRACKET;
    	} else if(name.equals("<")) {
    		type = LT;
    	} else if(name.equals(">")) {
    		type = GT;
    	} else if(name.equals(";")) {
    		type = SEMI;
    	} else if(name.equals(",")) {
    		type = COMMA;
    	} else if(name.equals(".")) {
    		type = DOT;
    	} else if(name.equals("...")) {
    		type = ELLIPSIS;
    	} 
    	
    	return type;
    }
    
    public static TokenType findKeyword(String name) {
    	TokenType type = null;
    	if(name.equals("abstract")) {
    		type = ABSTRACT;
    	} else if(name.equals("assert")) {
    		type = ASSERT;
    	} else if(name.equals("boolean")) {
    		type = BOOLEAN;
    	} else if(name.equals("break")) {
    		type = BREAK;
    	} else if(name.equals("byte")) {
    		type = BYTE;
    	} else if(name.equals("case")) {
    		type = CASE;
    	} else if(name.equals("catch")) {
    		type = CATCH;
    	} else if(name.equals("char")) {
    		type = CHAR;
    	} else if(name.equals("class")) {
    		type = CLASS;
    	} else if(name.equals("const")) {
    		type = CONST;
    	} else if(name.equals("continue")) {
    		type = CONTINUE;
    	} else if(name.equals("default")) {
    		type = DEFAULT;
    	} else if(name.equals("do")) {
    		type = DO;
    	} else if(name.equals("double")) {
    		type = DOUBLE;
    	} else if(name.equals("else")) {
    		type = ELSE;
    	} else if(name.equals("enum")) {
    		type = ENUM;
    	} else if(name.equals("extends")) {
    		type = EXTENDS;
    	} else if(name.equals("final")) {
    		type = FINAL;
    	} else if(name.equals("finally")) {
    		type = FINALLY;
    	} else if(name.equals("float")) {
    		type = FLOAT;
    	} else if(name.equals("for")) {
    		type = FOR;
    	} else if(name.equals("goto")) {
    		type = GOTO;
    	} else if(name.equals("if")) {
    		type = IF;
    	} else if(name.equals("implements")) {
    		type = IMPLEMENTS;
    	} else if(name.equals("import")) {
    		type = IMPORT;
    	} else if(name.equals("instanceof")) {
    		type = INSTANCEOF;
    	} else if(name.equals("int")) {
    		type = INT;
    	} else if(name.equals("interface")) {
    		type = INTERFACE;
    	} else if(name.equals("long")) {
    		type = LONG;
    	} else if(name.equals("native")) {
    		type = NATIVE;
    	} else if(name.equals("new")) {
    		type = NEW;
    	} else if(name.equals("package")) {
    		type = PACKAGE;
    	} else if(name.equals("private")) {
    		type = PRIVATE;
    	} else if(name.equals("protected")) {
    		type = PROTECTED;
    	} else if(name.equals("public")) {
    		type = PUBLIC;
    	} else if(name.equals("return")) {
    		type = RETURN;
    	} else if(name.equals("short")) {
    		type = SHORT;
    	} else if(name.equals("static")) {
    		type = STATIC;
    	} else if(name.equals("strictfp")) {
    		type = STRICTFP;
    	} else if(name.equals("super")) {
    		type = SUPER;
    	} else if(name.equals("switch")) {
    		type = SWITCH;
    	} else if(name.equals("synchronized")) {
    		type = SYNCHRONIZED;
    	} else if(name.equals("this")) {
    		type = THIS;
    	} else if(name.equals("throw")) {
    		type = THROW;
    	} else if(name.equals("throws")) {
    		type = THROWS;
    	} else if(name.equals("transient")) {
    		type = TRANSIENT;
    	} else if(name.equals("try")) {
    		type = TRY;
    	} else if(name.equals("void")) {
    		type = VOID;
    	} else if(name.equals("volatile")) {
    		type = VOLATILE;
    	} else if(name.equals("while")) {
    		type = WHILE;
    	} 
    	return type;
    }
    
    public static TokenType findRefinedKeyword(String name) {
    	TokenType type = null;
    	if(name.equals("break") || name.equals("continue") || name.equals("do")
    			|| name.equals("else") || name.equals("for") || name.equals("goto")
    			|| name.equals("if") || name.equals("return") || name.equals("switch")
    			|| name.equals("while") || name.equals("try") || name.equals("catch")
    			|| name.equals("finally") || name.equals("throw")) {
    		type = CONTROL;
    	} else if(name.equals("byte") || name.equals("boolean")  || name.equals("char")
    			 || name.equals("double") || name.equals("float") || name.equals("int")
    			 || name.equals("long") || name.equals("short") || name.equals("void")) {
    		type = TYPE;
    	} else if(name.equals("new") || name.equals("instanceof")) {
    		type = OPERATOR;
    	} else if(name.equals("super") || name.equals("this")) {
    		type = REFERENCE;
    	} else {
    		type = OTHER;
    	}
    	return type;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
