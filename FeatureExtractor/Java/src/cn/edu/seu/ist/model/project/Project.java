package cn.edu.seu.ist.model.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import cn.edu.seu.ist.model.feature.Feature;
import cn.edu.seu.ist.model.file.JavaFile;
import cn.edu.seu.ist.model.method.Method;
import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.statistics.GlobalStat;

/**
 * Representation of Java Project
 * @author Dong Qiu
 *
 */
public class Project {
	
	/* project name */
	private String projName;
	/* collection of java files */
	private Collection<JavaFile> javaFiles;
	/* number of all methods in the project */
	private int numOfMethods;
	/* number of all features in the project */
	private int numOfFeatures;
	/* number of all undefined features in the project */
	private int numOfUndefFeatures;
	
	/**
	 * Constructor
	 * @param projName
	 */
	public Project(String projName) {
		this.projName = projName;
		javaFiles = new ArrayList<JavaFile>();
		this.numOfFeatures = 0;
		this.numOfMethods = 0;
		this.numOfUndefFeatures = 0;
	}
	
	/* Getters */
	public String getName() {
		return projName;
	}
	
	public int numOfFiles() {
		return javaFiles.size();
	}

	public int numOfMethods() {
		return numOfMethods;
	}

	public int numOfFeatures() {
		return numOfFeatures;
	}

	public int numOfUndefFeatures() {
		return numOfUndefFeatures;
	}
	
	public Collection<JavaFile> getJavaFiles() {
		return javaFiles;
	}
	
	/* Setter */
	public void setName(String projName) {
		this.projName = projName;
	}

	/**
	 * Add Java Files into Project and perform 
	 * project-related statistics
	 * @param javaFile
	 */
	public void addJavaFile(JavaFile javaFile) {
		javaFiles.add(javaFile);
		// update the statistics of the project
		Collection<Method> methods = javaFile.getMethods();
		numOfMethods += methods.size();
		for(Method method : methods) {
			Set<Feature> features = method.getFeatureSet().getFeatures();
			for(Feature feature : features) {
				numOfFeatures += feature.getCount();
				if(feature.getRefinedLevel() == TokenType.UNDEFINED) {
					numOfUndefFeatures += feature.getCount();
				}
			}
			// Global Statistics
			GlobalStat.updateFromMethod(method);
		}
	}
	
	/**
	 * Translate the project summary info into list 
	 * @return
	 */
	public List<String> projSummaryInfo() {
		List<String> infos = new ArrayList<String>();
		infos.add(projName);
		infos.add(String.valueOf(numOfFiles()));
		infos.add(String.valueOf(numOfMethods()));
		infos.add(String.valueOf(numOfFeatures()));
		infos.add(String.valueOf(numOfUndefFeatures()));
		return infos;
	}
}
