package cn.edu.seu.ist.global;

import java.util.ArrayList;
import java.util.Collection;

public class CurrentFilePaths {
	private static Collection<String> filePaths = new ArrayList<String>();
	
	/**
	 * Clean the file paths
	 */
	public static void empty() {
		filePaths.clear();
	}
	
	public static void newFilePaths() {
		filePaths = new ArrayList<String>();
	}
	
	/**
	 * Add a file path
	 * @param filepath
	 */
	public static void addFilePath(String filepath) {
		filePaths.add(filepath);
	}
	
	/**
	 * Get the size of current files under analysis
	 * @return
	 */
	public static int size() {
		return filePaths.size();
	}
	
	/**
	 * get file list
	 * @return
	 */
	public static Collection<String> getFilePaths() {
		return filePaths;
	}
	
	public static void setFilePaths(Collection<String> currentPaths) {
		if(currentPaths != null && currentPaths.size() > 0) {
			filePaths = currentPaths;
		}
	}
	
	
}
