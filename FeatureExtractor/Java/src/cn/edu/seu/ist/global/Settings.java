package cn.edu.seu.ist.global;

public class Settings {
	/* Locations of configuration files */
	public static String SYSTEM_CONFIG = "conf/config.properties";
	public final static String LOG4J_CONFIG = "conf/log4j.properties";
	/* Scope of JDK Types and JDK Calls */
	public final static String JDK_SCOPE = "resources/jdk_package_prefix.txt";
	/* Files used to record the projects that are parsed correctly */
	public final static String FINISHED_OUTPUT = "finished.txt"; 
	/* Files used to record the projects that are not parsed correctly */
	public final static String UNFINISHED_OUTPUT = "unfinished.txt";
	/* Statistical result output name */
	public final static String CORPUS_OUTPUT = "CorpusStat.csv";
	public final static String METHOD_OUTPUT = "Methods.csv";
	public final static String COMPRESSED_METHOD_OUTPUT = "Compressed.csv";
	public final static String FEATURE_OUTPUT = "MethodFeature.csv";
	public final static String DUPLICATED_METHOD_OUTPUT = "DuplicatedMethods.txt";
	public final static String CLASS_OUTPUT = "Classes.txt";
	public final static String SUMMARY_OUTPUT = "summary.txt";
	public final static String LOC_DIS_OUTPUT = "LineDistribution.csv";
	public final static String LOC_INBODY_DIS_OUTPUT = "LineInBodyDistribution.csv";
	public final static String FEATURE_DIS_OUTPUT = "FeatureDistribution.csv";
	public final static String FEATURE_INBODY_DIS_OUTPUT = "FeatureInBodyDistribution.csv";
	public final static String POSSIBLE_DUPS_OUTPUT = "PossibleDuplicatedMethodID.csv";
	public final static String PROJECT_STATS_OUTPUT = "ProjectStats.csv";
	public final static String PROJECT_RUNTIME_OUTPUT = "ProjectRunTime.csv";
	public final static String PATH_STATE_OUTPUT = "PathStates.csv";
	public final static String UNIQUE_FILE_OUTPUT = "_unique.txt";
	public final static String DUPLICATED_FILE_OUTPUT = "_duplicated.txt";
	public final static String REMOVED_FILE_OUTPUT = "_removed.txt";
	/* Extension settings */
	public final static String CSV_EXTENSION = ".csv";
	public final static String TXT_EXTENSION = ".txt";
	public final static String JAVA_EXTENSION = "java";
	public final static String JAR_EXTENSION = "jar";
	/* Test version settings */
	public final static String TEST_VERSION = "_test";
	/* Log settings */
	//public static String LOG_OUTPUT_PATH = "log";
	
	public static String LOG_OUTPUT_PATH(String project_path){
		return project_path + "/log";
	}
}
