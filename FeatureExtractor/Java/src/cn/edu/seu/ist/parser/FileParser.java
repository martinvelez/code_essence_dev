package cn.edu.seu.ist.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

import cn.edu.seu.ist.model.file.JavaFile;
import cn.edu.seu.ist.util.PathUtil;
import cn.edu.seu.ist.visitor.FeatureVisitor;

/**
 * The class is used to parse one single file
 * @author Dong Qiu
 *
 */
public class FileParser extends AbstractParser {
	/* add the logger of the feature parser */
	static Logger logger = Logger.getLogger(FileParser.class.getName());
	/* The scope of the project under analysis */
	private String[] sourcepathEntries;
	/* All classpaths used for parsing the source files */
	private String[] classpathEntries;
	/* The paths of all source files under analysis */
	private String[] sourcepaths;
	
	/**
	 * Constructor
	 * @param sourcepathEntries
	 * @param classpathEntries
	 */
	public FileParser(String projName, String[] sourcepathEntries, 
			String[] classpathEntries, String[] sourcepaths) {
		super(projName);
		this.sourcepathEntries = sourcepathEntries;
		this.classpathEntries = classpathEntries;
		this.sourcepaths  = sourcepaths;
	}
	
	/**
	 * Constructor
	 * @param sourcepaths
	 * @param classpathEntries
	 */
	public FileParser(String projName, String[] sourcepathEntries, 
			Collection<String> classpathEntries, String[] sourcepaths) {
		super(projName);
		this.sourcepathEntries = sourcepathEntries;
		this.classpathEntries = classpathEntries.toArray(new String[classpathEntries.size()]);
		this.sourcepaths = sourcepaths;
	}
	
	/**
	 * Parse the project one file by one file
	 */
	@Override
	public void parse() {
		for(String sourcepath : sourcepaths) {
			requestor.addJavaFile(parseFile(sourcepath));
		}
	}
	
	/**
	 * Parse single java file
	 * @param sourcepath
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private JavaFile parseFile(String sourcepath) {
		// create a AST parser
		ASTParser pars = ASTParser.newParser(AST.JLS4);
		// set the environment for the AST parsers
		pars.setEnvironment(/*classpathEntries*/classpathEntries, /*sourcepathEntries*/sourcepathEntries, 
				/*encodings*/null, true);
		// enable binding
		pars.setResolveBindings(true);
		pars.setKind(ASTParser.K_COMPILATION_UNIT);
		// set the compiler option
		Map complierOptions= JavaCore.getOptions();
		JavaCore.setComplianceOptions(JavaCore.VERSION_1_7, complierOptions);
		pars.setCompilerOptions(complierOptions);
		FeatureVisitor visitor = new FeatureVisitor(sourcepath);
		try {
			pars.setSource(PathUtil.getSource(sourcepath));
			pars.setUnitName(new File(sourcepath).getName());
			CompilationUnit ast = (CompilationUnit) pars.createAST(null);
			ast.accept(visitor);
		} catch (FileNotFoundException fe) {
			logger.error("Error: Can't find the source file " + sourcepath, fe);
		} catch (IOException e) {
			logger.error("Error: Can't read the content of source file " + sourcepath, e);
		}
		return visitor.getJavaFile();
	}
}
