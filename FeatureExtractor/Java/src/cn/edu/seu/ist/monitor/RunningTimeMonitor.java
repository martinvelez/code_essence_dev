package cn.edu.seu.ist.monitor;


public class RunningTimeMonitor {
	private Runtime r = Runtime.getRuntime();
	private long startTime;
	

	public RunningTimeMonitor() {
		hook();
		this.startTime = System.currentTimeMillis();
	}

	public void hook() {
		r.addShutdownHook(new Thread() {
			public void run() {
				buildTimeReport();
			}
		});
	}

	protected void buildTimeReport() {
		long during = System.currentTimeMillis() - this.startTime; 
		long hour = during / 3600000;
		long min = (during % 3600000) / 60000; 
		long sec = (during % 60000) / 1000;
		long minsec = during % 1000;
		 
		System.out.println("Running time is " + hour + ":" + min + ":" + sec + ":" + minsec);
		
	}
}
