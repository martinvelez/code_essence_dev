package cn.edu.seu.ist.conf;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import cn.edu.seu.ist.global.Settings;
import cn.edu.seu.ist.util.PathUtil;
import cn.edu.seu.ist.util.statistics.GlobalStat;

/**
 * configuration of the system
 * @author Dong Qiu
 *
 */
public class ProjConfig {
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(ProjConfig.class.getName());
	
	private PropertiesConfiguration config;
	
	/* list of corpus paths */
	public static List<String> corpusPaths;
	/* list of project paths */
	public static List<String> projectPaths;
	/* list of project need to be ignored */
	public static List<String> ignoredProjects;
	/* output path of the results */
	public static String outputPath;
	/* output format */
	public static int outputFormat;
	/* list of classpaths */
	public static List<String> classpaths;
	/* execution mode */
	public static String executionMode;
	public static String log_output_path;
	
	public ProjConfig(String configPath) {
		logger.debug("ProjConfig(" + '"' + configPath + '"' + ')');
		try {
			logger.info("configPath = " + configPath);
			File configFile = new File(configPath).getCanonicalFile();
			if(!configFile.exists()) {
				logger.error("Error: config.properties does not exist in your specified path, please create one ... ");
				System.exit(-1);
			}
			config = new PropertiesConfiguration(configFile);
		} catch (IOException e) {
			logger.error("Error: Path of config.properties is illegal...", e);
			System.exit(-1);
		} catch (ConfigurationException e) {
			logger.error("Error: Can't load the config.properties...");
		}
	}
	
	/**
	 * Load the configuration properties of Project
	 */
    public void initProjConfig(String projectPath, String outputDirPath) {
		corpusPaths = Arrays.asList(config.getStringArray("corpus.path"));
		projectPaths = new ArrayList<String>(Arrays.asList(config.getStringArray("project.path")));
		if(projectPath != null){
			logger.info(projectPath);
			try{
			projectPaths.add(projectPath);
			} catch (UnsupportedOperationException e){
				logger.error(e.getMessage());
				logger.error(e.getCause());
			}
		}
		logger.info("projectPaths = " + projectPaths);
		
		ignoredProjects = Arrays.asList(config.getStringArray("project.ignore"));		
		outputFormat = config.getInt("output.format", 0);
		executionMode = config.getString("execution.mode");
		if(projectPath != null){
			log_output_path = projectPath + "/log";
		}
		
		prepareClasspaths();
		prepareOutputDir(outputDirPath);
		prepareHistory();
		validation();
    }
    
    private void prepareHistory() {
    	// boolean variable for whether loading history or not
    	boolean loadHistory = config.getBoolean("execution.load.history", false);
    	if(loadHistory) {
    		File file = PathUtil.prepareFile(log_output_path, Settings.FINISHED_OUTPUT);
    		if(file.exists()) {
    			try {
    				CSVReader csvReader = new CSVReader(new FileReader(file));
    				List<String[]> lines = csvReader.readAll();
    				for(String[] line : lines) {
    					if(line != null && line[0] != null) {
    						GlobalStat.addFinishedProj(line[0]);
    					}
    				}
    				csvReader.close();
    			} catch (IOException e) {
    				logger.error("Error: Can't load the finished projects from " + Settings.FINISHED_OUTPUT, e);
    			}
    		} else {
    			logger.error("Error: No history file " + Settings.FINISHED_OUTPUT + " is found!");
    		}
    	}
    	
	}

	/**
     * Check whether the properties in the configuration file is valid
     */
    private void validation() {
    	if(corpusPaths == null && projectPaths == null) {
			logger.error("ERROR: No corpus or project path are set in config.properties!");
			System.exit(-1);
		}
    }
    
    /**
     * Prepare for the output directory
     */
    private void prepareOutputDir(String outputDirPath) {
    	String output = config.getString("output.path", "output");
    	if(outputDirPath != null){
    		output = outputDirPath;
    	}
    	File outputDir = new File(output);
    	if(!outputDir.exists()) {
    		outputDir.mkdir();
    	}
    	try {
			outputPath = outputDir.getCanonicalPath();
		} catch (IOException e) {
			logger.error("Error: Can't get the canonical path of the output.path", e);
			logger.info("Info: Please check the value of output.path... ");
			System.exit(-1);
		}
    }
    
    /**
     * Prepare for all classpaths
     */
    private void prepareClasspaths() {
    	classpaths = new ArrayList<String>();
    	boolean classpathUse = config.getBoolean("classpath.use", false);
    	if(classpathUse) {
    		List<String> classpathDirs = Arrays.asList(config.getStringArray("classpath.dir"));
    		List<String> classpathFiles = Arrays.asList(config.getStringArray("classpath.file"));
    		// get lists of classpath entries from classpath files
    		if(classpathFiles != null && classpathFiles.size() > 0) {
    			classpaths.addAll(classpathFiles);
    		}
    		// get lists of classpath entries from classpath dirs
    		if(classpathDirs != null && classpathDirs.size() > 0) {
    			for(String classpathDir : classpathDirs) {
    				File classpathDirFile = new File(classpathDir);
    				if(classpathDirFile.isDirectory()) {
    					classpaths.addAll(PathUtil.listFilePaths(classpathDirFile, new String[]{Settings.JAR_EXTENSION}));
    				}
    			}
    		}
    	}
    }
}
