package cn.edu.seu.ist.conf;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import cn.edu.seu.ist.global.Settings;

public class Log4jConfig {
	
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(Log4jConfig.class.getName());
	
	/**
	 * Load the configuration properties of log4j
	 */
    public static void initLog4j() {
        try {
            Properties props = new Properties();
            props.load(Log4jConfig.class.getClassLoader().getResourceAsStream(Settings.LOG4J_CONFIG));
            PropertyConfigurator.configure(props);
        } catch (Exception ex) {
        	logger.error("Error: Can't load the log4j configuration file!", ex);
        }
     }
}
