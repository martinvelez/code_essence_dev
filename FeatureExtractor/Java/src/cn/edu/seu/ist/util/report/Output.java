package cn.edu.seu.ist.util.report;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import cn.edu.seu.ist.util.PathUtil;

public class Output {
	
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(Output.class.getName());
	
	/**
	 * Output content to file
	 * @param dirPath
	 * @param fileName
	 * @param content
	 */
	public static void outputString(String dirPath, String fileName, String content) {
		File outputFile = PathUtil.prepareFile(dirPath, fileName);
		try {
			FileUtils.writeStringToFile(outputFile, content);
		} catch (IOException e) {
			logger.error("Error: Can't write into file " + outputFile.getAbsolutePath(), e);
		}
	}
}
