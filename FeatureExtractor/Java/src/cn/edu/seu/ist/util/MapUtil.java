package cn.edu.seu.ist.util;

import java.util.Map;

public class MapUtil {
	
	/**
	 * Update the number of element key in the tree map
	 * @param distri
	 * @param key
	 * @param count
	 */
	public static <E> void updateDistribution(Map<E, Integer> distri, E key, int count) {
		if(distri.containsKey(key)) {
			Integer value = distri.get(key);
			distri.put(key, value + count);
		} else {
			distri.put(key, count);
		}
	}
	
	/**
	 * Update the number of element key in the tree map
	 * @param distri
	 * @param key
	 * @param count=1
	 */
	public static <E> void updateDistribution(Map<E, Integer> distri, E key) {
		updateDistribution(distri, key, 1);
	}
}
