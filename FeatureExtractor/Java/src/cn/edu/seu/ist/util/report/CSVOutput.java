package cn.edu.seu.ist.util.report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.log4j.Logger;

import cn.edu.seu.ist.util.PathUtil;

import au.com.bytecode.opencsv.CSVWriter;


public class CSVOutput {
	
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(CSVOutput.class.getName());
	
	/**
	 * Output the content(collection) in into file with CSV format
	 * @param dirPath
	 * @param fileName
	 * @param collections
	 */
	public static <E> void outputCollection(String dirPath, String fileName, Collection<E> collections) {
		File outputFile = PathUtil.prepareFile(dirPath, fileName);
		try {
			CSVWriter csvWriter = new CSVWriter(new FileWriter(outputFile));
			String[] line = new String[1];
			for(E e : collections) {
				line[0] = e.toString();
				csvWriter.writeNext(line);
			}
			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			logger.error("Error: Can't write into file " + outputFile.getAbsolutePath(), e);
		}
	}
	
	/**
	 * Output the content(map) in into file with CSV format
	 * @param dirPath
	 * @param fileName
	 * @param map
	 */
	public static <K,V> void outputMap(String dirPath, String fileName, AbstractMap<K,V> map) {
		File outputFile = PathUtil.prepareFile(dirPath, fileName);
		try {
			CSVWriter csvWriter = new CSVWriter(new FileWriter(outputFile));
			String[] line = new String[2];
			for(K key : map.keySet()) {
				line[0] = key.toString();
				line[1] = map.get(key).toString();
				csvWriter.writeNext(line);
			}
			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			logger.error("Error: Can't write into file " + outputFile.getAbsolutePath(), e);
		}
	}
	
	/**
	 * Output the content(table) in into file with CSV format
	 * @param dirPath
	 * @param fileName
	 * @param table
	 */
	public static <K,V> void outputTable(String dirPath, String fileName, Hashtable<K,Vector<V>> table) {
		File outputFile = PathUtil.prepareFile(dirPath, fileName);
		try {
			CSVWriter csvWriter = new CSVWriter(new FileWriter(outputFile));
			for(K key : table.keySet()) {
				Collection<V> collections = table.get(key);
				if(collections.size() > 1) {
					String[] line = new String[collections.size()];
					int index = 0;
					for(V v : collections) {
						line[index] = v.toString();
						index ++;
					}
					csvWriter.writeNext(line);
				}
			}
			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			logger.error("Error: Can't write into file " + outputFile.getAbsolutePath(), e);
		}
	}	
	
	public static <T> void outputCollections(String dirPath, String fileName, Collection<Collection<T>> collections) {
		File outputFile = PathUtil.prepareFile(dirPath, fileName);
		try {
			CSVWriter csvWriter = new CSVWriter(new FileWriter(outputFile));
			for(Collection<T> collection : collections) {
				String[] line = new String[collection.size()];
				int index = 0;
				for(T t : collection) {
					line[index] = t.toString();
					index ++;
				}
				csvWriter.writeNext(line);
			}
			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			logger.error("Error: Can't write into file " + outputFile.getAbsolutePath(), e);
		}
		
	}
}
