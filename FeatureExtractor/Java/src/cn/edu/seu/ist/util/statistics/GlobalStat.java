package cn.edu.seu.ist.util.statistics;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import cn.edu.seu.ist.model.method.Method;
import cn.edu.seu.ist.util.MapUtil;

public class GlobalStat {
	/* Universal ID generated for different methods */
	private static long mid = 0;
	/* Universal ID generated for different project */
	private static int pid = 0;
	/* distribution of LoC in the whole method */
	private static AbstractMap<Integer, Integer> locDis = new TreeMap<Integer, Integer>();
	/* distribution of LoC in method body only  */
	private static AbstractMap<Integer, Integer> locInBodyDis = new TreeMap<Integer, Integer>();
	/* distribution of tokens in the whole method  */
	private static AbstractMap<Integer, Integer> featureDis = new TreeMap<Integer, Integer>();
	/* distribution of tokens in method body only */
	private static AbstractMap<Integer, Integer> featureInBodyDis = new TreeMap<Integer, Integer>();
	/* list of project statistics*/
	private static Collection<Collection<String>> projectStats = new ArrayList<Collection<String>>();
	/* running time of each project, key is the name of the project */
	private static AbstractMap<String, Long> runTimes = new HashMap<String, Long>();
	/* Total runtime of the whole process */
	private static long totalRuntime = 0;
	/* analyzed project of feature extractor */
	private static List<String> finishedProjs = new ArrayList<String>();
	/* projects that are not parsed correctly */
	private static List<String> unFinishedProjs = new ArrayList<String>();
	/* duplicated method based on their hash code value */
	private static Map<Integer, Collection<Long>> possibleDups = 
			Collections.synchronizedMap(new HashMap<Integer, Collection<Long>>());

	/* Getters and Setters */
	public static long getMid() {
		return GlobalStat.mid;
	}
	
	public static void setMid(long mid) {
		GlobalStat.mid = mid;
	}

	public static void increaseMid() {
		GlobalStat.mid ++;
	}
	
	public static void decreaseMid() {
		GlobalStat.mid --;;
	}
	
	public static long getPid() {
		return GlobalStat.pid;
	}
	
	public static void setPid(int pid) {
		GlobalStat.pid = pid;
	}

	public static void increasePid() {
		GlobalStat.pid ++;
	}
	
	public static void decreasePid() {
		GlobalStat.pid --;
	}
	
	public static AbstractMap<Integer,Integer> getLocDis() {
		return GlobalStat.locDis;
	}
	
	public static void updateLoCDis(int loc) {
		MapUtil.<Integer>updateDistribution(GlobalStat.locDis, loc);
	}
	
	public static AbstractMap<Integer, Integer> getLocInBodyDis() {
		return locInBodyDis;
	}
	
	public static void updateLocInBodyDis(int loc) {
		MapUtil.<Integer>updateDistribution(GlobalStat.locInBodyDis, loc);
	}

	public static AbstractMap<Integer, Integer> getFeatureDis() {
		return featureDis;
	}
	
	public static void updateFeatureDis(int num) {
		MapUtil.<Integer>updateDistribution(GlobalStat.featureDis, num);
	}

	public static AbstractMap<Integer, Integer> getFeatureInBodyDis() {
		return featureInBodyDis;
	}
	
	
	public static void updateFeatureInBodyDis(int num) {
		MapUtil.<Integer>updateDistribution(GlobalStat.featureInBodyDis, num);
	}
	
	public static Collection<Collection<String>> getProjectStats() {
		return projectStats;
	}
	
	public static void addProjectStat(Collection<String> coll) {
		projectStats.add(coll);
	}

	/**
	 * Update the total runtime
	 * @param runtime
	 */
	public static void updateTotalRuntime(long runtime) {
		totalRuntime += runtime;
	}
	
	public static long getTotalRuntime() {
		return totalRuntime;
	}
	
	public static AbstractMap<String, Long> getRunTimes() {
		return runTimes;
	}
	
	public static void AddRunTime(String projName, long runTime) {
		runTimes.put(projName, runTime);	
	}
	
	
	public static List<String> getFinishedProjs() {
		return finishedProjs;
	}

	/**
	 * Update the list of finished projects
	 * @param project
	 */
	public static void addFinishedProj(String project) {
		finishedProjs.add(project);
	}
	
	public static List<String> getUnFinishedProjs() {
		return unFinishedProjs;
	}

	/**
	 * Update the list of unfinished projects
	 * @param project
	 */
	public static void addUnfinishedProjs(String project) {
		unFinishedProjs.add(project);
	}
	
	public static Map<Integer, Collection<Long>> getPossibleDups() {
		return possibleDups;
	}
	
	public static void updatePossibleDups(int hashcode, long mid) {
		if(possibleDups.containsKey(hashcode)) {
			Collection<Long> duplicates = possibleDups.get(hashcode);
			duplicates.add(mid);
		} else {
			Collection<Long> duplicates = Collections.synchronizedList(new ArrayList<Long>());
			duplicates.add(mid);
			possibleDups.put(hashcode, duplicates);
		}
	}
	
	/**
	 * Update data from method definition
	 * @param method
	 */
	public static void updateFromMethod(Method method) {
		// collect the LoC distribution for the whole corpus
		GlobalStat.updateLoCDis(method.getLines());
		// collect the LoC (method body only) distribution for the whole corpus
		GlobalStat.updateLocInBodyDis(method.getLinesInBody());
		// collect the method token distribution for the whole corpus
		GlobalStat.updateFeatureDis(method.numOfFeatures());
		// collect the method token (in method body only) distribution for the whole corpus
		GlobalStat.updateFeatureInBodyDis(method.numOfFeaturesInBody());
		if(method.numOfFeaturesInBody() >= 30) {
			// check the possible duplicated methods 
			GlobalStat.updatePossibleDups(method.hashCode(), method.getMid());
		}
	}
}
