package cn.edu.seu.ist.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;


public class PathUtil {
	
	/* add the logger of feature extractor */
	static Logger logger = Logger.getLogger(PathUtil.class.getName());
	
	/**
	 * Get list of paths of all files with given extensions under dirPath 
	 * @param dirPath
	 * @param extensions
	 * @return
	 */
	public static Collection<String> listFilePaths(File dirPath, String[] extensions) {
		logger.info("dirPath = " + dirPath);
		Collection<File> filesUnderDir = FileUtils.listFiles(dirPath, extensions, true);
		Collection<String> filePathsUnderDir = new ArrayList<String>();
		for(File sourceFile : filesUnderDir) {
			filePathsUnderDir.add(sourceFile.getAbsolutePath());
		}
		return filePathsUnderDir;
	}
	
	/**
	 * Get the char array of the file
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static char[] getSource(String filePath) throws IOException  {
		File file = new File(filePath);
		FileInputStream fis = new FileInputStream(file);
		FileChannel fChannel = fis.getChannel();
	    byte[] barray = new byte[(int) file.length()];
	    ByteBuffer bb = ByteBuffer.wrap(barray);
	    fChannel.read(bb);
	    fis.close();
	    return new String(barray).toCharArray();
	}
	
	/**
	 * Generate the dirs and output file according to the dir path and file name
	 * @param dirPath
	 * @param fileName
	 * @return
	 */
	public static File prepareFile(String dirPath, String fileName) {
		File outputDir = new File(dirPath);
		if(!outputDir.exists()) {
			outputDir.mkdirs();
		}
		File outputFile = FileUtils.getFile(outputDir, fileName);
		if(!outputFile.exists()) {
			try {
				outputFile.createNewFile();
			} catch (IOException e) {
				logger.error("Error: Can't create the file " + outputFile.getAbsolutePath(), e);
			}
		}
		return outputFile;
	}
	
	public static File prepareDir(String rootPath, String subDir) {
		File rootDir = new File(rootPath);
		if(!rootDir.exists()) {
			rootDir.mkdirs();
		}
		File outputDir = FileUtils.getFile(rootDir, subDir);
		if(!outputDir.exists()) {
			outputDir.mkdir();
		}
		return outputDir;
	}
}
