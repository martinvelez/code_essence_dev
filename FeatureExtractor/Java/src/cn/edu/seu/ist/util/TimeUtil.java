package cn.edu.seu.ist.util;

public class TimeUtil {
	
	/**
	 * Translate the millisecond to the format like HH:MM:SS:MS
	 * @param millisecond
	 * @return
	 */
	public static String formatIntoHHMMSS(long millisecond) {
		
		long hours = millisecond / 3600000;
		long remainder = millisecond % 3600000;
		long minutes = remainder / 60000;
		remainder = remainder % 60000;
		long seconds = remainder / 1000;
		long msecends = remainder % 1000;
		
		
		StringBuffer timeBuffer = new StringBuffer();
		timeBuffer.append((hours < 10 ? "0" : "")).append(hours).append("H").append(":")
			.append((minutes < 10 ? "0" : "")).append(minutes).append("M").append(":")
			.append(seconds< 10 ? "0" : "").append(seconds).append("S").append(":");
		if(msecends < 100)
			timeBuffer.append("0");
		if(msecends < 10 )
			timeBuffer.append("0");
		timeBuffer.append(msecends).append("MS");
		
		return timeBuffer.toString();
	
	}

}
