package cn.edu.seu.ist.util.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import cn.edu.seu.ist.util.checker.JDKChecker;

public class JDKCheckerTest {

	@Test
	public void testIsFromJDK() {
		assertTrue(JDKChecker.isFromJDK("java.lang"));
		assertTrue(JDKChecker.isFromJDK("java.io.File"));
	}

}
