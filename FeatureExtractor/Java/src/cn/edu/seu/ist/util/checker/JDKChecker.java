package cn.edu.seu.ist.util.checker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cn.edu.seu.ist.global.Settings;

public class JDKChecker {
	
	public static List<String> JDKPrefixes = new ArrayList<String>();
	
	// load the JDK list
	static {
		/*try {
			JDKPrefixes = FileUtils.readLines(new File(Settings.JDK_SCOPE));
			System.out.println(JDKPrefixes.size());
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		try {
			InputStreamReader isr = new InputStreamReader(JDKChecker.class.getClassLoader().getResourceAsStream(Settings.JDK_SCOPE));
			BufferedReader reader = new BufferedReader(isr);
			String line = reader.readLine();
			while(line != null) {
				JDKPrefixes.add(line.trim());
				line = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Check whether type or method call are from JDK definition
	 * @param qualifiedName
	 * @return
	 */
	public static boolean isFromJDK(String qualifiedName) {
		if(qualifiedName == null) 
			return false;
		boolean isIn = false;
		for(String prefix : JDKPrefixes) {
			if(qualifiedName.startsWith(prefix)) {
				isIn = true;
				break;
			}
		}
		return isIn;
	}
}
