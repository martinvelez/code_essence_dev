package cn.edu.seu.ist.util.checker.test;

import static org.junit.Assert.*;

import org.junit.Test;

import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.checker.NumericChecker;

public class NumericCheckerTest {

	@Test
	public void testGetNumericType() {
		assertEquals(TokenType.INTLITERAL, NumericChecker.getNumericType("0"));
		assertEquals(TokenType.INTLITERAL, NumericChecker.getNumericType("0372"));
		assertEquals(TokenType.INTLITERAL, NumericChecker.getNumericType("0xDada_Cafe"));
		assertEquals(TokenType.INTLITERAL, NumericChecker.getNumericType("1996"));
		assertEquals(TokenType.INTLITERAL, NumericChecker.getNumericType("0x00_FF__00_FF"));
		
		assertEquals(TokenType.LONGLITERAL, NumericChecker.getNumericType("0l"));
		assertEquals(TokenType.LONGLITERAL, NumericChecker.getNumericType("0777L"));
		assertEquals(TokenType.LONGLITERAL, NumericChecker.getNumericType("0x100000000L"));
		assertEquals(TokenType.LONGLITERAL, NumericChecker.getNumericType("2_147_483_648L"));
		assertEquals(TokenType.LONGLITERAL, NumericChecker.getNumericType("0xC0B0L"));
		
		assertEquals(TokenType.FLOATLITERAL, NumericChecker.getNumericType("6.022137e+23f"));
		assertEquals(TokenType.FLOATLITERAL, NumericChecker.getNumericType("3.14f"));
		assertEquals(TokenType.FLOATLITERAL, NumericChecker.getNumericType("0f"));
		assertEquals(TokenType.FLOATLITERAL, NumericChecker.getNumericType(".3f"));
		assertEquals(TokenType.FLOATLITERAL, NumericChecker.getNumericType("2.f"));
		assertEquals(TokenType.FLOATLITERAL, NumericChecker.getNumericType("1e1f"));
		
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("1e1"));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("2."));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType(".3"));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("0.0"));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("3.14"));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("1e-9d"));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("1e137"));
		
		
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("1.1"));
		assertEquals(TokenType.DOUBLELITERAL, NumericChecker.getNumericType("1d"));
		
		
	}

}
