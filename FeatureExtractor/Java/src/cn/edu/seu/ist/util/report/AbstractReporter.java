package cn.edu.seu.ist.util.report;

import java.lang.reflect.Constructor;

import org.apache.log4j.Logger;

import cn.edu.seu.ist.model.project.Project;

public abstract class AbstractReporter {
	/* logger */
	static Logger logger = Logger.getLogger(AbstractReporter.class.getName());
	
	private static String reporterPackage = "cn.edu.seu.ist.util.report";
	private static String reporterBase = "Reporter";
	/* The output path */
	protected String outputPath;
	/* The output format */
	protected int format;
	/* Collection of java information extracted from projects */
	protected Project project;

	public AbstractReporter(String outputPath, Project project, int format) {
		this.outputPath = outputPath;
		this.project = project;
		this.format = format;
	}
	
	/**
	 *  Prepare reporter object according to the executionMode
	 * @param executionMode
	 * @param constrParams
	 * @return
	 */
	public static AbstractReporter prepareClass(String executionMode, Object[] constrParams) {
		AbstractReporter reporter = null;
		StringBuffer buffer = new StringBuffer();
		buffer.append(reporterPackage).append(".").append(executionMode).append(reporterBase);
		try {
			Class<?> reporterClass = Class.forName(buffer.toString());
			Constructor<?> constr = reporterClass.getConstructor(new Class[]{String.class, Project.class, int.class});
			reporter = (AbstractReporter)constr.newInstance(constrParams);
		} catch (Exception e) {
			logger.error("The execution mode " + executionMode + " is unavailable now... ");
			System.exit(-1);
		}
		return reporter;
	}
	
	public abstract void report();
}
