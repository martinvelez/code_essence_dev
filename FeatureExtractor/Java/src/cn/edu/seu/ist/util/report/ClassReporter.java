package cn.edu.seu.ist.util.report;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import cn.edu.seu.ist.global.Settings;
import cn.edu.seu.ist.model.file.JavaFile;
import cn.edu.seu.ist.model.project.Project;
import cn.edu.seu.ist.util.PathUtil;

public class ClassReporter extends AbstractReporter {
	
	/* logger */
	static Logger logger = Logger.getLogger(ClassReporter.class.getName());

	public ClassReporter(String outputPath, Project project, int format) {
		super(outputPath, project, format);
	}

	@Override
	public void report() {
		File classOutputFile = PathUtil.prepareFile(outputPath, 
				Settings.CLASS_OUTPUT);
		try {
			FileUtils.writeLines(classOutputFile, prepareClasses());
		} catch (IOException e) {
			logger.error("Error: Can't write into file " + Settings.CLASS_OUTPUT, e);
		}
	}
	
	private Collection<String> prepareClasses() {
		Collection<String> allClasses = new ArrayList<String>();
		for(JavaFile javaFile : project.getJavaFiles()) {
			allClasses.addAll(javaFile.getClassNames());
		}
		return allClasses;
	}

}
