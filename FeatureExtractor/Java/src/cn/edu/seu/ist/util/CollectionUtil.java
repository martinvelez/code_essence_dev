package cn.edu.seu.ist.util;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionUtil {
	
	/**
	 * Test whether element e is in the list of elements
	 * @param element
	 * @param elements
	 * @return
	 */
	public static <E> boolean isInList(E element, Collection<E> elements) {
		boolean isIn = false;
		if(elements != null && elements.size() > 0) {
			for(E e : elements) {
				if(e.equals(element)) {
					isIn = true;
					break;
				}
			}
		}
		return isIn;
	}
	
	/**
	 * Filter the collection whose size is less than filter size
	 * @param collections
	 * @param size
	 * @return
	 */
	public static <T> Collection<Collection<T>> filter(Collection<Collection<T>> collections, int size) {
		Collection<Collection<T>> newColls = new ArrayList<Collection<T>>();
		for(Collection<T> collection : collections) {
			if(collection.size() > size) {
				newColls.add(collection);
			}
		}
		return newColls;
	}
}
