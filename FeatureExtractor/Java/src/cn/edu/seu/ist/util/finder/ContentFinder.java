package cn.edu.seu.ist.util.finder;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;


public class ContentFinder {
	
	/**
	 * Find keyword in files with selected extensions under dirPath 
	 * @param dirPath
	 * @param extensions
	 * @param keyword
	 */
	public static void find(String dirPath, String[] extensions, String keyword) {
		boolean found = false;
		Collection<File> files = FileUtils.listFiles(new File(dirPath), extensions, true);
		for(File file : files) {
			List<String> lines;
			try {
				lines = FileUtils.readLines(file);
				for(String line : lines) {
					if(line != null && line.equals(keyword)) {
						System.out.println(file.getAbsolutePath());
						found = true;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(!found) {
			System.out.println("No file contains keyword [" + keyword + "]");
		}
	}
	
}
