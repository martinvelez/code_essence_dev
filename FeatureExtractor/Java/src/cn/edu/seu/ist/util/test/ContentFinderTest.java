package cn.edu.seu.ist.util.test;

import org.junit.Test;

import cn.edu.seu.ist.util.finder.ContentFinder;

public class ContentFinderTest {

	@Test
	public void testFind() {
		String dirPath = "D:\\Working\\Papers\\code_essence\\dev\\data";
		String keyword = "org.hibernate.internal.SessionFactoryImpl.IntegratorObserver.sessionFactoryCreated(org.hibernate.SessionFactory)";
		
		ContentFinder.find(dirPath, new String[]{"java"}, keyword);
	}

}
