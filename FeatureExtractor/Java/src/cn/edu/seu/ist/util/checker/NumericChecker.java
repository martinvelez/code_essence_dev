package cn.edu.seu.ist.util.checker;

import cn.edu.seu.ist.model.token.TokenType;

public class NumericChecker {
	
	/**
	 * Get the concrete category of numeric literal (IntLiteral, LongLiteral, FloatLiteral, DoubleLiteral)
	 * @param numericValue
	 * @return
	 */
	public static TokenType getNumericType(String numericValue) {
		TokenType type = null;
		if(numericValue.endsWith("L") || numericValue.endsWith("l")) {
			type = TokenType.LONGLITERAL;
		} else if(numericValue.endsWith("F") || numericValue.endsWith("f")){
			if(numericValue.startsWith("0X") || numericValue.startsWith("0x")) {
				if(numericValue.contains("P") || numericValue.contains("p")) {
					type = TokenType.FLOATLITERAL;
				} else {
					type = TokenType.INTLITERAL;
				}
			} else {
				type = TokenType.FLOATLITERAL;
			}
		} else if(numericValue.endsWith("D") || numericValue.endsWith("d")) {
			if(numericValue.startsWith("0X") || numericValue.startsWith("0x")) {
				if(numericValue.contains("P") || numericValue.contains("p")) {
					type = TokenType.DOUBLELITERAL;
				} else {
					type = TokenType.INTLITERAL;
				}
			} else {
				type = TokenType.DOUBLELITERAL;
			}
		} else {
			if(numericValue.contains(".")) {
				type = TokenType.DOUBLELITERAL;
			} else {
				if(numericValue.contains("p") || numericValue.contains("P")) {
					type = TokenType.DOUBLELITERAL;
				} else {
					if(numericValue.contains("e") || numericValue.contains("E")) {
						if(numericValue.startsWith("0X") || numericValue.startsWith("0x")) {
							type = TokenType.INTLITERAL;
						} else {
							type = TokenType.DOUBLELITERAL;
						}
					} else {
						type = TokenType.INTLITERAL;
					}
				}
			}
		}
		return type;
	}
}
