package cn.edu.seu.ist.util.report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVWriter;
import cn.edu.seu.ist.global.Settings;
import cn.edu.seu.ist.model.feature.Feature;
import cn.edu.seu.ist.model.file.JavaFile;
import cn.edu.seu.ist.model.method.Method;
import cn.edu.seu.ist.model.project.Project;
import cn.edu.seu.ist.util.PathUtil;

public class FeatureReporter extends AbstractReporter{
	/* logger */
	static Logger logger = Logger.getLogger(FeatureReporter.class.getName());
	
	
	public FeatureReporter(String outputPath, Project project, int format) {
		super(outputPath, project, format);
	}
	
	/**
	 * Report the results according to the output format
	 */
	public void report() {
		switch(format) {
			case 0: 
				reportMR();
				break;
			case 1:
				reportHR();
				break;
			default:
				break;
		}
	}
	
	/**
	 * Report machine readable output
	 */
	private void reportMR() {
		// generate the output files for methods
		File methodOutputFile = PathUtil.prepareFile(outputPath, Settings.METHOD_OUTPUT);
		CSVWriter methodWriter = null;
		try {
			methodWriter = new CSVWriter(new FileWriter(methodOutputFile));
		} catch (IOException e) {
			logger.error("Error: Fail when open the file " + methodOutputFile.getAbsolutePath(), e);
		} 
		
		// generate the output files for method features
		File featureOutputFile = new File(new File(outputPath), Settings.FEATURE_OUTPUT);
		CSVWriter featureWriter = null;
		try {
			featureWriter = new CSVWriter(new FileWriter(featureOutputFile, true));
		} catch (IOException e) {
			logger.error("Error: Fail when open the file " + featureOutputFile.getAbsolutePath(), e);
		} 
		
		// generate the output files for compressed source code of method
		File compressedMethodOutputFile = PathUtil.prepareFile(outputPath, Settings.COMPRESSED_METHOD_OUTPUT);
		CSVWriter compressedWriter = null;
		try {
			compressedWriter = new CSVWriter(new FileWriter(compressedMethodOutputFile, true));
		} catch (IOException e) {
			logger.error("Error: Fail when open the file " + compressedMethodOutputFile.getAbsolutePath(), e);
		} 
		
		for(JavaFile javaFile : project.getJavaFiles()) {
			for(Method method : javaFile.getMethods()) {
				String[] methodContent = new String[5];
				methodContent[0] = String.valueOf(method.getMid());
				methodContent[1] = method.getQualifiedMethodName();
				methodContent[2] = javaFile.getSourceFilePath();
				methodContent[3] = String.valueOf(method.getStartLine());
				methodContent[4] = String.valueOf(method.getEndLine());
				methodWriter.writeNext(methodContent);
				
				String[] compressMethodContent = new String[2];
				compressMethodContent[0] = String.valueOf(method.getMid());
				compressMethodContent[1] = method.getTokenList().toString();
				compressedWriter.writeNext(compressMethodContent);
				
				Set<Feature> features = method.getFeatureSet().getFeatures();
				for(Feature feature: features) {
					String[] featureContent = new String[8];
					featureContent[0] = String.valueOf(method.getMid());
					featureContent[1] = (feature.isInBody()) ? "1" : "0";
					featureContent[2] = feature.getLexeme();
					featureContent[3] = feature.getHighLevel().toString();
					featureContent[4] = feature.getLowLevel().toString();
					featureContent[5] = (feature.getRefinedLevel() != null) ? feature.getRefinedLevel().toString() : "";
					featureContent[6] = (feature.getRefinedInfo() != null) ? feature.getRefinedInfo() : "";
					featureContent[7] = new Integer(feature.getCount()).toString();
					featureWriter.writeNext(featureContent);
				}
			}
		}
		IOUtils.closeQuietly(methodWriter);
		IOUtils.closeQuietly(featureWriter);
		IOUtils.closeQuietly(compressedWriter);
	}
	
	/**
	 * Report human readable output
	 */
	private void reportHR() {
		int index = 1;
		for(JavaFile javaFile : project.getJavaFiles()) {
			StringBuffer fileNameBuffer = new StringBuffer();
			fileNameBuffer.append(index).append("_")
				.append(javaFile.getFileName()).append(Settings.TXT_EXTENSION);
			File outputFile = PathUtil.prepareFile(outputPath, fileNameBuffer.toString());
			FileWriter fileWriter = null;
			CSVWriter csvWriter = null;
			try {
				fileWriter = new FileWriter(outputFile);
				csvWriter = new CSVWriter(fileWriter);
			} catch (IOException e) {
				logger.error("Error: Fail when open the file " + outputFile.getAbsolutePath(), e);
			}
			
			try {
				fileWriter.write(javaFile.getSourceFilePath());
				fileWriter.write("\n");
				String[] featureElements = new String[7];
				for(Method method : javaFile.getMethods()) {
					// output the method name
					fileWriter.write(method.getQualifiedMethodName() + "\n");
					Set<Feature> features = method.getFeatureSet().getFeatures();
					for(Feature feature: features) {
						featureElements[0] = (feature.isInBody()) ? "1" : "0";
						featureElements[1] = feature.getLexeme();
						featureElements[2] = feature.getHighLevel().toString();
						featureElements[3] = feature.getLowLevel().toString();
						featureElements[4] = (feature.getRefinedLevel() != null) ? feature.getRefinedLevel().toString() : "";
						featureElements[5] = (feature.getRefinedInfo() != null) ? feature.getRefinedInfo() : "";
						featureElements[6] = new Integer(feature.getCount()).toString();
						csvWriter.writeNext(featureElements);
					}
					StringBuffer methodInfo = new StringBuffer();
					// output the start line number and end line number of the method
					methodInfo.append("Method start line: ").append(method.getStartLine()).append("\n")
							.append("Method end line: ").append(method.getEndLine()).append("\n")
							.append("Method signature end line: ").append(method.getEndLineOfSignature()).append("\n") 
							.append("Method body start line: ").append(method.getStartLineOfBody()).append("\n");
					// output the number of the tokens (token in signature and in body)
					methodInfo.append("Number of tokens in method signature: ")
						.append(method.numOfTokensInSignature()).append("\n")
						.append("Number of tokens in method body: ")
						.append(method.numOfTokensInBody()).append("\n");
					// output the number of the features
					methodInfo.append("Number of features: ").append(method.numOfFeatures()).append("\n");
					methodInfo.append("Number of features in method body: ")
						.append(method.numOfFeaturesInBody()).append("\n");
					// output the hash code of the method 
					methodInfo.append("Hashcode of the method: ").append(method.hashCode()).append("\n\n");
					fileWriter.write(methodInfo.toString());
				}
			} catch(IOException e) {
				logger.error("Error: Fail when writing the file " + outputFile.getAbsolutePath(), e);
			}
			index ++;
			IOUtils.closeQuietly(csvWriter);	
			IOUtils.closeQuietly(fileWriter);
		} 
	}
}
		
