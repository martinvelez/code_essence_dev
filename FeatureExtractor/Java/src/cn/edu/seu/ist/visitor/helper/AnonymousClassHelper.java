package cn.edu.seu.ist.visitor.helper;

import org.eclipse.jdt.core.dom.ASTNode;

public class AnonymousClassHelper {
	
	/**
	 * Check whether type is defined in anonymous class
	 * @param node
	 * @return
	 */
	public static boolean isInAnonymousClass(ASTNode node) {
		boolean isIn = false;
		while(node != null) {
			if(node.getNodeType() == ASTNode.ANONYMOUS_CLASS_DECLARATION) {
				isIn = true;
				break;
			}
			node = node.getParent();
		}
		return isIn;
	}
}
