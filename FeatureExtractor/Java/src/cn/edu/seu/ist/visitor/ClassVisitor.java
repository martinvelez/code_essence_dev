package cn.edu.seu.ist.visitor;

import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.TypeDeclaration;

/**
 * Extract the list of classes in given Java project 
 * @author Dong Qiu
 *
 */
public class ClassVisitor extends AbstractVisitor {
	
	public ClassVisitor(String sourceFilePath) {
		super(sourceFilePath);
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		ITypeBinding typeBinding = node.resolveBinding();
		if(typeBinding != null) {
			javaFile.addClassName(typeBinding.getQualifiedName());
		}
		return super.visit(node);
	}	
	
	@Override
	public boolean visit(EnumDeclaration node) {
		ITypeBinding typeBinding = node.resolveBinding();
		if(typeBinding != null) {
			javaFile.addClassName(typeBinding.getQualifiedName());
		}
		return super.visit(node);
	}
}
