package cn.edu.seu.ist.visitor;

import java.lang.reflect.Constructor;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.ASTVisitor;

import cn.edu.seu.ist.model.file.JavaFile;

public abstract class AbstractVisitor extends ASTVisitor{
	/* logger */
	static Logger logger = Logger.getLogger(AbstractVisitor.class.getName());
	
	private static String visitorPackage = "cn.edu.seu.ist.visitor";
	private static String visitorBase = "Visitor";
	
	/* store the content of java file */
	protected JavaFile javaFile;
	/* flag whether it is the duplicates within the project */
	protected boolean isDuplicates;

	public AbstractVisitor(String sourceFilePath) {
		this.javaFile = new JavaFile(sourceFilePath);
		this.isDuplicates = false;
	}
	
	/**
	 * Return the java file containing method list 
	 * @return
	 */
	public JavaFile getJavaFile() {
		return javaFile;
	}
	
	public boolean isDuplicates() {
		return isDuplicates;
	}

	public void setDuplicates(boolean isDuplicates) {
		this.isDuplicates = isDuplicates;
	}
	
	/**
	 * Prepare visitor object according to the executionMode
	 * @param executionMode
	 * @param constrParams
	 * @return
	 */
	public static AbstractVisitor prepareClass(String executionMode, Object[] constrParams) {
		AbstractVisitor visitor = null;
		StringBuffer buffer = new StringBuffer();
		buffer.append(visitorPackage).append(".").append(executionMode).append(visitorBase);
		try {
			Class<?> visitorClass = Class.forName(buffer.toString());
			Constructor<?> constr = visitorClass.getConstructor(String.class);
			visitor = (AbstractVisitor)constr.newInstance(constrParams);
		} catch (Exception e) {
			logger.error("The execution mode " + executionMode + " is unavailable now... ");
			System.exit(-1);
		}
		return visitor;
	}
}
