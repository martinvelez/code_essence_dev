package cn.edu.seu.ist.visitor.helper;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.MarkerAnnotation;
import org.eclipse.jdt.core.dom.MemberValuePair;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeParameter;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;

import cn.edu.seu.ist.handler.impl.SimpleTypeHandler;
import cn.edu.seu.ist.model.method.Method;
import cn.edu.seu.ist.model.token.TokenType;

public class VariableHelper {
	// add the logger
	static Logger logger = Logger.getLogger(SimpleTypeHandler.class.getName());
	
	/**
	 * Get the category of variables in methods (including method signature)  
	 * @param node
	 * @param localVariables
	 * @return
	 */
	public static TokenType getNameTokenType(SimpleName node, Method method) {
		TokenType tokenType = null;
		if(isSimpleType(node) || isMethodInvocation(node)) {
			// simpleType and method invocation is handled before
		} else if(isPackageUse(node) || isOthersInMethodSignature(node)
				|| isLabel(node) || isMemberInMemberValuePair(node)
				|| isTypeNameInAnnotation(node)
				|| isTypeOrEnumName(node)) {
			tokenType = TokenType.OTHER;
		} else if(isLocalVariableDef(node)) {
			tokenType = TokenType.VARIABLE;
			method.addLocalVariable(node.getIdentifier());
		} else if(isParameterDef(node) || isParameterUse(node) 
				|| isLocalVariableUse(node, method)
				|| isExternalVariableUse(node)) {
			tokenType = TokenType.VARIABLE;
		} else if(isNameType(node) || isMethodTypeVariable(node)) {
			tokenType = TokenType.TYPE;
		} else {
			tokenType = TokenType.UNDEFINED;
		}
		return tokenType;
	}
	
	/**
	 * Check whether the ASTNode is local variable declaration
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean isLocalVariableDef(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.VARIABLE_DECLARATION_STATEMENT) {
				VariableDeclarationStatement variableDeclaration = (VariableDeclarationStatement)pointer;
				List<VariableDeclarationFragment> fragments = variableDeclaration.fragments();
				for(VariableDeclarationFragment fragment : fragments) {
					if(fragment.getName().equals(node)) {
						flag = true;
						break;
					}
				}
			}
			if(pointer.getNodeType() == ASTNode.VARIABLE_DECLARATION_EXPRESSION) {
				VariableDeclarationExpression variableDeclaration = (VariableDeclarationExpression)pointer;
				List<VariableDeclarationFragment> fragments = variableDeclaration.fragments();
				for(VariableDeclarationFragment fragment : fragments) {
					if(fragment.getName().equals(node)) {
						flag = true;
						break;
					}
				}
			}
			if(pointer.getNodeType() == ASTNode.SINGLE_VARIABLE_DECLARATION) {
				if(!ASTNodeHelper.isMethodFormalParameter(pointer)) {
					SingleVariableDeclaration singleVariable = (SingleVariableDeclaration) pointer;
					if(singleVariable.getName().equals(node)) {
						flag = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}

	/**
	 * Check whether the ASTNode is local variable usage
	 * @param node
	 * @param localVariables
	 * @return
	 */
	public static boolean isLocalVariableUse(SimpleName node, Method method) {
		boolean flag = false;
		IBinding binding = null; 
		try {
			binding = node.resolveBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		if(binding != null && binding.getKind() == IBinding.VARIABLE) {
			IVariableBinding variableBinding = (IVariableBinding)binding;
			if(!variableBinding.isParameter() && !variableBinding.isField() && !variableBinding.isEnumConstant()) {
				for(String localVariable : method.getLocalVariables()) {
					if(node.getIdentifier().equals(localVariable)) {
						flag = true;
						break;
					}
				}
			}
		} else {
			for(String localVariable : method.getLocalVariables()) {
				if(node.getIdentifier().equals(localVariable)) {
					flag = true;
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * Check whether the ASTNode is parameter declaration in method signature
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean isParameterDef(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_DECLARATION) {
				MethodDeclaration methodDeclaration = (MethodDeclaration)pointer;
				List<SingleVariableDeclaration> parameters = methodDeclaration.parameters();
				for(SingleVariableDeclaration parameter : parameters) {
					if(parameter.getName().equals(node)) {
						flag = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}

	/**
	 * Check whether the ASTNode is parameter usage in method body
	 * @param node
	 * @return
	 */
	private static boolean isParameterUse(SimpleName node) {
		boolean flag = false;
		IBinding binding = null; 
		try {
			binding = node.resolveBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		if(binding != null) {
			// use binding information to check if it is parameter usage
			if(binding.getKind() == IBinding.VARIABLE) {
				IVariableBinding variableBinding = (IVariableBinding)binding;
				if(variableBinding.isParameter()) {	
					if(!isParameterDef(node)) {
						flag = true;
					}
				}
			}
		} else {
			// if binding information can not be obtained, we use other way instead
			if(isParamUse(node)) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * Check whether the ASTNode is external variable usage
	 * @param node
	 * @return
	 */
	private static boolean isExternalVariableUse(SimpleName node) {
		boolean flag = false;
		IBinding binding = null; 
		try {
			binding = node.resolveBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		if(binding != null) {
			// use binding information to check if it is external variable usage
			if(binding.getKind() == IBinding.VARIABLE) {
				IVariableBinding variableBinding = (IVariableBinding)binding;
				if(variableBinding.isField() || variableBinding.isEnumConstant()) {
					flag = true;
				}
			}
		} else {
			// if binding information can not be obtained, we use other way instead
			if(isFieldAccess(node)) {
				flag = true;
			} else if(isInStaticImport(node)) {
				flag = true;
			} else if(isLocalInQualifiedName(node)) {
				flag = true;
			} else if(isLocalInCallerOfMethodInvocation(node)) {
				flag = true;
			} else if(isFieldUse(node)) {
				flag = true;
			} /*else if(isSuperFieldUse(node)) {
				flag = true;
			}*/
		}
		return flag;
	}

	/**
	 * Check whether the ASTNode is package usage
	 * @param node
	 * @return
	 */
	private static boolean isPackageUse(SimpleName node) {
		boolean flag = false;
		IBinding binding = null;
		try {
			binding = node.resolveBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		if(binding != null && binding.getKind() == IBinding.PACKAGE) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Check whether the ASTNode is type variable / method name / method exception 
	 * @param node
	 * @return
	 */
	private static boolean isOthersInMethodSignature(SimpleName node) {
		if(isMethodName(node) || isMethodException(node)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Check whether a simple name is a simple type
	 * @param node
	 * @return
	 */
	private static boolean isSimpleType(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.SIMPLE_TYPE) {
				flag = true;
				break;
			}
			pointer = pointer.getParent();
		}
		return flag;
	}

	/**
	 * Check whether a simpleName is className or enumName
	 * @param node
	 * @return
	 */
	private static boolean isNameType(SimpleName node) {
		boolean flag = false;
		IBinding binding = null; 
		try {
			binding = node.resolveBinding();
		} catch(Exception e) {
			logger.error(ASTNodeHelper.getExceptionMessage(node), e);
		}
		if(binding != null) { 
			if(binding.getKind() == IBinding.TYPE) {
				ITypeBinding typeBinding = (ITypeBinding)binding;
				if(typeBinding.isClass() || typeBinding.isInterface() 
						|| typeBinding.isEnum() || typeBinding.isAnnotation()
						|| typeBinding.isGenericType()) {
					flag = true;
				}
			}
		} else {
			if(isQualiferInQualifiedName(node)) {
				flag = true;
			} else if(isCallerOfMethodInvocation(node)) {
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * Check whether ASTNode is Method Type Bound 
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean isMethodTypeVariable(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_DECLARATION) {
				MethodDeclaration methodDeclaration = (MethodDeclaration)pointer;
				List<TypeParameter> typeParameters = methodDeclaration.typeParameters();
				for(TypeParameter typeParam : typeParameters) {
					if(node.equals(typeParam.getName())) {
						flag = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 * Check whether simpleName is thrown exception in method declaration
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean isMethodException(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_DECLARATION) {
				MethodDeclaration methodDeclaration = (MethodDeclaration)pointer;
				List<Name> exceptions = methodDeclaration.thrownExceptions();
				if(exceptions != null) {
					for(Name exception : exceptions) {
						if(exception.equals(node)) {
							flag = true;
							break;
						}
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 * Check whether simpleName is method name
	 * @param node
	 * @return
	 */
	private static boolean isMethodName(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_DECLARATION) {
				MethodDeclaration methodDeclaration = (MethodDeclaration)pointer;
				if(node.equals(methodDeclaration.getName())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 * Check whether simpleName is class or interface name
	 * @param node
	 * @return
	 */
	private static boolean isTypeOrEnumName(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.TYPE_DECLARATION) {
				TypeDeclaration typeDeclaration = (TypeDeclaration)pointer;
				if(node.equals(typeDeclaration.getName())) {
					flag = true;
					break;
				}
			}
			if(pointer.getNodeType() == ASTNode.ENUM_DECLARATION) {
				EnumDeclaration enumDeclaration = (EnumDeclaration)pointer;
				if(node.equals(enumDeclaration.getName())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 * Check whether simpleName is method name
	 * @param node
	 * @return
	 */
	private static boolean isMethodInvocation(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_INVOCATION) {
				MethodInvocation methodInvocation = (MethodInvocation)pointer;
				if(node.equals(methodInvocation.getName())) {
					flag = true;
					break;
				}
			}
			if(pointer.getNodeType() == ASTNode.SUPER_METHOD_INVOCATION) {
				SuperMethodInvocation methodInvocation = (SuperMethodInvocation)pointer;
				if(node.equals(methodInvocation.getName())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 *  Check whether SimpleName is the caller of the method (e.g. System.out.println, System is what we want)
	 * @param node
	 * @return
	 */
	private static boolean isCallerOfMethodInvocation(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_INVOCATION) {
				MethodInvocation methodInvocation = (MethodInvocation)pointer;
				Expression caller = methodInvocation.getExpression();
				if(caller != null) {
					if(caller.getNodeType() == ASTNode.SIMPLE_NAME) {
						SimpleName simpleName = (SimpleName)caller;
						if(node.getIdentifier().equals(simpleName.getIdentifier())) {
							flag = true;
							break;
						}
					}
					if(caller.getNodeType() == ASTNode.QUALIFIED_NAME) {
						QualifiedName qName = (QualifiedName)caller;
						if(node.getIdentifier().equals(qName.getQualifier().toString())) {
							flag = true;
							break;
						}
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}

	/**
	 * Check whether simpleName is the qualified name of the qualified name
	 * @param node
	 * @return
	 */
	private static boolean isQualiferInQualifiedName(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.QUALIFIED_NAME) {
				QualifiedName qualifiedName = (QualifiedName)pointer; 
				if(node.getIdentifier().equals(qualifiedName.getQualifier().toString())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 * Check whether simpleName is the local name of the qualified name
	 * @param node
	 * @return
	 */
	private static boolean isLocalInQualifiedName(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.QUALIFIED_NAME) {
				QualifiedName qualifiedName = (QualifiedName)pointer; 
				if(node.getIdentifier().equals(qualifiedName.getName().toString())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 * Check whether SimpleName is field usage
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean isFieldUse(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.TYPE_DECLARATION) {
				TypeDeclaration typeDeclaration = (TypeDeclaration)pointer;
				FieldDeclaration[] fields = typeDeclaration.getFields();
				for(FieldDeclaration field : fields) {
					List<VariableDeclarationFragment> fieldFragments = field.fragments();
					for(VariableDeclarationFragment fieldFragment : fieldFragments) {
						if(fieldFragment.getName().toString().equals(node.getIdentifier())) {
							flag = true;
							break;
						}
					}
				}
			}
			pointer = pointer.getParent();
		 }
		 return flag;
	}
	
	/**
	 * Check whether SimpleName is in static import
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean isInStaticImport(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.COMPILATION_UNIT) {
				CompilationUnit unit = (CompilationUnit)pointer;
				List<ImportDeclaration> imports = unit.imports();
				for(ImportDeclaration importDeclaration : imports) {
					if(importDeclaration.isStatic()) {
						if(importDeclaration.isOnDemand()) {
							// if the import is on-demand (like java.util.*)
							IBinding binding = null;
							try {
								binding = importDeclaration.resolveBinding();
							} catch(Exception e) {
								logger.error(ASTNodeHelper.getExceptionMessage(node), e);
							}
							if(binding != null) {
								if(binding.getKind() == IBinding.TYPE) {
									ITypeBinding typeBinding = (ITypeBinding)binding;
									 IVariableBinding[] variables = typeBinding.getDeclaredFields();
									 for(IVariableBinding variable : variables) {
										 if(variable.getName().equals(node.getIdentifier())) {
											 flag = true;
											 break;
										 }
									 }
								}
							}
						} else {
							// if the import is ont-type (like java.util.List)
							Name importName = importDeclaration.getName();
							if(importName.isQualifiedName()) {
								QualifiedName qualifeiedImportName = (QualifiedName)importName;
								if(qualifeiedImportName.getName().getIdentifier().equals(node.getIdentifier())) {
									flag = true;
									break;
								}
							} else {
								SimpleName simpleImportName = (SimpleName)importName;
								if(simpleImportName.getIdentifier().equals(node.getIdentifier())) {
									flag = true;
									break;
								}
							}
						}
						
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	/**
	 *  Check whether SimpleName is parameter usage
	 * @param node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static boolean isParamUse(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_DECLARATION) {
				MethodDeclaration methodDeclaration = (MethodDeclaration)pointer;
				List<SingleVariableDeclaration> parameters = methodDeclaration.parameters();
				for(SingleVariableDeclaration parameter : parameters) {
					if(node.getIdentifier().equals(parameter.getName().getIdentifier())) {
						flag = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		 }
		 return flag;
	}
	
	/**
	 *  Check whether SimpleName is the local name of caller if caller is qualifier name (e.g. System.out.println, out is what we want)
	 * @param node
	 * @return
	 */
	private static boolean isLocalInCallerOfMethodInvocation(SimpleName node) {
		boolean flag = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_INVOCATION) {
				MethodInvocation methodInvocation = (MethodInvocation)pointer;
				Expression caller = methodInvocation.getExpression();
				if(caller != null) {
					if(caller.getNodeType() == ASTNode.QUALIFIED_NAME) {
						QualifiedName qName = (QualifiedName)caller;
						if(node.getIdentifier().equals(qName.getName().getIdentifier())) {
							flag = true;
							break;
						}
					}
				}
			}
			pointer = pointer.getParent();
		}
		return flag;
	}
	
	/**
	 *  Check whether SimpleName is the label 
	 * @param node
	 * @return
	 */
	private static boolean isLabel(SimpleName node) {
		boolean flag = false;
		if(isLabelInLabelStatement(node) || isLabelInContinueStatement(node) || isLabelInBreakStatement(node)) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Check whether SimpleName is the label in the labeled statement 
	 * @param node
	 * @return
	 */
	private static boolean isLabelInLabelStatement(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.LABELED_STATEMENT) {
				LabeledStatement labeledStatement = (LabeledStatement)pointer;
				if(node.equals(labeledStatement.getLabel())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		 }
		return flag;
	}
	
	/**
	 *  Check whether SimpleName is the label in the break statement
	 * @param node
	 * @return
	 */
	private static boolean isLabelInBreakStatement(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.BREAK_STATEMENT) {
				BreakStatement breakStatement = (BreakStatement)pointer;
				if(breakStatement.getLabel() != null) {
					if(node.equals(breakStatement.getLabel())) {
						flag = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		 }
		return flag;
	}
	
	/**
	 *  Check whether SimpleName is the label in the continue statement
	 * @param node
	 * @return
	 */
	private static boolean isLabelInContinueStatement(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.CONTINUE_STATEMENT) {
				ContinueStatement continueStatement = (ContinueStatement)pointer;
				if(continueStatement.getLabel() != null) {
					if(node.equals(continueStatement.getLabel())) {
						flag = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		 }
		return flag;
	}
	
	/**
	 *  Check whether SimpleName is the type name of annotation
	 * @param node
	 * @return
	 */
	private static boolean isTypeNameInAnnotation(SimpleName node) {
		boolean flag = false;
		if(isTypeNameInMarkerAnnotation(node) || isTypeNameInSingleMemberAnnotation(node)
				|| isTypeNameInNormalAnnotation(node)) {
			flag = true;
		}
		return flag;
	}
	
	/**
	 * Check whether SimpleName is the type name of single member annotation
	 * @param node
	 * @return
	 */
	private static boolean isTypeNameInSingleMemberAnnotation(SimpleName node) {
		boolean flag = false;
		if(node.getParent().getNodeType() == ASTNode.SINGLE_MEMBER_ANNOTATION) {
			SingleMemberAnnotation singleMemberAnnotation = (SingleMemberAnnotation) node.getParent();
			if(singleMemberAnnotation.getTypeName().equals(node)) {
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * Check whether SimpleName is the type name of marker annotation
	 * @param node
	 * @return
	 */
	private static boolean isTypeNameInMarkerAnnotation(SimpleName node) {
		boolean flag = false;
		if(node.getParent().getNodeType() == ASTNode.MARKER_ANNOTATION) {
			MarkerAnnotation markerAnnotation = (MarkerAnnotation) node.getParent();
			if(markerAnnotation.getTypeName().equals(node)) {
				flag = true;
			}
		}
		return flag;
	}
	
	
	/**
	 * Check whether SimpleName is the type name of normal annotation
	 * @param node
	 * @return
	 */
	private static boolean isTypeNameInNormalAnnotation(SimpleName node) {
		boolean flag = false;
		if(node.getParent().getNodeType() == ASTNode.NORMAL_ANNOTATION) {
			NormalAnnotation normalAnnotation = (NormalAnnotation) node.getParent();
			if(normalAnnotation.getTypeName().equals(node)) {
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * Check whether SimpleName is the member of member value pair
	 * @param node
	 * @return
	 */
	private static boolean isMemberInMemberValuePair(SimpleName node) {
		boolean flag = false;
		if(node.getParent().getNodeType() == ASTNode.MEMBER_VALUE_PAIR) {
			MemberValuePair memberValuePair = (MemberValuePair) node.getParent();
			if(memberValuePair.getName().equals(node)) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * Check whether SimpleName is the field access
	 * @param node
	 * @return
	 */
	private static boolean isFieldAccess(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.FIELD_ACCESS) {
				FieldAccess fieldAccess = (FieldAccess)pointer;
				if(node.equals(fieldAccess.getName())) {
					flag = true;
					break;
				}
			}
			pointer = pointer.getParent();
		 }
		return flag;
	}

	/**
	 * Check whether SimpleName is the super field access
	 * @param node
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	private static boolean isSuperFieldUse(SimpleName node) {
		boolean flag = false;
		 ASTNode pointer = node;
		 while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.TYPE_DECLARATION) {
				TypeDeclaration typeDeclaration = (TypeDeclaration)pointer;
				// search the fields in the super class
				Type superClass = typeDeclaration.getSuperclassType();
				ITypeBinding superClassBinding = null;
				if(superClass != null) {
					try {
						superClassBinding = superClass.resolveBinding();
					} catch(Exception e) {
						e.printStackTrace();
					}
					if(superClassBinding != null) {
						flag = isSuperFieldUse(node, superClassBinding);
						if(flag) break;
					}
				}
				// search the fields in the super interfaces
				List<Type> superInterfaces = typeDeclaration.superInterfaceTypes();
				if(superInterfaces != null && superInterfaces.size() > 0) {
					for(Type superInterface : superInterfaces) {
						ITypeBinding superInterfaceBinding = null;
						if(superInterface != null) {
							try {
								superInterfaceBinding = superInterface.resolveBinding();
							} catch(Exception e) {
								e.printStackTrace();
							}
							if(superInterfaceBinding != null) {
								flag = isSuperFieldUse(node, superInterfaceBinding);
								if(flag) break;
							}
						}
					}
				}
				
				ITypeBinding typeBinding = null;
				try {
					typeBinding = typeDeclaration.resolveBinding();
				} catch(Exception e) {
					e.printStackTrace();
				}
				if(typeBinding != null) {
					ITypeBinding superClass = typeBinding.getSuperclass();
					if(superClass != null) {
						flag = isSuperFieldUse(node, superClass);
						if(flag)
							break;
					}
					ITypeBinding[] interfaces = typeBinding.getInterfaces();
					for(ITypeBinding interfaceBinding : interfaces) {
						flag = isSuperFieldUse(node, interfaceBinding);
							if(flag) 
								break;
					}
				}
			}
			pointer = pointer.getParent();
		 }
		return flag;
	}
	
	private static boolean isSuperFieldUse(SimpleName node, ITypeBinding typeBinding) {
		IVariableBinding[] variableBindings = typeBinding.getDeclaredFields();
		if(variableBindings != null && variableBindings.length > 0) {
			for(IVariableBinding variableBinding : variableBindings) {
				if(node.getIdentifier().equals(variableBinding.getName())) {
					return true;
				}
			}
		}
		//handling the superclass
		ITypeBinding superClass = typeBinding.getSuperclass();
		if(superClass != null) {
			if(isSuperFieldUse(node, superClass)) {
				return true;
			}
		}
		// handling the super interfaces
		ITypeBinding[] superInterfaces = typeBinding.getInterfaces();
		if(superInterfaces != null && superInterfaces.length > 0) {
			for(ITypeBinding superInterface : superInterfaces) {
				if(isSuperFieldUse(node, superInterface)) {
					return true;
				}
			}
		}
		return false;
	}*/
}
