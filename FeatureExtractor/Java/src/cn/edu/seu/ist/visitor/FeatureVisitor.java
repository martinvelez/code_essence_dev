package cn.edu.seu.ist.visitor;

import java.util.Stack;

import org.eclipse.jdt.core.dom.AnnotationTypeDeclaration;
import org.eclipse.jdt.core.dom.AnnotationTypeMemberDeclaration;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.ArrayType;
import org.eclipse.jdt.core.dom.AssertStatement;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.BlockComment;
import org.eclipse.jdt.core.dom.BooleanLiteral;
import org.eclipse.jdt.core.dom.BreakStatement;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CatchClause;
import org.eclipse.jdt.core.dom.CharacterLiteral;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.ContinueStatement;
import org.eclipse.jdt.core.dom.DoStatement;
import org.eclipse.jdt.core.dom.EmptyStatement;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.LabeledStatement;
import org.eclipse.jdt.core.dom.LineComment;
import org.eclipse.jdt.core.dom.MarkerAnnotation;
import org.eclipse.jdt.core.dom.MemberRef;
import org.eclipse.jdt.core.dom.MemberValuePair;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.MethodRef;
import org.eclipse.jdt.core.dom.MethodRefParameter;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.ParameterizedType;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.PrimitiveType;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.QualifiedType;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.SuperFieldAccess;
import org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.eclipse.jdt.core.dom.SwitchCase;
import org.eclipse.jdt.core.dom.SwitchStatement;
import org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.TryStatement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclarationStatement;
import org.eclipse.jdt.core.dom.TypeLiteral;
import org.eclipse.jdt.core.dom.TypeParameter;
import org.eclipse.jdt.core.dom.UnionType;
import org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.eclipse.jdt.core.dom.WildcardType;

import cn.edu.seu.ist.handler.impl.AnonymousClassDeclarationHandler;
import cn.edu.seu.ist.handler.impl.ArrayAccessHandler;
import cn.edu.seu.ist.handler.impl.ArrayCreationHandler;
import cn.edu.seu.ist.handler.impl.ArrayInitializerHandler;
import cn.edu.seu.ist.handler.impl.ArrayTypeHandler;
import cn.edu.seu.ist.handler.impl.AssertStatementHandler;
import cn.edu.seu.ist.handler.impl.AssignmentHandler;
import cn.edu.seu.ist.handler.impl.BlockHandler;
import cn.edu.seu.ist.handler.impl.BooleanLiteralHandler;
import cn.edu.seu.ist.handler.impl.BreakStatementHandler;
import cn.edu.seu.ist.handler.impl.CastExpressionHandler;
import cn.edu.seu.ist.handler.impl.CatchClauseHandler;
import cn.edu.seu.ist.handler.impl.CharacterLiteralHandler;
import cn.edu.seu.ist.handler.impl.ClassInstanceCreationHandler;
import cn.edu.seu.ist.handler.impl.CompilationUnitHandler;
import cn.edu.seu.ist.handler.impl.ConditionalExpressionHandler;
import cn.edu.seu.ist.handler.impl.ConstructorInvocationHandler;
import cn.edu.seu.ist.handler.impl.ContinueStatementHandler;
import cn.edu.seu.ist.handler.impl.DoStatementHandler;
import cn.edu.seu.ist.handler.impl.EmptyStatementHandler;
import cn.edu.seu.ist.handler.impl.EnhancedForStatementHandler;
import cn.edu.seu.ist.handler.impl.EnumConstantDeclarationHandler;
import cn.edu.seu.ist.handler.impl.EnumDeclarationHandler;
import cn.edu.seu.ist.handler.impl.ExpressionStatementHandler;
import cn.edu.seu.ist.handler.impl.FieldAccessHandler;
import cn.edu.seu.ist.handler.impl.FieldDeclarationHandler;
import cn.edu.seu.ist.handler.impl.ForStatementHandler;
import cn.edu.seu.ist.handler.impl.IfStatementHandler;
import cn.edu.seu.ist.handler.impl.InfixExpressionHandler;
import cn.edu.seu.ist.handler.impl.InstanceofExpressionHandler;
import cn.edu.seu.ist.handler.impl.LabeledStatementHandler;
import cn.edu.seu.ist.handler.impl.MarkerAnnotationHandler;
import cn.edu.seu.ist.handler.impl.MemberValuePairHandler;
import cn.edu.seu.ist.handler.impl.MethodDeclarationHandler;
import cn.edu.seu.ist.handler.impl.MethodInvocationHandler;
import cn.edu.seu.ist.handler.impl.ModifierHandler;
import cn.edu.seu.ist.handler.impl.NormalAnnotationHandler;
import cn.edu.seu.ist.handler.impl.NullLiteralHandler;
import cn.edu.seu.ist.handler.impl.NumberLiteralHandler;
import cn.edu.seu.ist.handler.impl.ParameterizedTypeHandler;
import cn.edu.seu.ist.handler.impl.ParenthesizedExpressionHandler;
import cn.edu.seu.ist.handler.impl.PostfixExpressionHandler;
import cn.edu.seu.ist.handler.impl.PrefixExpressionHandler;
import cn.edu.seu.ist.handler.impl.PrimitiveTypeHandler;
import cn.edu.seu.ist.handler.impl.QualifiedNameHandler;
import cn.edu.seu.ist.handler.impl.QualifiedTypeHandler;
import cn.edu.seu.ist.handler.impl.ReturnStatementHandler;
import cn.edu.seu.ist.handler.impl.SimpleNameHandler;
import cn.edu.seu.ist.handler.impl.SimpleTypeHandler;
import cn.edu.seu.ist.handler.impl.SingleMemberAnnotationHandler;
import cn.edu.seu.ist.handler.impl.SingleVariableDeclarationHandler;
import cn.edu.seu.ist.handler.impl.StringLiteralHandler;
import cn.edu.seu.ist.handler.impl.SuperConstructorInvocationHandler;
import cn.edu.seu.ist.handler.impl.SuperFieldAccessHandler;
import cn.edu.seu.ist.handler.impl.SuperMethodInvocationHandler;
import cn.edu.seu.ist.handler.impl.SwitchCaseHandler;
import cn.edu.seu.ist.handler.impl.SwitchStatementHandler;
import cn.edu.seu.ist.handler.impl.SynchronizedStatementHandler;
import cn.edu.seu.ist.handler.impl.ThisExpressionHandler;
import cn.edu.seu.ist.handler.impl.ThrowStatementHandler;
import cn.edu.seu.ist.handler.impl.TryStatementHandler;
import cn.edu.seu.ist.handler.impl.TypeDeclarationHandler;
import cn.edu.seu.ist.handler.impl.TypeLiteralHandler;
import cn.edu.seu.ist.handler.impl.TypeParameterHandler;
import cn.edu.seu.ist.handler.impl.UnionTypeHandler;
import cn.edu.seu.ist.handler.impl.VariableDeclarationExpressionHandler;
import cn.edu.seu.ist.handler.impl.VariableDeclarationFragmentHandler;
import cn.edu.seu.ist.handler.impl.VariableDeclarationStatementHandler;
import cn.edu.seu.ist.handler.impl.WhileStatementHandler;
import cn.edu.seu.ist.handler.impl.WildcardTypeHandler;
import cn.edu.seu.ist.model.method.Method;
import cn.edu.seu.ist.util.statistics.GlobalStat;

/**
 * FeatureVisitor is used to collect feature-based information 
 * from Java project. 
 * @author Dong Qiu
 *
 */
public class FeatureVisitor extends AbstractVisitor {
	/* record the feature in current method under analysis */
	private Method method;
	/* store the unfinished method when analyzing  method */
	private Stack<Method> methodStack;
	/* record whether the method are in the anonymous class */
	private int isInAnonymousClass;
	/* record whether the method are in the method inner class */
	private int isInMethodInnerClass;
	/* store current compilationUnit under analysis */
	private CompilationUnit currentCU;

	/**
	 * Constructor
	 */
	public FeatureVisitor(String sourceFilePath) {
		super(sourceFilePath);
		this.methodStack = new Stack<Method>();
		this.isInAnonymousClass = 0;
		this.isInMethodInnerClass = 0;
		this.currentCU = null;
	}
	
	/**
	 * When visitor arrive the node of method declaration,
	 * method name, method parameter type and method return 
	 * type should be collected. 
	 * 
	 * Note: we consider methods defined in the anonymous 
	 * class, which is used in method A, as parts of A.  
	 * 
	 */
	@Override
	public boolean visit(MethodDeclaration node) {
		// If method declaration is not in the anonymous class
		if(isInAnonymousClass == 0 && isInMethodInnerClass == 0) {
			// if current method under analysis is not finished, push it into stack
			if(this.method != null) {
				this.methodStack.push(this.method);
			}
			// testing script
			//System.out.println("Start to analyzed method " + node.getName());
			// create a new method and increase mid
			GlobalStat.increaseMid();
			this.method = new Method(GlobalStat.getMid());
			// handle the method declaration 
			MethodDeclarationHandler mdHandler = new MethodDeclarationHandler(node, this.currentCU);
			this.method.addFeatures(mdHandler.handle());
			this.method.setTokenList(mdHandler.getTokens());
			this.method.setMethodName(mdHandler.getMethodName());
			this.method.setClassName(mdHandler.getClassName());
			this.method.setSourceFilePath(this.javaFile.getSourceFilePath());
			// set the start line number and end line number of the method
			this.method.setStartLine(mdHandler.getStartLine());
			this.method.setEndLine(mdHandler.getEndLine());
			this.method.setStartLineOfBody(mdHandler.getStartLineOfBody());
			this.method.setEndLineOfSignature(mdHandler.getEndLineOfSignature());
			// set the number of tokens in method body
			this.method.setTokensInBody(mdHandler.getNumOfTokensInBody());
		} else {
			if(method != null) {
				this.method.addFeatures(new MethodDeclarationHandler(node).handle());
			}
		}
		return super.visit(node);
	}
	
	/**
	 * When visitor arrive the end of method declaration node,
	 * add method with collected features into method list
	 */
	@Override
	public void endVisit(MethodDeclaration node) {
		if(isInAnonymousClass == 0 && isInMethodInnerClass == 0) {
			// when finish visiting the method, add method into java file
			javaFile.addMethod(this.method);
			// Global data collection

			
			
			/*// Check possible duplicated method 
			if(this.method.numOfFeaturesInBody() >= 30) {
				long hashcodeVal = this.method.hashCode();
				if(GlobalVars.possibleDups.containsKey(hashcodeVal)) {
					Collection<Long> duplicates = GlobalVars.possibleDups.get(hashcodeVal);
					duplicates.add(this.method.getMid());
				} else {
					Collection<Long> duplicates = Collections.synchronizedList(new ArrayList<Long>());
					duplicates.add(this.method.getMid());
					GlobalVars.possibleDups.put(hashcodeVal, duplicates);
				}
			}*/
			// if there are unfinished methods in stack, pop it and continue handling
			if(methodStack.isEmpty()) {
				this.method = null;
			} else {
				this.method = methodStack.pop();
			}
		}
		super.endVisit(node);
	}
	
	@Override
	public boolean visit(AnnotationTypeDeclaration node) {
		// do nothing since annotation can't be part of method body
		return super.visit(node);
	}

	@Override
	public boolean visit(AnnotationTypeMemberDeclaration node) {
		// do nothing since annotation can't be part of method body
		return super.visit(node);
	}

	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		this.isInAnonymousClass ++;
		if(method != null) {
			method.addFeatures(new AnonymousClassDeclarationHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public void endVisit(AnonymousClassDeclaration node) {
		this.isInAnonymousClass --;
		super.endVisit(node);
	}

	@Override
	public boolean visit(ArrayAccess node) {
		if(method != null) {
			method.addFeatures(new ArrayAccessHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ArrayCreation node) {
		if(method != null) {
			method.addFeatures(new ArrayCreationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ArrayInitializer node) {
		if(method != null) {
			method.addFeatures(new ArrayInitializerHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ArrayType node) {
		if(method != null) {
			method.addFeatures(new ArrayTypeHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(AssertStatement node) {
		if(method != null) {
			method.addFeatures(new AssertStatementHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(Assignment node) {
		if(method != null) {
			method.addFeatures(new AssignmentHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(Block node) {
		if(method != null) {
			method.addFeatures(new BlockHandler().handle());
		}
		return super.visit(node);
	}
	
	@Override
	public boolean visit(BlockComment node) {
		// do nothing for comment 
		return super.visit(node);
	}

	@Override
	public boolean visit(BooleanLiteral node) {
		if(method != null) {
			method.addFeatures(new BooleanLiteralHandler(node).handle());
		}
		return super.visit(node);
	}
	
	@Override
	public boolean visit(BreakStatement node) {
		if(method != null) {
			method.addFeatures(new BreakStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(CastExpression node) {
		if(method != null) {
			method.addFeatures(new CastExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(CatchClause node) {
		if(method != null) {
			method.addFeatures(new CatchClauseHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(CharacterLiteral node) {
		if(method != null) {
			method.addFeatures(new CharacterLiteralHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {
		if(method != null) {
			method.addFeatures(new ClassInstanceCreationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(CompilationUnit node) {
		// initial current compilation unit
		this.currentCU = node;
		// set the package name 
		CompilationUnitHandler compilationUnitHandler = new CompilationUnitHandler(node);
		compilationUnitHandler.handle();
		javaFile.setPackageName(compilationUnitHandler.getPackageName());
		javaFile.setPublicClassName(compilationUnitHandler.getPublicClassName());
		return super.visit(node);
	}
	
	@Override
	public void endVisit(CompilationUnit node) {
		// empty current compilation unit
		this.currentCU = null;
		super.endVisit(node);
	}

	@Override
	public boolean visit(ConditionalExpression node) {
		if(method != null) {
			method.addFeatures(new ConditionalExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ConstructorInvocation node) {
		if(method != null) {
			method.addFeatures(new ConstructorInvocationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ContinueStatement node) {
		if(method != null) {
			method.addFeatures(new ContinueStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(DoStatement node) {
		if(method != null) {
			method.addFeatures(new DoStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(EmptyStatement node) {
		if(this.method != null) {
			method.addFeatures(new EmptyStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(EnhancedForStatement node) {
		if(method != null) {
			method.addFeatures(new EnhancedForStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(EnumConstantDeclaration node) {
		if(method != null) {
			method.addFeatures(new EnumConstantDeclarationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(EnumDeclaration node) {
		if(method != null) {
			method.addFeatures(new EnumDeclarationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public void endVisit(EnumDeclaration node) {
		super.endVisit(node);
	}

	@Override
	public boolean visit(ExpressionStatement node) {
		if(method != null) {
			method.addFeatures(new ExpressionStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(FieldAccess node) {
		if(method != null) {
			method.addFeatures(new FieldAccessHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		if(method != null) {
			method.addFeatures(new FieldDeclarationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ForStatement node) {
		if(method != null) {
			method.addFeatures(new ForStatementHandler(node).handle());
		}
		return super.visit(node);
	}


	@Override
	public boolean visit(IfStatement node) {
		if(method != null) {
			method.addFeatures(new IfStatementHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ImportDeclaration node) {
		// do nothing since import information is out of our consideration
		return super.visit(node);
	}

	@Override
	public boolean visit(InfixExpression node) {
		if(method != null) {
			method.addFeatures(new InfixExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(Initializer node) {
		// do nothing (static is handled in ModifierHandler)
		return super.visit(node);
	}

	@Override
	public boolean visit(InstanceofExpression node) {
		if(method != null) {
			method.addFeatures(new InstanceofExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(Javadoc node) {
		// do nothing
		return super.visit(node);
	}

	@Override
	public boolean visit(LabeledStatement node) {
		if(method != null) {
			method.addFeatures(new LabeledStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(LineComment node) {
		// do nothing 
		return super.visit(node);
	}

	@Override
	public boolean visit(MarkerAnnotation node) {
		if(method != null) {
			method.addFeatures(new MarkerAnnotationHandler(node).handle());
		}
		return super.visit(node);
	}

	/**
	 *  MemberRef:
     *          [ Name ] # Identifier
	 * @param node
	 * @return
	 */
	@Override
	public boolean visit(MemberRef node) {
		// do nothing since MemberRef is used in Java doc
		return super.visit(node);
	}

	@Override
	public boolean visit(MemberValuePair node) {
		if(method != null) {
			method.addFeatures(new MemberValuePairHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(MethodInvocation node) {
		if(method != null) {
			method.addFeatures(new MethodInvocationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(MethodRef node) {
		// do nothing since MethodRef is in JavaDoc while not method
		return super.visit(node);
	}

	@Override
	public boolean visit(MethodRefParameter node) {
		// do nothing since MethodRefParameter is in JavaDoc while not method
		return super.visit(node);
	}
	
	@Override
	public boolean visit(Modifier node) {
		if(method != null) {
			method.addFeatures(new ModifierHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(NormalAnnotation node) {
		if(method != null) {
			method.addFeatures(new NormalAnnotationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(NullLiteral node) {
		if(method != null) {
			method.addFeatures(new NullLiteralHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(NumberLiteral node) {
		if(method != null) {
			method.addFeatures(new NumberLiteralHandler(node).handle());
		}
		return super.visit(node);
	}

	/**
	 * PackageDeclaration:
     *		[ Javadoc ] { Annotation } package Name ;
	 */
	@Override
	public boolean visit(PackageDeclaration node) {
		// do nothing
		return super.visit(node);
	}

	@Override
	public boolean visit(ParameterizedType node) {
		if(method != null) {
			method.addFeatures(new ParameterizedTypeHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ParenthesizedExpression node) {
		if(method != null) {
			method.addFeatures(new ParenthesizedExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(PostfixExpression node) {
		if(method != null) {
			method.addFeatures(new PostfixExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(PrefixExpression node) {
		if(method != null) {
			method.addFeatures(new PrefixExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(PrimitiveType node) {
		if(method != null) {
			method.addFeatures(new PrimitiveTypeHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(QualifiedName node) {
		if(method != null) {
			method.addFeatures(new QualifiedNameHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(QualifiedType node) {
		if(method != null) {
			method.addFeatures(new QualifiedTypeHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ReturnStatement node) {
		if(method != null) {
			method.addFeatures(new ReturnStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SimpleName node) {
		if(method != null) {
			method.addFeatures(new SimpleNameHandler(node, this.method).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SimpleType node) {
		if(method != null) {
			method.addFeatures(new SimpleTypeHandler(node).handle());
		}
		return super.visit(node);
	}

	/**
	 *  SingleMemberAnnotation:
   	 *		@ TypeName ( Expression  )
	 */
	@Override
	public boolean visit(SingleMemberAnnotation node) {
		if(method != null) {
			method.addFeatures(new SingleMemberAnnotationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SingleVariableDeclaration node) {
		if(method != null) {
			method.addFeatures(new SingleVariableDeclarationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(StringLiteral node) {
		if(method != null) {
			method.addFeatures(new StringLiteralHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperConstructorInvocation node) {
		if(method != null) {
			method.addFeatures(new SuperConstructorInvocationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperFieldAccess node) {
		if(method != null) {
			method.addFeatures(new SuperFieldAccessHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SuperMethodInvocation node) {
		if(method != null) {
			method.addFeatures(new SuperMethodInvocationHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SwitchCase node) {
		if(method != null) {
			method.addFeatures(new SwitchCaseHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SwitchStatement node) {
		if(method != null) {
			method.addFeatures(new SwitchStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(SynchronizedStatement node) {
		if(method != null) {
			method.addFeatures(new SynchronizedStatementHandler().handle());
		}
		return super.visit(node);
	}
	
	/**
	 *  TagElement:
     *		[ @ Identifier ] { DocElement }
 	 * 	DocElement:
     *		TextElement
     *		Name
     *		MethodRef
     *		MemberRef
     *		{ TagElement }
	 */
	@Override
	public boolean visit(TagElement node) {
		// do nothing since it is not in method
		return super.visit(node);
	}

	/**
	 * TextElement:
     *		Sequence of characters not including a close comment delimiter 
	 */
	@Override
	public boolean visit(TextElement node) {
		// do nothing since it is not in method
		return super.visit(node);
	}

	@Override
	public boolean visit(ThisExpression node) {
		if(method != null) {
			method.addFeatures(new ThisExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(ThrowStatement node) {
		if(method != null) {
			method.addFeatures(new ThrowStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(TryStatement node) {
		if(method !=  null) {
			method.addFeatures(new TryStatementHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(TypeDeclaration node) {
		if(method != null) {
			method.addFeatures(new TypeDeclarationHandler(node).handle());
		}
		return super.visit(node);
	}

	/**
	 *  TypeDeclarationStatement:
     *		TypeDeclaration
     *		EnumDeclaration
	 */
	@Override
	public boolean visit(TypeDeclarationStatement node) {
		this.isInMethodInnerClass ++;
		return super.visit(node);
	}
	
	@Override
	public void endVisit(TypeDeclarationStatement node) {
		this.isInMethodInnerClass --;
		super.endVisit(node);
	}

	@Override
	public boolean visit(TypeLiteral node) {
		if(method != null) {
			method.addFeatures(new TypeLiteralHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(TypeParameter node) {
		if(method != null) {
			method.addFeatures(new TypeParameterHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(UnionType node) {
		if(method != null) {
			method.addFeatures(new UnionTypeHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationExpression node) {
		if(method != null) {
			method.addFeatures(new VariableDeclarationExpressionHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		if(method != null) {
			method.addFeatures(new VariableDeclarationFragmentHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(VariableDeclarationStatement node) {
		if(method != null) {
			method.addFeatures(new VariableDeclarationStatementHandler(node).handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(WhileStatement node) {
		if(method != null) {
			method.addFeatures(new WhileStatementHandler().handle());
		}
		return super.visit(node);
	}

	@Override
	public boolean visit(WildcardType node) {
		if(method != null) {
			method.addFeatures(new WildcardTypeHandler(node).handle());
		}
		return super.visit(node);
	}
}
