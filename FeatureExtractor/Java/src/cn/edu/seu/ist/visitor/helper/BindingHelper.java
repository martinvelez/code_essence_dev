package cn.edu.seu.ist.visitor.helper;

import org.eclipse.jdt.core.dom.ITypeBinding;

import cn.edu.seu.ist.model.token.TokenType;
import cn.edu.seu.ist.util.checker.JDKChecker;

public class BindingHelper {
	
	/**
	 * Get refined Info from ITypeBinding
	 * @param typeBinding
	 * @return
	 */
	public static String getRefinedInfo(ITypeBinding typeBinding) {
		String refinedInfo = TokenType.LOCAL_TYPE.toString();
		if(typeBinding != null) {
			if(typeBinding.isPrimitive()) {
				refinedInfo = typeBinding.getName();
			} else if(typeBinding.isArray()){
				int dimension = typeBinding.getDimensions();
				StringBuffer arrayBuffer = new StringBuffer(getRefinedInfo(typeBinding.getElementType()));
				for(int i=0; i<dimension; i++) {
					arrayBuffer.append(TokenType.LBRACKET.toString()).append(TokenType.RBRACKET.toString());
				}
				refinedInfo = arrayBuffer.toString();
			} else if(typeBinding.isTypeVariable()) {
				refinedInfo = TokenType.GENERIC_TYPE.toString();
			} else if(typeBinding.isParameterizedType()){
				StringBuffer paramBuffer = new StringBuffer();
				paramBuffer.append(typeBinding.getErasure().getQualifiedName()).append(TokenType.LT.toString());
				ITypeBinding[] argBindings = typeBinding.getTypeArguments();
				if(argBindings != null && argBindings.length > 0) {
					for(ITypeBinding argBinding : argBindings) {
						paramBuffer.append(getRefinedInfo(argBinding)).append(",");
					}
					paramBuffer.deleteCharAt(paramBuffer.length() - 1);
				}
				paramBuffer.append(TokenType.GT.toString());
				refinedInfo = paramBuffer.toString();
			} else if(typeBinding.isWildcardType()) {
				StringBuffer wildBuffer = new StringBuffer();
				wildBuffer.append(TokenType.QUES.toString());
				ITypeBinding boundBinding = typeBinding.getBound();
				if(boundBinding != null) {
					if(typeBinding.isUpperbound()) {
						wildBuffer.append(" extends ");
					} else {
						wildBuffer.append(" super ");
					}
					wildBuffer.append(getRefinedInfo(boundBinding));
				}
				refinedInfo = wildBuffer.toString();
			} else {
				String qualifiedName = typeBinding.getQualifiedName();
				if(JDKChecker.isFromJDK(qualifiedName)) {
					refinedInfo = qualifiedName;
				} 
			}
		}
		return refinedInfo;
	}
}
