package cn.edu.seu.ist.visitor.helper;

import org.eclipse.jdt.core.dom.ASTNode;

public class MethodInnerClassHelper {
	/**
	 * Check whether type is defined in method inner class
	 * @param node
	 * @return
	 */
	public static boolean isInMethodInnerClass(ASTNode node) {
		boolean isIn = false;
		while(node != null) {
			if(node.getNodeType() == ASTNode.TYPE_DECLARATION_STATEMENT) {
				isIn = true;
				break;
			}
			node = node.getParent();
		}
		return isIn;
	}
}
