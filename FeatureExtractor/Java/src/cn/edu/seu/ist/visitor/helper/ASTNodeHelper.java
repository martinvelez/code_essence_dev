package cn.edu.seu.ist.visitor.helper;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class ASTNodeHelper {
	
	/**
	 * Check whether the ASTNode is in the method Body (with consideration of anonymousClass)
	 * @param node
	 * @return
	 */
	public static boolean isInMethodBody(ASTNode node) {
		boolean isInBody = false;
		if(AnonymousClassHelper.isInAnonymousClass(node) 
				|| MethodInnerClassHelper.isInMethodInnerClass(node)) {
			isInBody = true;
		} else {
			if(simpleCheckInMethodBody(node)) {
				isInBody = true;
			}
		}
		return isInBody;
	}

	/**
	 * Get the root of the AST node
	 * @param node
	 * @return
	 */
	public static ASTNode getRoot(ASTNode node) {
		ASTNode pointer = node;
		while (node != null) {
			if(node.getNodeType() == ASTNode.COMPILATION_UNIT) {
				pointer = node;
				break;
			}
			node = node.getParent();
		}
		return pointer;
	}

	public static String getExceptionMessage(ASTNode node) {
		StringBuffer buffer = new StringBuffer();
		if(ASTNodeHelper.getClassDeclaration(node).getNodeType() == ASTNode.TYPE_DECLARATION) {
			TypeDeclaration classType = (TypeDeclaration)ASTNodeHelper.getClassDeclaration(node);
			buffer.append("Class: ").append(classType.getName()).append("\n");
		}
		if(ASTNodeHelper.getClassDeclaration(node).getNodeType() == ASTNode.ENUM_DECLARATION) {
			EnumDeclaration classType = (EnumDeclaration)ASTNodeHelper.getClassDeclaration(node);
			buffer.append("Enum: ").append(classType.getName()).append("\n");
		}
		if(ASTNodeHelper.getMethodDeclaration(node).getNodeType() == ASTNode.METHOD_DECLARATION) {
			MethodDeclaration methodType = (MethodDeclaration)ASTNodeHelper.getMethodDeclaration(node);
			buffer.append("Method: ").append(methodType.getName()).append("\n");
		}
		buffer.append("Token: ").append(node);
		return buffer.toString();
	}

	/**
	 * Check whether one ASTNode is the child of or equal with other ASTNode
	 * @param child
	 * @param parent
	 * @return
	 */
	public static boolean isChildOrEqual(ASTNode child, ASTNode parent) {
		boolean isChild = false;
		while(child != null) {
			if(child.equals(parent)) {
				isChild = true;
				break;
			}
			child = child.getParent();
		}
		return isChild;
	}

	/**
	 * Check whether one ASTNode is the child of or equal with the id of certain ASTNode
	 * @param child
	 * @param parentId
	 * @return
	 */
	public static boolean isChildOrEqual(ASTNode child, int parentId) {
		boolean isChild = false;
		while(child != null) {
			if(child.getNodeType() == parentId) {
				isChild = true;
				break;
			}
			child = child.getParent();
		}
		return isChild;
	}

	/**
	 * Get the root of the AST node
	 * @param node
	 * @return
	 */
	public static ASTNode getClassDeclaration(ASTNode node) {
		ASTNode pointer = node;
		while (node != null) {
			if(node.getNodeType() == ASTNode.TYPE_DECLARATION ||
					node.getNodeType() == ASTNode.ENUM_DECLARATION) {
				pointer = node;
				break;
			}
			node = node.getParent();
		}
		return pointer;
	}

	/**
	 * Check whether singleVariableDeclaration belongs to method declaration
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean isMethodFormalParameter(ASTNode node) {
		boolean isIn = false;
		ASTNode pointer = node;
		while(pointer != null) {
			if(pointer.getNodeType() == ASTNode.METHOD_DECLARATION) {
				MethodDeclaration methodDeclaration = (MethodDeclaration)pointer;
				List<SingleVariableDeclaration> parameters = methodDeclaration.parameters();
				for(SingleVariableDeclaration parameter : parameters) {
					if(node.equals(parameter)) {
						isIn = true;
						break;
					}
				}
			}
			pointer = pointer.getParent();
		}
		return isIn;
	}
	
	/**
	 * Get the root of the AST node
	 * @param node
	 * @return
	 */
	public static ASTNode getMethodDeclaration(ASTNode node) {
		ASTNode pointer = node;
		while (node != null) {
			if(node.getNodeType() == ASTNode.METHOD_DECLARATION) {
				pointer = node;
				break;
			}
			node = node.getParent();
		}
		return pointer;
	}
	
	/**
	 * Check whether the ASTNode is in the method Body 
	 * Note: without consideration of anonymousClass and methodInnerClass
	 * @param node
	 * @return
	 */
	private static boolean simpleCheckInMethodBody(ASTNode node) {
		boolean isInBody = false;
		while(node != null) {
			if(node.getNodeType() == ASTNode.BLOCK) {
				Block block = (Block)node;
				if(isChildOrEqual(block, ASTNode.METHOD_DECLARATION)) {
					isInBody = true;
					break;
				}
			}
			node = node.getParent();
		}
		return isInBody;
	}
}
