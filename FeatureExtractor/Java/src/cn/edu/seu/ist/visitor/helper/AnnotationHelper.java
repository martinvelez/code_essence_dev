package cn.edu.seu.ist.visitor.helper;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Annotation;

public class AnnotationHelper {
	
	/**
	 * Check whether the node is the child or equal with annotation node type 
	 * @param node
	 * @return
	 */
	public static boolean isChildOrEqualWithAnnotation(ASTNode node) {
		boolean isChildOrEual = false;
		if(ASTNodeHelper.isChildOrEqual(node, ASTNode.SINGLE_MEMBER_ANNOTATION)
				|| ASTNodeHelper.isChildOrEqual(node, ASTNode.MARKER_ANNOTATION) 
				|| ASTNodeHelper.isChildOrEqual(node, ASTNode.NORMAL_ANNOTATION)) {
			isChildOrEual = true;
		}
		return isChildOrEual;
	}
	
	/**
	 * Get the parent annotation node that contains ASTNode
	 * @param node
	 * @return
	 */
	public static Annotation getParentAnnotation(ASTNode node) {
		Annotation annotation = null;
		while(node != null) {
			if(node.getNodeType() == ASTNode.SINGLE_MEMBER_ANNOTATION ||
					node.getNodeType() == ASTNode.MARKER_ANNOTATION ||
					node.getNodeType() == ASTNode.NORMAL_ANNOTATION) {
				annotation = (Annotation)node;
				break;
			}
			node = node.getParent();	
		}
		return annotation;
	}
	
}
