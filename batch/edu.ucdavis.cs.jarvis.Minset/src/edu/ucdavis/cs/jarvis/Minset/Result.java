/**
 * 
 */
package edu.ucdavis.cs.jarvis.Minset;

import java.util.TreeSet;

import java.util.List;

/**
 * @author marvelez
 *
 */
public final class Result {
	private Integer pid;
	private Integer size;
	private TreeSet<Integer> minset;
	private TreeSet<Integer> duplicates;
	private TreeSet<Integer> strict_supersets;
	//private Integer[] strict_supersets;
	//private Integer[] duplicates;

	public Result(Integer pid, Integer size){
		this.pid = pid;
		this.size = size;
	}
	
	public void setMinDupSuperSets(List<TreeSet<Integer>> sets){
			
		// Deep copy of the minset
		this.minset = new TreeSet<Integer>();
		for(Integer element : sets.get(0)){
			this.minset.add(element);
		}
		this.duplicates = new TreeSet<Integer>();
		for(Integer element : sets.get(1)){
			this.duplicates.add(element);
		}
		this.strict_supersets = new TreeSet<Integer>();
		for(Integer element : sets.get(2)){
			this.strict_supersets.add(element);
		}
	}
	
	public String toString(){
		/*
		 * We need to replace the square brackets ('[' and ']') with curly
		 * braces ('{' and '}') so the output can be uploaded directly into 
		 * PostgreSQL. 
		 */
		String minset_str = minset.toString().replaceAll("\\[", "{").replaceAll("\\]", "}");
		String duplicates_str = duplicates.toString().replaceAll("\\[", "{").replaceAll("\\]", "}");
		String strict_supersets_str = strict_supersets.toString().replaceAll("\\[", "{").replaceAll("\\]", "}");
		return pid + "\t" + size + "\t" + 
				minset_str + "\t" + minset.size() + "\t" +
				duplicates_str + "\t" + duplicates.size() + "\t" +
				strict_supersets_str + "\t" + strict_supersets.size();
	}
}
