/**
 * @author marvelez
 */

package edu.ucdavis.cs.jarvis.Minset;


import gnu.trove.map.hash.THashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class computes the minset for each set in a universe U of sets. 
 * 
 * @author marvelez
 *
 */
public class Minset {
	
	/**
	 * This file should contain SETID\tSET.
	 */
	private static String collFilename;
	/**
	 * This file should contain a list of set ids for which we want to compute minsets.
	 */
	private static String subcollFilename;
	private static THashMap<Integer, int[]> sid_eids;
	private static ArrayList<Integer> sample;
	private static THashMap<Integer, ArrayList<Integer>> eid_sids;
	
	
	/** 
	 * @param args
	 */
	public static void main(String[] args) {		
		if(args.length != 2){
			System.err.println("USAGE: minset COLLFILE SUBCOLLFILE");
			System.exit(1);			
		} else {
			collFilename = args[0];
			subcollFilename = args[1];
		}

		sid_eids = parseCollFile();
		System.err.println("(SID => EID) size = " + sid_eids.size());		
		sample = parseSubcollFile();
		System.err.println("Sample size = " + sample.size());
		eid_sids = invert_sid_eids();
		System.err.println("(EID => SIDS) size = " + eid_sids.size());
		
		for(Integer sid:sample){
			ArrayList<Integer> minset = minset(sid);
			System.out.println(sid + "\t" + sid_eids.get(sid).length + "\t" + minset + "\t" + minset.size());
		}
	}//main

	
	private static THashMap<Integer, ArrayList<Integer>> invert_sid_eids() {
		//System.err.println("invert_sid_eids()");
		THashMap<Integer, ArrayList<Integer>> eid_sids = new THashMap<Integer, ArrayList<Integer>>(10,4);
		//Initialize eid_sids
		//System.err.println("eid_sids: initializing");
		for(int sid:sample){
			if(sid_eids.containsKey(sid)){
				for(Integer eid:sid_eids.get(sid)){
					eid_sids.put(eid, new ArrayList<Integer>());
				}	
			} else {
				System.err.println("ERROR: Your list of sets contains an invalid id!: " + sid);
				System.exit(1);
			}
		}
		//Populate
		//System.err.println("eid_sids: populating");
		for(Entry<Integer, int[]> entry:sid_eids.entrySet()){
			for(int i = 0; i < entry.getValue().length; i++){
				if(eid_sids.containsKey(entry.getValue()[i])){
					eid_sids.get(entry.getValue()[i]).add(entry.getKey());
				}
			}
			//System.out.println("eid_size.size() = " + eid_size.size());
		}
		
		return eid_sids;
	}//invert_sid_eids


	/**
	 * Read the entire file line by line to create C, the collection of sets.
	 */
	private static THashMap<Integer, int[]> parseCollFile(){
		System.err.println("parseCollFile(): " + collFilename);
		BufferedReader br = null;
		THashMap<Integer, int[]> sid_eids = new THashMap<Integer, int[]>(500000,4);
		try {
			 
			String sCurrentLine;
			br = new BufferedReader(new FileReader(collFilename));
			
			while ((sCurrentLine = br.readLine()) != null) {
				int tabPos= sCurrentLine.indexOf('\t');
				Integer sid = Integer.parseInt(sCurrentLine.substring(0,tabPos));
				String[] elements_str = sCurrentLine.substring(tabPos+2,sCurrentLine.length()-1).split(","); 
				ArrayList<Integer> eids = new ArrayList<Integer>();
				for(String e:elements_str){
					eids.add(Integer.parseInt(e));
				}
				sid_eids.put(sid, buildIntArray(eids));
			}//while
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}// try - catch
		
		return sid_eids;
	}//parseCollFile
	
	
	/**
	 * Get the list of sets for which to compute minsets.
	 * @return sids
	 */
	private static ArrayList<Integer> parseSubcollFile() {
		System.err.println("parseSubcollFile(): " + subcollFilename);
		ArrayList<Integer> sids = new ArrayList<Integer>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(subcollFilename));
			
			String line;
			while((line = br.readLine()) != null){
				int sid = Integer.parseInt(line);
				sids.add(sid);
			}
			br.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // try - catch
		
		return sids;
	}


	/**
	 * Implementation of the minset algorithm
	 * C is the universe of sets minus the target set itself
	 * 
	 * @param targetSet
	 * @param C
	 * @return s_star
	 */
	public static ArrayList<Integer> minset(int target_sid){
		// Build local hash: eid to sids
		HashMap<Integer, TreeSet<Integer>> set = new HashMap<Integer, TreeSet<Integer>>();
		for(int eid:sid_eids.get(target_sid)){
			TreeSet<Integer> sids = new TreeSet<Integer>(eid_sids.get(eid));
			sids.remove(target_sid);
			set.put(eid, sids);
		}
		/**
		 * This is the list of sets containing the element e.
		 * For optimization, we'll just put an arbitrary element in there so 
		 * the loop will run at least once.
		 */
		Set<Integer> C = new TreeSet<Integer>();
		C.add(1);
		ArrayList<Integer> minset = new ArrayList<Integer>();
		
		
		while(!C.isEmpty() && set.size() != 0){
			// Choose the rarest element e
			int rarestElement = findRarestElement(set);
			// Add the element e to S*
			minset.add(rarestElement);
			// consider only sets that contain the rarestElement
			C = set.get(rarestElement);
			// Peel off e from the target set S
			
			set.remove(rarestElement);
			for(Integer k: set.keySet()){
				set.get(k).retainAll(C);
			}
			/*
			 * The following is done for optimization.
			 * At this point, if the current set shares all of its elements a 
			 * fixed group of sets, then it has supersets.
			 */
			boolean has_supersets = true;
			for(int eid:set.keySet()){
				has_supersets = set.get(eid).equals(C);
				if(!has_supersets){break;}
			}
			if(has_supersets){break;}			
		}//while there are possible supersets and the set is not empty

		if(!C.isEmpty()){
			minset.clear(); // the minset we computed is not a minset
		}
		
		return minset;
	}//minset
	
	
	/**
	 *  Find the rarest element in a set
	 * @param elementCount
	 * @return
	 */
	private static Integer findRarestElement(HashMap<Integer, TreeSet<Integer>> set){
		int rarest_element = -1;
		int rarest_element_count = -1;
		
		for(Entry<Integer, TreeSet<Integer>> entry: set.entrySet()){
			if(rarest_element_count == -1 || entry.getValue().size() < rarest_element_count){
				rarest_element = entry.getKey();
				rarest_element_count = entry.getValue().size();
			}
			if(rarest_element_count == 0){ break;} // optimization
		}
		
		return rarest_element;
	}//findRarestElement
		
	private static int[] buildIntArray(List<Integer> integers) {
	    int[] ints = new int[integers.size()];
	    int i = 0;
	    for (Integer n : integers) {
	        ints[i++] = n;
	    }
	    return ints;
	}
}
