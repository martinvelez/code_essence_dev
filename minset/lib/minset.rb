require 'pg'
require 'progressbar'
require 'benchmark'


# mid = method id
# fid = feature id
class Minset

	def initialize(opts={},conf={})
		@opts = opts
		@conn = PG::Connection.open(conf[@opts[:dbname]])
		@mf = {} # method-to-features (mid -> fids)
		@fm = {} # feature-to-methods (fid -> mids)
		@sample = [] # sample of mids
	end

	# Prepare, and start minset algorithm
	def start
		prepare_statements; get_mids; build_hashes
		exitstatus = 0	
		time = Benchmark.realtime { exitstatus = minset }
		puts "#{@opts[:experiment]}, #{@opts[:file]}, #{time}, only minset"
		return exitstatus
	end

	# Prepare SQL statements to increase execution speed
	def prepare_statements
		minsets = "minsets_#{@opts[:experiment]}"
		minsets = "#{minsets}_largest" if @opts[:largest] 
		puts "Using table: #{minsets}"
		sql = %{INSERT INTO #{minsets} 
			VALUES ($1, $2, $3::integer[], $4, $5::integer[], $6, $7::integer[], $8)}
		@conn.prepare('insert_minset', sql)
		sql = "SELECT mid, fids FROM mf_#{@opts[:experiment]}_compact"	
		@conn.prepare('get_mf', sql) 
	end


	# Determine the method ids for which the minset will be computed
	# Method ids can be:
	#  * a random sample of all of the methods in the database
	#  * listed in a file
	#  * queried when given a list of FQMNs in a file
	#  * in a specified range (offset into methods table, limit/count)
	def get_mids
		case @opts[:mode]
		when "id"
			@sample = File.open(@opts[:file], 'r').read.split("\n")
		when "fqmn"
			File.open(@opts[:file], 'r').read.split("\n").each do |method_name|
				sql = 'SELECT id FROM methods where method_name=$1'	
				@sample.push(@conn.exec(sql,[method_name]).getvalue(0,0))
			end
		when "range"
			sql = 'SELECT id from methods LIMIT $2 OFFSET $1'
			@sample = @conn.exec(sql,@opts[:range]).column_values(0)
		when "sample"
			@sample = @conn.exec_prepared('sample',[@opts[:sample]]).column_values(0)
		end
		@sample = @sample.map!(&:to_i)
	end


	# Find the emptysets in our sample at this abstraction level
	def find_emptysets
		empties = []
		pbar = ProgressBar.new("empties", @sample.size) if @opts[:pbar]		
		@conn.transaction do |conn|
			@sample.each do |mid|
				if not @mf.has_key?(mid)
					conn.exec_prepared('insert_minset', [mid,0,'{}',0,'{}',0,'{}',0])
					empties << mid
				end
				pbar.inc if @opts[:pbar]
			end	# each 
		end # transaction
		pbar.finish if @opts[:pbar]
		@sample = @sample - empties 
	end


	# Greedily construct the minset:
	#  * Select a method's rarest feature
	#  * Eliminate all methods from universe without that feature
	#  * Stop if all features have been selected OR no other methods share minset
	def minset
		find_emptysets
		pbar = ProgressBar.new("minset", @sample.size) if @opts[:pbar]	
		@conn.transaction do |conn|
			@sample.each do |mid|
				fm = {} # hash { "feature" => [methods containing "feature"]} 
				@mf[mid].each { |fid| fm[fid] = @fm[fid] - [mid] } # build fm
				
				f_not_m = [1] # Optimization: f_not_m only needs to be non-empty
				strict_supersets, dups, minset = [], [], []

				begin
					while not f_not_m.empty? and not fm.empty?
						f_min = rarest_feature(fm)
						minset.push(f_min) # fids will be in order (rarest first) 
						f_not_m = fm[f_min] # F_{-m} = F_{-m}(f_min)
						fm.delete(f_min)	# delete the rarest feature
						# keep only methods in rarest feature
						# TODO: can we minimize the number of times we do the &
						# There must many duplicates for 103,104,105
						fm.each { |fid, mids| fm[fid] = mids & f_not_m } 
						# Optimization: break if all of the f_not_m mids have the 
						# same features as the target method
						break if fm.all?{ |fid, mids| mids == f_not_m } 
					end
				rescue # some error inside begin block
					puts "ERROR: #{mid}:minset: need to investigate"
					minset, @mf[mid].size = [], -1 # Magic values/flags
				end # begin
	
				minset = [] if not f_not_m.empty?
				f_not_m.each do |other_mid|	
					if @mf[mid] == @mf[other_mid]	
						dups << other_mid
					else
						strict_supersets << other_mid
					end # if
				end # each

				#minset_size = conn.exec("SELECT sum(index) FROM features_#{@opts[:experiment]} 
				#	WHERE id IN (#{minset.inspect[1...-1]})").getvalue(0,0)
		
				conn.exec_prepared('insert_minset', [mid, @mf[mid].size, 
					minset.inspect.gsub('[','{').gsub(']','}'), minset.size, 
					dups.sort!.inspect.gsub('[','{').gsub(']','}'), dups.size,
					strict_supersets.sort!.inspect.gsub('[','{').gsub(']','}'), 
					strict_supersets.size])

				pbar.inc if @opts[:pbar]
			end # each
		end # transaction
		pbar.finish if @opts[:pbar]
		
		return 0
	end # minset_mem


	# Given an adjacency list, {"feature" => [methods containing "feature"]},
	# returns the rarest feature, the feature appearing in the least number of
	# methods
	def rarest_feature(fm)
		f_min = "" # the feature with the
		f_min_count = nil # initial |U'(f)| 
		fm.each do |f, mids|
			if f_min_count.nil? or mids.size < f_min_count	
				f_min_count = mids.size	
				f_min = f
				break if f_min_count == 0 # found a feature not present in other methods
			end
		end
		f_min
	end


	# Read the MethodFeature junction table and create two hashes for quick
	# lookups 
	# Relatively fast.  Expensive in memory.
	def build_hashes	
		# BUILD MF, Takes <21 mins (worst case)
		puts "Querying DB for MF..." if @opts[:debug]
		res = @conn.exec_prepared('get_mf')
		puts "Building hash..." if @opts[:debug]
		pbar = ProgressBar.new("mf hash", res.ntuples) if @opts[:pbar]	
		res.each do |mid_fids|
			#arrays must be sorted for == to work later 
			@mf[mid_fids['mid'].to_i] = 
				mid_fids['fids'][1...-1].split(',').map!(&:to_i).sort!
			pbar.inc if @opts[:pbar]
		end
		pbar.finish if @opts[:pbar]
		res.clear

		@sample.delete_if { |mid| !@mf.has_key?(mid) }

		# BUILD FM, Takes <2 mins (worst case)
		puts "Building hash..." if @opts[:debug]
		@sample.each do |mid|
			@mf[mid].each do |fid|
				@fm[fid] = []
			end
		end

		pbar = ProgressBar.new("fm hash", @mf.length) if @opts[:pbar]
		@mf.each do |mid, fids|
			fids.each do |fid|
				if @fm.has_key?(fid)
					@fm[fid] << mid
				end
			end	
			pbar.inc if @opts[:pbar]
		end
		pbar.finish if @opts[:pbar]
			
		puts "#{@opts[:experiment]}, fm.size = #{@fm.length}, mf.size = #{@mf.length}"
	end
end # class
