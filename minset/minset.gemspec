Gem::Specification.new do |s|
	s.name = 'minset'
	s.version = '0.1.0'
	s.date = '2013-09-13'
	s.summary = 'Implementation of Minset algorithm'
	s.description = 'Minset finds the minimal distinguishing subset of a set with respect to other sets'
	s.authors = ['Martin Velez, Dong Qiu, You Zhou, Earl Barr, Zhendong Su']
	s.email = 'mvelez999@gmail.com'
	s.files = ["bin/minset", "lib/minset.rb", "conf/conf.yml.sample"] + ["README.rdoc","LICENSE"]
	s.homepage = 'https://bitbucket.org/martinvelez/code_essence_dev/downloads'
	s.require_paths = ["bin"]
	s.executables << 'minset'
end
