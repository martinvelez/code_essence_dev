#!/usr/bin/env ruby

require 'rake'
require 'progressbar'
require 'pg'
require 'yaml'
require 'tempfile'


conf_file = File.expand_path(File.join(File.dirname(__FILE__),'..','conf','conf.yml'))
conf = YAML.load_file(conf_file)
conn = PG.connect(conf["raw"])

# This piece of code initialized the 'Rake' application
# A 'Rake' application provided a standard command line interface
Rake.application.init('db')


# START TASKS DEFINITIONS
# -----------------------

namespace :db do

	desc "Create PostgreSQL Database"
	task :create do
		sql = [%{
			CREATE TABLE methods_unfiltered (
				id SERIAL PRIMARY KEY,
				method_name TEXT NOT NULL,
				source TEXT NOT NULL,
				line_start INTEGER NOT NULL,
				line_end INTEGER NOT NULL
			)},
			%{CREATE TABLE MethodFeature_unfiltered (
				mid INTEGER NOT NULL,	
				body BOOLEAN NOT NULL,
				lexeme TEXT NOT NULL,
				basic_token_type TEXT NOT NULL,
				lexer_token_type TEXT NOT NULL,
				category TEXT NOT NULL,
				subcategory TEXT,
				count INTEGER NOT NULL
			)},
			%{CREATE TABLE methods_compressed (
					mid INTEGER NOT NULL,
					compressed_source_code TEXT NOT NULL
			)}, 
			%{CREATE TABLE dups (
				mids INTEGER[]
			)}]

			sql .each{ |s| conn.exec(s) }
	end # task

	# COPY requires that the user be a SuperUser 
	desc "Imports methods into PostgreSQL"
	task :import_methods do
		success = true
		bad_file = ""
		dir = File.expand_path(File.join(File.dirname(__FILE__),"..","S"))
		method_files = Dir.glob("#{dir}/**/Methods.csv")

		pbar = ProgressBar.new("methods", method_files.size)
		method_files.each do |f|
			begin
				conn.exec("COPY methods_unfiltered FROM '#{f}' WITH CSV")
			rescue
				success = false
				bad_file = f	
				break
			end
			pbar.inc
		end
		pbar.finish

		puts "BAD FILE: #{bad_file}" if !success
	end # task

	# First, manually clean these files:
	# S/sourceforge/mobile-midp/MethodFeature.csv",
	# S/eclipse/cbi/MethodFeature.csv",
	# S/eclipse/jdt/MethodFeature.csv",
	# S/apache/poi-3.8/MethodFeature.csv",
	# S/apache/apache-harmony/MethodFeature.csv"
	#
	# I use VIM to replace all the \x00 and \x01 characters from literal string lexemes.
	# I use the following search-and-replace commands: 
	# %s/\%x00//g
	# %s/\%x01//g
	#
	# I have noted the known bad files for our corpus.  If another corpus is
	# used, that corpus may have different bad files.
	desc "Import MethodFeature relation into PG"
	task :import_mf do
		success = true
		dir = File.expand_path(File.join(File.dirname(__FILE__),"..","S"))
		mf_files = Dir.glob("#{dir}/**/MethodFeature.csv")
		
		pbar = ProgressBar.new("mf", mf_files.size)
		conn.transaction do |conn|
			mf_files.each do |f|
				begin
					conn.exec("COPY MethodFeature_unfiltered from '#{f}' WITH CSV")
				rescue
					success = false
					bad_file = f
					break
				end
				pbar.inc
			end
			pbar.finish
		end # transaction

		puts "BAD FILE: #{bad_file}" if !success
	end # task

	# Takes about 3 minutes on 100M LOC
	# First, manually clean the following files.  Remove the \x00 and \x01 chars.
	# S/sourceforge/mobile-midp/Compressed.csv
	# S/eclipse/cbi/Compressed.csv
	# S/eclipse/jdt/Compressed.csv
	# S/apache/poi-3.8/Compressed.csv
	# S/apache/apache-harmony/Compressed.csv
	# Note: Same projects as in import_mf
	desc "Import compressed method strings"
	task :import_compressed do
		success = true
		dir = File.expand_path(File.join(File.dirname(__FILE__),"..","S"))
		files = Dir.glob("#{dir}/**/Compressed.csv")
		bad_file = ""

		pbar = ProgressBar.new("compressed", files.size)
		conn.transaction do |conn|
			files.each do |f|
				begin
					conn.exec("COPY methods_compressed FROM '#{f}' WITH CSV")
				rescue
					success = false
					bad_file = f
					break
				end
			end	
			pbar.inc
		end # transaction
		pbar.finish

		puts "BAD FILE: #{bad_file}" if !success
	end

	desc "Import bad files from CSV"
	task :import_bad_files do
			#	
	end	

	desc "create method sizes table"
	task :size_filter do
		conn.exec("CREATE TABLE method_body_sizes AS 
			SELECT mid, sum(count) AS size FROM methodfeature_unfiltered 
			WHERE body=true 
			GROUP BY mid")

		conn.exec("CREATE TABLE method_body_size_filter AS SELECT mid from method_body_sizes") 
	end
	

	desc "Filter the methods_compressed table by size"
	task :filter_methods_compressed do	
		# Filter methods_compressed table by size
		sql = %{CREATE TABLE methods_compressed_filtered AS
			SELECT method_body_size_filter.mid, compressed_source_code 
			FROM method_body_size_filter 
			LEFT OUTER JOIN methods_compressed 
			ON (method_body_size_filter.mid = methods_compressed.mid)
		}
		conn.exec(sql)
	end

	

	desc "Create list of duplicate methods to ignore"
	task :dups_filter do
		methods_compressed_inverted = {}

		# Get data from database
		pbar = ProgressBar.new("Getting Data", 1)
		arrays = conn.exec("select * from methods_compressed_filtered").values
		pbar.inc
		pbar.finish

		puts "#{arrays.size} methods"

		# Invert mid => source, want source => [mid1, mid2, ...]
		pbar = ProgressBar.new("Inverting", arrays.size)
		arrays.each do |a|
			mid, source = a[0].to_i, a[1]
			if methods_compressed_inverted.has_key?(source)
				methods_compressed_inverted[source] << mid
			else
				methods_compressed_inverted[source] = [mid]
			end
			pbar.inc	
		end
		pbar.finish

		puts "#{methods_compressed_inverted.size} unique compressed source code strings"	

		arrays = [] # FREE MEMORY

		# filter source => mids hash, keep only those keys where more than 
		# mid has that same source 	
		pbar = ProgressBar.new("Filter", 1)	
			methods_compressed_inverted.select!{|source,mids| mids.size > 1}	
			pbar.inc
		pbar.finish

		puts "#{methods_compressed_inverted.size} compressed source code with collisions"	

		# Sort each duplicate group in ascending order
		methods_compressed_inverted.map { |key,mids| mids.sort!.uniq! }

		# Store groups of duplicates in database	
		# prepare statement
		conn.prepare('store_dups', "INSERT INTO dups VALUES ($1::integer[])")	

		pbar = ProgressBar.new("dups", methods_compressed_inverted.size)
		conn.transaction do |conn|
			methods_compressed_inverted.each do |source, mids|			
				conn.exec_prepared('store_dups', [mids.inspect.gsub('[','{').gsub(']','}')])
				pbar.inc
			end
		end
		pbar.finish
		
		# create view of dups to keep and dups to remove
		sql = %{CREATE OR REPLACE VIEW dups_keep_and_remove AS
				SELECT mids[1] AS keeper, mids[2:array_upper(mids,1)] AS dups_to_filter 
				FROM dups
		}
		conn.exec(sql)
		sql = %{CREATE OR REPLACE VIEW dups_filter AS
			SELECT unnest(dups_to_filter) AS mid FROM dups_keep_and_remove
		}
		conn.exec(sql)
		
		sql = %{create or replace view dups_distribution as 
			select array_length(mids,1) AS size, count(*) 
			from dups GROUP BY size ORDER BY size
		}
		conn.exec(sql)
	end

	desc "Filter methods and methodfeature table"
	task :filter do 
		sql = [%{
			CREATE OR REPLACE VIEW methods_filter AS
			SELECT mid FROM method_body_size_filter 
			EXCEPT select mid FROM dups_filter
		},
		%{
			CREATE OR REPLACE VIEW methods AS
			SELECT methods_filter.mid AS id, method_name, source, line_start, line_end
			FROM methods_filter
			LEFT OUTER JOIN methods_unfiltered 
				ON (methods_filter.mid=methods_unfiltered.id)
		},
		%{
			CREATE OR REPLACE VIEW methodfeature AS
			SELECT methods_filter.mid, lexeme, basic_token_type, 
				lexer_token_type, category, subcategory, count 
			FROM methods_filter 
			LEFT OUTER JOIN 
				(SELECT * FROM MethodFeature_unfiltered where body=true) AS t
				ON (methods_filter.mid=t.mid)	
		}]
		
		sql.each { |s| conn.exec(s) }
	end

	desc "Export filtered methodfeature and methods tables"
	task :export do
		#fname = File.join(Dir.pwd,'methodfeature.csv')
		#sql = "COPY (SELECT * from methodfeature) TO '#{fname}' WITH CSV"	
		#conn.exec(sql)
		#fname = File.join(Dir.pwd,'methods.csv')
		#sql = "COPY (SELECT * from methods) TO '#{fname}' WITH CSV"	
		##conn.exec(sql)

		#\copy (SELECT * FROM methods) TO 'methods.sql'	
		#\copy (SELECT * from methodfeature) TO 'methodfeature.sql'
	end

end # namespace


task :default do
	puts "USAGE: '#{Rake.application.name} -T' to get list of all available commands"
end
# ------------------
# END TASK DEFINITIONS

Rake.application.top_level
