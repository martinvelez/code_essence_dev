#!/usr/bin/env ruby

# Create indices to increase speed of data retrieval operations.
# We want rapid random lookups and efficient access.  
# Updating the index causes slower writes and increased storage space. 
# Thus it needs to be done after filtering, and before minsets.
#
# Set your maintenance_work_mem very high to increase speed

require 'pg'
require 'progressbar'
require 'yaml'


# DEFAULTS
conf_file = File.expand_path(File.join(File.dirname(__FILE__),'..','conf','conf.yml'))
conf = YAML.load_file(conf_file)
conn = PG.connect(conf["dev"])


# This is why we create them at the end.
pbar = ProgressBar.new("VIEWS", 4)
begin
	sql = %{
		CREATE OR REPLACE VIEW results_base AS
			SELECT m.mid, mf.old_size,m.new_size, ROUND(CAST(new_size as numeric)/old_size,2) AS ratio, methods.method_name, methods.source 
			FROM (SELECT mid,COUNT(*) as new_size FROM minsets GROUP BY mid) as m 
			LEFT OUTER JOIN methods on (m.mid=methods.id) 
			LEFT OUTER JOIN (SELECT * FROM (SELECT mid,COUNT(*) AS old_size 
			FROM methodfeature GROUP BY mid) AS x) AS mf ON (m.mid=mf.mid)
	}
	conn.exec(sql)
	pbar.inc # 1
rescue PG::Error
	puts "NOTICE: indexing failed"
end
pbar.finish
