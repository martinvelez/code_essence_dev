#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')


#read the data
yield_data = read.table(file = "../sec4-2/min50_min14_yield.txt",
                        col.name = c("EXP", "Number", "Methods"))
yield_data$Methods = factor(yield_data$Methods, 
                             levels=c("Have_more_than_10_supersets", "Have_10_or_fewer_supersets", "Have_minsets"))
yield = ggplot(yield_data, aes(x=EXP, y=Number, fill=Methods)) + 
	geom_bar(stat="identity", width=0.4) + 
  ylab("Count") + 
  xlab("Lexicon") + 
  scale_fill_manual(values=c("#B0E2FF", "#79AEFF","#04477C"))  + 
  scale_y_continuous(limits=c(0,11500), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme_bw() + 
  theme(plot.margin=unit(c(0.1,0.1,0.1,0.2), "cm")) + 
  theme(panel.border = element_rect(colour = "black")) +
	theme(legend.position=c(0.5, 0.925), legend.direction="vertical") +
  theme(axis.title.y=element_text(vjust=0.3)) +
  theme(axis.title=element_text(size=15, face="bold", vjust=0.2)) + 
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=13, face="bold")) +
  theme(legend.text=element_text(size=11, face="bold")) + 
  theme(legend.title=element_blank()) +
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Non-Threshable (73.07%)", x=1.3, y=6336.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=1.2, xend=1.4, y=9990, yend=9990, size=0.5) + 
  annotate("segment", x=1.2, xend=1.4, y=2684, yend=2684, size=0.5) +
  annotate("segment", x=1.3, xend=1.3, y=3900, yend=2784, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.3, xend=1.3, y=8700, yend=9890, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Non-Threshable (70.27%)", x=2.3, y=6484.5, angle=90, vjust=0.5,size=4.2) + 
  annotate("segment", x=2.2, xend=2.4, y=9998, yend=9998, size=0.5) + 
  annotate("segment", x=2.2, xend=2.4, y=2972, yend=2972, size=0.5) +
  annotate("segment", x=2.3, xend=2.3, y=4100, yend=3072, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.3, xend=2.3, y=8800, yend=9808, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Non-Threshable (58.56%)", x=3.3, y=7072, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=3.2, xend=3.4, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=3.2, xend=3.4, y=4145, yend=4145, size=0.5) +
  annotate("segment", x=3.3, xend=3.3, y=4700, yend=4245, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=3.3, xend=3.3, y=9400, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Non-Threshable (55.21%)", x=4.3, y=7239.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=4.2, xend=4.4, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=4.2, xend=4.4, y=4480, yend=4480, size=0.5) +
  annotate("segment", x=4.3, xend=4.3, y=4900, yend=4580, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=4.3, xend=4.3, y=9600, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) 
	
duplicates_data = read.table(file = "../sec4-2/min50_min14_duplicates.txt",
                             col.name = c("EXP", "Number", "Methods"))

duplicates = ggplot() + 
	geom_bar(data=duplicates_data, aes(x=EXP, y=Number, fill=Methods), 
           stat="identity", width=0.4) +  
  scale_fill_manual(values=c("#04477C", "#B0E2FF")) +
  xlab("Lexicon") + 
  scale_y_continuous(limits=c(0,11500), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme_bw() +
  theme(plot.margin=unit(c(0.1,0.1,0.1,0.2), "cm")) + 
  theme(panel.border = element_rect(colour = "black")) +
  theme(legend.position=c(0.5, 0.945), legend.direction="vertical") +
  theme(axis.title.y=element_blank()) +
  theme(axis.title=element_text(size=15, face="bold", vjust = 0.2)) + 
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=13, face="bold")) +
  theme(legend.text=element_text(size=11, face="bold")) + 
  theme(legend.title=element_blank()) +
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Do Not Have Minset (85.52%)", x=1.3, y=5718.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=1.2, xend=1.4, y=9990, yend=9990, size=0.5) + 
  annotate("segment", x=1.2, xend=1.4, y=1448, yend=1448, size=0.5) +
  annotate("segment", x=1.3, xend=1.3, y=2900, yend=1548, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.3, xend=1.3, y=8400, yend=9890, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Do Not Have Minset (83.98%)", x=2.3, y=5800, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=2.2, xend=2.4, y=9998, yend=9998, size=0.5) + 
  annotate("segment", x=2.2, xend=2.4, y=1603, yend=1603, size=0.5) +
  annotate("segment", x=2.3, xend=2.3, y=3000, yend=1703, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.3, xend=2.3, y=8500, yend=9898, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Do Not Have Minset (76.15%)", x=3.3, y=6192.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=3.2, xend=3.4, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=3.2, xend=3.4, y=2386, yend=2386, size=0.5) +
  annotate("segment", x=3.3, xend=3.3, y=3400, yend=2486, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=3.3, xend=3.3, y=8900, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Do Not Have Minset (74.08%)", x=4.3, y=6296, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=4.2, xend=4.4, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=4.2, xend=4.4, y=2593, yend=2593, size=0.5) +
  annotate("segment", x=4.3, xend=4.3, y=3500, yend=2693, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=4.3, xend=4.3, y=9000, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) 
  

#png(filename="10000_random_yield.png", type="cairo",units="in", width=9, height=7, res=100)
pdf("min50_min14_yield.pdf", width=8, height=6)
#print(yield)
multiplot(yield, duplicates, cols=2)

dev.off()