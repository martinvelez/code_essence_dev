#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

#include the package
library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')

#read the data
min1 = read.table(file = "../sec4-2/min50_min1.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min2 = read.table(file = "../sec4-2/min50_min2.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min3 = read.table(file = "../sec4-2/min50_min3.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min4 = read.table(file = "../sec4-2/min50_min4.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min1 = subset(min1, Method_size>0)
min2 = subset(min2, Method_size>0)
min3 = subset(min3, Method_size>0)
min4 = subset(min4, Method_size>0)

min1_m= read.table(file = "../sec4-3/min50_min1_multiplicity.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min2_m= read.table(file = "../sec4-3/min50_min2_multiplicity.sql", 
                   col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min3_m= read.table(file = "../sec4-3/min50_min3_multiplicity.sql", 
                   col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min4_m= read.table(file = "../sec4-3/min50_min4_multiplicity.sql", 
                   col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min1_m = subset(min1_m, Method_size>0)
min2_m = subset(min2_m, Method_size>0)
min3_m = subset(min3_m, Method_size>0)
min4_m = subset(min4_m, Method_size>0)


#clean the data
min1_threshed = subset(min1,Minset_size>0)
min2_threshed = subset(min2,Minset_size>0)
min3_threshed = subset(min3,Minset_size>0)
min4_threshed = subset(min4,Minset_size>0)

min1_m_threshed = subset(min1_m,Minset_size>0)
min2_m_threshed = subset(min2_m,Minset_size>0)
min3_m_threshed = subset(min3_m,Minset_size>0)
min4_m_threshed = subset(min4_m,Minset_size>0)

min1$multiplicity = "Without_multiplicity"
min1_m$multiplicity = "With_multiplicity"
min1_combined = rbind(min1, min1_m)

min2$multiplicity = "Without_multiplicity"
min2_m$multiplicity = "With_multiplicity"
min2_combined = rbind(min2, min2_m)

min3$multiplicity = "Without_multiplicity"
min3_m$multiplicity = "With_multiplicity"
min3_combined = rbind(min3, min3_m)

min4$multiplicity = "Without_multiplicity"
min4_m$multiplicity = "With_multiplicity"
min4_combined = rbind(min4, min4_m)

min1_threshed$multiplicity = "Without_multiplicity"
min1_m_threshed$multiplicity = "With_multiplicity"
min1_threshed_combined = rbind(min1_threshed, min1_m_threshed)

min2_threshed$multiplicity = "Without_multiplicity"
min2_m_threshed$multiplicity = "With_multiplicity"
min2_threshed_combined = rbind(min2_threshed, min2_m_threshed)

min3_threshed$multiplicity = "Without_multiplicity"
min3_m_threshed$multiplicity = "With_multiplicity"
min3_threshed_combined = rbind(min3_threshed, min3_m_threshed)

min4_threshed$multiplicity = "Without_multiplicity"
min4_m_threshed$multiplicity = "With_multiplicity"
min4_threshed_combined = rbind(min4_threshed, min4_m_threshed)

min1_threshed_plot = ggplot(min1_threshed_combined, aes(Minset_size, fill = multiplicity)) + 
  geom_histogram(alpha = 0.8, position="identity", binwidth=1) + 
  scale_fill_manual(values=c("#04477C", "#EBF5FF")) + 
  scale_y_continuous(limits=c(0,1400), breaks=c(0, 300, 600, 900, 1200)) +
  xlab("Minset Size (MIN1)") +
  ylab("Count") + 
  theme(axis.title = element_text(size=15, face="bold", colour="#8C8C8C")) + 
  theme(legend.position = c(0.5, 0.95), legend.direction="horizontal") + 
  theme(legend.background=element_rect(fill="transparent")) +
  theme(legend.title = element_blank()) + 
  theme(legend.text = element_text(size=11, face="bold")) +  
  theme(plot.margin=unit(c(0.1, 0.1, 0.2, 0), "cm")) +
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=13))
 
min2_threshed_plot = ggplot(min2_threshed_combined, aes(Minset_size, fill = multiplicity)) + 
  geom_histogram(alpha = 0.8, position="identity", binwidth=1) + 
  scale_fill_manual(values=c("#04477C", "#EBF5FF")) + 
  scale_x_continuous(limits=c(0,13), breaks=c(0, 3, 6, 9, 12)) + 
  scale_y_continuous(limits=c(0,1300), breaks=c(0, 300, 600, 900, 1200)) +
  xlab("Minset Size (MIN2)") + 
  theme(axis.title.x = element_text(size=15, face="bold", colour="#8C8C8C")) + 
  theme(legend.position = "top") + 
  theme(legend.title = element_blank()) + 
  theme(legend.text = element_text(size=12, face="bold")) + 
  theme(plot.margin=unit(c(0.1, 0.1, 0.2, -0.1), "cm")) +
  theme(axis.title.y = element_blank()) + 
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=13))

min3_threshed_plot = ggplot(min3_threshed_combined, aes(Minset_size, fill = multiplicity)) + 
  geom_histogram(alpha = 0.8, position="identity", binwidth=1) +
  scale_fill_manual(values=c("#04477C", "#EBF5FF")) + 
  scale_x_continuous(limits=c(0,13), breaks=c(0, 3, 6, 9, 12)) + 
  scale_y_continuous(limits=c(0,1300), breaks=c(0, 300, 600, 900, 1200)) +
  xlab("Minset Size (MIN3)") + 
  theme(axis.title.x = element_text(size=15, face="bold", colour="#8C8C8C")) + 
  theme(legend.position = "top") + 
  theme(legend.title = element_blank()) + 
  theme(legend.text = element_text(size=12)) + 
  theme(plot.margin=unit(c(0.1, 0.1, 0.2, -0.1), "cm")) +
  theme(axis.title.y = element_blank()) + 
  theme(axis.text.y = element_text(angle=90, hjust=0.5))+ 
  theme(axis.text = element_text(size=13))

min4_threshed_plot = ggplot(min4_threshed_combined, aes(Minset_size, fill = multiplicity)) + 
  geom_histogram(alpha = 0.8, position="identity", binwidth=1) + 
  scale_fill_manual(values=c("#04477C", "#EBF5FF")) + 
  #scale_x_continuous(limits=c(0,13), breaks=c(0, 3, 6, 9, 12)) + 
  scale_y_continuous(limits=c(0,1400), breaks=c(0, 300, 600, 900, 1200)) +
  xlab("Minset Size (MIN4)") + 
  theme(axis.title.x = element_text(size=15, face="bold", colour="#8C8C8C")) + 
  theme(legend.position = c(0.5, 0.95), legend.direction="horizontal") + 
  theme(legend.background=element_rect(fill="transparent")) + 
  theme(legend.title = element_blank()) + 
  theme(legend.text = element_text(size=11, face="bold")) + 
  theme(plot.margin=unit(c(0.1, 0.1, 0.2, -0.1), "cm")) +
  theme(axis.title.y = element_blank()) + 
  theme(axis.text.y = element_text(angle=90, hjust=0.5))+ 
  theme(axis.text = element_text(size=13))

#pdf("min50_min14_overlay.pdf", width=15, height=5)  
#multiplot(min1_threshed_plot, min2_threshed_plot, min3_threshed_plot, min4_threshed_plot, cols=4)

pdf("min50_min14_overlay.pdf", width=8, height=6)  
multiplot(min1_threshed_plot, min4_threshed_plot, cols=2)

dev.off()

