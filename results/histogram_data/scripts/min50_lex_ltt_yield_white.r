#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')


#read the data
yield_data = read.table(file = "../sec4-1/min50_lex_ltt_yield.txt",
                        col.name = c("EXP", "Number", "Methods")) 
yield_data$Methods = factor(yield_data$Methods, 
                             levels=c("Have_minset", "Do_not_have_minset"))
yield = ggplot(yield_data, aes(x=EXP, y=Number, fill=Methods)) + 
	geom_bar(stat="identity", width=0.25) + 
  ylab("Count") + 
  xlab("Lexicon") + 
  scale_fill_manual(values=c("#79AEFF", "#04477C")) +
  scale_y_continuous(limits=c(0,11000), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme_bw() +
  theme(plot.margin=unit(c(0.1,0.1,0.1,0.3), "cm")) +
  theme(panel.border = element_rect(colour = "black")) +
  theme(axis.title.y=element_text(size=15, vjust=0.3)) +
  theme(axis.title=element_text(size=15, face="bold")) + 
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=14, face="bold")) +
  theme(legend.position=c(0.5, 0.95), legend.direction="vertical") +
  theme(legend.text=element_text(size=12, face="bold")) + 
  theme(legend.title=element_blank()) + 
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Threshable (91.62%)", x=1.25, y=4581, angle=90, vjust=0.4, size = 6) + 
  annotate("segment", x=1.15, xend=1.35, y=9162, yend=9162, size=0.5) + 
  annotate("segment", x=1.15, xend=1.35, y=0, yend=0, size=0.5) +
  annotate("segment", x=1.25, xend=1.25, y=2000, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.25, xend=1.25, y=7200, yend=9062, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Non-Threshable (99.13%)", x=2.25, y=5043.5, angle=90, vjust=0.4, size = 6) + 
  annotate("segment", x=2.15, xend=2.35, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=2.15, xend=2.35, y=88, yend=88, size=0.5) +
  annotate("segment", x=2.25, xend=2.25, y=1800, yend=188, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.25, xend=2.25, y=8300, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) 
	
duplicates_data = read.table(file = "../sec4-1/min50_lex_ltt_duplicates.txt",
                             col.name = c("EXP", "Number", "Methods"))
duplicates_data$Methods = factor(duplicates_data$Methods, 
                            levels=c("Have_duplicates", "Do_not_have_duplicates"))
duplicates = ggplot() + 
	geom_bar(data=duplicates_data, aes(x=EXP, y=Number, fill=Methods), 
           stat="identity", width=0.25) +  
  scale_fill_manual(values=c("#79AEFF", "#04477C")) +
  xlab("Lexicon") + 
  scale_y_continuous(limits=c(0,11000), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme_bw() +
  theme(plot.margin=unit(c(0.1,0.1,0.1,0.3), "cm")) + 
  theme(panel.border = element_rect(colour = "black")) +
  theme(axis.title.y=element_blank()) + 
  theme(axis.title=element_text(size=15, face="bold")) + 
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=14, face="bold")) +
  theme(legend.position=c(0.5, 0.95), legend.direction="vertical") +
  theme(legend.text=element_text(size=12, face="bold")) +
  theme(legend.title=element_blank()) +
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Have Minset (91.62%)", x=1.25, y=4581, angle=90, vjust=0.4, size = 6) + 
  annotate("segment", x=1.15, xend=1.35, y=9162, yend=9162, size=0.5) + 
  annotate("segment", x=1.15, xend=1.35, y=0, yend=0, size=0.5) +
  annotate("segment", x=1.25, xend=1.25, y=1900, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.25, xend=1.25, y=7200, yend=9062, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Do Not Have Minset (99.13%)", x=2.25, y=5043.5, angle=90, vjust=0.4, size = 6) + 
  annotate("segment", x=2.15, xend=2.35, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=2.15, xend=2.35, y=87, yend=87, size=0.5) +
  annotate("segment", x=2.25, xend=2.25, y=1500, yend=187, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.25, xend=2.25, y=8600, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) 

#png(filename="10000_random_yield.png", type="cairo",units="in", width=9, height=7, res=100)
pdf("min50_lex_ltt_yield.pdf", width=8, height=6)
#print(yield)
multiplot(yield, duplicates, cols=2)

dev.off()