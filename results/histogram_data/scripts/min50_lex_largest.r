#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')

#read the data
lex1 = read.table(file = "../sec4-1/min50_lex_largest.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))

# lex1 = read.table(file = "lex1_largest.sql", 
#                   col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
lex1 = subset(lex1, Method_size>0)
#clean the data whose minset size is 0
lex1_threshed = subset(lex1,Minset_size>0)

lex1.size.max = max(lex1$Method_size)
lex1.size.min = min(lex1$Method_size)
lex1.size.mean = round(mean(lex1$Method_size),1)
lex1.size.median = median(lex1$Method_size)
lex1.size.mode = names(sort(-table(lex1$Method_size)))[1]

lex1_threshed.minsize.max = max(lex1_threshed$Minset_size)
lex1_threshed.minsize.min = min(lex1_threshed$Minset_size)
lex1_threshed.minsize.mean = round(mean(lex1_threshed$Minset_size),1)
lex1_threshed.minsize.median = median(lex1_threshed$Minset_size)
lex1_threshed.minsize.mode = names(sort(-table(lex1_threshed$Minset_size)))[1]

lex1_threshed.ratio.max = round(max(lex1_threshed$Minset_relative_size),3)
lex1_threshed.ratio.min = round(min(lex1_threshed$Minset_relative_size),4)
#lex1_threshed.ratio.min = "1.2e-4"
lex1_threshed.ratio.mean = round(mean(lex1_threshed$Minset_relative_size),3)
lex1_threshed.ratio.median = round(median(lex1_threshed$Minset_relative_size), 3)
#lex1_threshed.ratio.mode = names(sort(-table(lex1_threshed$Minset_relative_size)))[1]
lex1_threshed.ratio.mode = names(sort(-table(round(lex1_threshed$Minset_relative_size, 4))))[1]
#lex1_threshed.ratio.mode = "2.4e-4"


#histogram of lex1
lex1.size = ggplot(lex1, aes(x = Method_size)) + 
			geom_histogram(binwidth=10, alpha=1, position="identity") + 
			scale_x_continuous(limits=c(0,8100), breaks=c(0, 2000, 4000, 6000, 8000)) + 
			scale_y_continuous(limits=c(0,31), breaks=c(0, 10, 20, 30)) +  
			theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
			xlab("Method Size (Threshable)") + 
			theme(axis.title.x = element_text(size=15, face="bold",vjust=0.2, colour="#8C8C8C")) +
			ylab("Count") + 
			theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) + 
			theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
			theme(axis.text = element_text(size=13, face="bold")) + 
			annotate("segment", x=5000, xend=8000, y=30, yend=30, size=0.5, color="#404040") +
			annotate("segment", x=5000, xend=8000, y=20, yend=20, size=0.5, color="#404040") +
			annotate("segment", x=5000, xend=5000, y=20, yend=30, size=0.5, color="#404040") +
			annotate("segment", x=8000, xend=8000, y=20, yend=30, size=0.5, color="#404040") +
			annotate("text", x=5200, y=28.8, label="Min", size=5, hjust=0, color="#404040") + 
			annotate("text", x=5200, y=26.9, label="Mean", size=5, hjust=0, color="#404040") +
			annotate("text", x=5200, y=25.0, label="Median", size=5, hjust=0, color="#404040") +
			annotate("text", x=5200, y=23.1, label="Max", size=5, hjust=0, color="#404040") +
			annotate("text", x=5200, y=21.2, label="Mode", size=5, hjust=0, color="#404040") +
			annotate("text", x=7800, y=28.6, label=lex1.size.min, size=5, hjust=1, color="#404040") +
			annotate("text", x=7800, y=26.8, label=lex1.size.mean, size=5, hjust=1, color="#404040") +
			annotate("text", x=7800, y=25.0, label=lex1.size.median, size=5, hjust=1, color="#404040") +
			annotate("text", x=7800, y=23.2, label=lex1.size.max, size=5, hjust=1, color="#404040") +
			annotate("text", x=7800, y=21.4, label=lex1.size.mode, size=5, hjust=1, color="#404040") +
			annotate("text", x=5000, y=31, label="LEX", size=5, hjust=0, vjust=0.5, color="#404040")  
			
lex1.minsize = ggplot(lex1_threshed, aes(x = Minset_size)) + 
			geom_histogram(binwidth=1, alpha=0.7, position="identity") +
			scale_x_continuous(limits=c(0,8.1), breaks=c(0, 2, 4, 6, 8)) + 
			scale_y_continuous(limits=c(0,930), breaks=c(0, 300, 600, 900)) +			
			theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
			xlab("Minset Size") + 
			theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
			theme(axis.title.y = element_blank()) + 
			theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
			theme(axis.text = element_text(size=13, face="bold")) +
			annotate("segment", x=5, xend=8, y=900, yend=900, size=0.5, color="#404040") +
			annotate("segment", x=5, xend=8, y=600, yend=600, size=0.5, color="#404040") +
			annotate("segment", x=5, xend=5, y=600, yend=900, size=0.5, color="#404040") +
			annotate("segment", x=8, xend=8, y=600, yend=900, size=0.5, color="#404040") +
			annotate("text", x=5.2, y=864, label="Min", size=5, hjust=0, color="#404040") +
			annotate("text", x=5.2, y=807, label="Mean", size=5, hjust=0, color="#404040") + 
			annotate("text", x=5.2, y=750, label="Median", size=5, hjust=0, color="#404040") + 
			annotate("text", x=5.2, y=696, label="Max", size=5, hjust=0, color="#404040") + 
			annotate("text", x=5.2, y=642, label="Mode", size=5, hjust=0, color="#404040") + 
			annotate("text", x=7.8, y=864, label=lex1_threshed.minsize.min, size=5, hjust=1, color="#404040") +
			annotate("text", x=7.8, y=807, label=lex1_threshed.minsize.mean, size=5, hjust=1, color="#404040") +
			annotate("text", x=7.8, y=750, label=lex1_threshed.minsize.median, size=5, hjust=1, color="#404040") +
			annotate("text", x=7.8, y=696, label=lex1_threshed.minsize.max, size=5, hjust=1, color="#404040") + 
			annotate("text", x=7.8, y=642, label=lex1_threshed.minsize.mode, size=5, hjust=1, color="#404040") +
			annotate("text", x=5, y=930, label="LEX", size=5, hjust=0, vjust=0.5, color="#404040") 
			
lex1.ratio = ggplot(lex1_threshed, aes(x = Minset_relative_size)) + 
			geom_histogram(binwidth=0.001, alpha=0.7, position="identity") + 
			scale_x_continuous(limits=c(0,0.162), breaks=c(0, 0.04, 0.08, 0.12, 0.16)) + 
			scale_y_continuous(limits=c(0,310), breaks=c(0, 100, 200, 300)) +
			theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
			xlab("Minset Ratio") + 
			theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) + 
			theme(axis.title.y = element_blank()) + 
			theme(axis.text.y = element_text(angle=90, hjust=0.5)) +
			theme(axis.text = element_text(size=13, face="bold")) +
			annotate("segment", x=0.1, xend=0.16, y=300, yend=300, size=0.5, color="#404040") +
			annotate("segment", x=0.1, xend=0.16, y=200, yend=200, size=0.5, color="#404040") +
			annotate("segment", x=0.1, xend=0.1, y=200, yend=300, size=0.5, color="#404040") +
			annotate("segment", x=0.16, xend=0.16, y=200, yend=300, size=0.5, color="#404040") +
			annotate("text", x=0.104, y=288, label="Min", size=5, hjust=0, color="#404040") +
			annotate("text", x=0.104, y=269, label="Mean", size=5, hjust=0, color="#404040") + 
			annotate("text", x=0.104, y=250, label="Median", size=5, hjust=0, color="#404040") + 
			annotate("text", x=0.104, y=231, label="Max", size=5, hjust=0, color="#404040") + 
			annotate("text", x=0.104, y=212, label="Mode", size=5, hjust=0, color="#404040") + 
			annotate("text", x=0.156, y=288, label=lex1_threshed.ratio.min, size=5, hjust=1, color="#404040") +
			annotate("text", x=0.156, y=269, label=lex1_threshed.ratio.mean, size=5, hjust=1, color="#404040") +
			annotate("text", x=0.156, y=250, label=lex1_threshed.ratio.median, size=5, hjust=1, color="#404040") +
			annotate("text", x=0.156, y=231, label=lex1_threshed.ratio.max, size=5, hjust=1, color="#404040") + 
			annotate("text", x=0.156, y=212, label=lex1_threshed.ratio.mode, size=5, hjust=1, color="#404040") +
			annotate("text", x=0.1, y=310, label="LEX", size=5, hjust=0, vjust=0.5, color="#404040")
			
#output the plots
#png(filename="lex1_1000_largest.png", type="cairo",units="in", width=14, height=3.5, res=100)
pdf("min50_lex_largest.pdf", width=14, height=4.5)	
#3*1 chart
multiplot(lex1.size, lex1.minsize, lex1.ratio, cols=3)
#print(lex1.minsize)

dev.off()

