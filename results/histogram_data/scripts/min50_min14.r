#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
library(plyr)
source('multiplot.r')

#read the data
min1 = read.table(file = "../sec4-2/min50_min1.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min2 = read.table(file = "../sec4-2/min50_min2.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min3 = read.table(file = "../sec4-2/min50_min3.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
min4 = read.table(file = "../sec4-2/min50_min4.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))

#clean the data whose method size is 0
min1$EXP = "MIN1"
min2$EXP = "MIN2"
min3$EXP = "MIN3"
min4$EXP = "MIN4"
min_combined = rbind(min1, min2, min3, min4)
#clean the data whose method size is 0
min_combined = subset(min_combined, Method_size>0)
#clean the data whose minset size is 0
min_threshed_combined = subset(min_combined, Minset_size>0)

# Identifying Records in Data Frame A 
#That Are Not Contained In Data Frame B 
compare <- function(df1,df2,...){
  df1.p <- do.call("paste", df1)
  df2.p <- do.call("paste", df2)
  df1[! df1.p %in% df2.p, ]
}

######Draw the figures of method size

#prepare the statistical data
method_stat = ddply(min_combined,~EXP,summarise,
                    mean=mean(Method_size),
                    sd=sd(Method_size),
                    lower_inner=min(Method_size),
                    upper_inner=max(Method_size),
                    lower_outer= min(Method_size),   
                    upper_outer= max(Method_size), 
                    median=median(Method_size),
                    Q1=summary(Method_size)['1st Qu.'],
                    Q3=summary(Method_size)['3rd Qu.']
)

#prepare the outliers
method_out = data.frame() #initialising storage for outliers
method_extre_out = data.frame() #initialising storage for extreme outliers
for(exp in 1:length((levels(factor(min_combined$EXP))))){
  exp_cate = paste("MIN", exp, sep="")
  # calcute the inner fence
  bps = boxplot.stats(min_combined$Method_size[min_combined$EXP == exp_cate], coef=1.5) 
  method_stat[method_stat$EXP == exp_cate,]$lower_inner = bps$stats[1] #lower inner fence
  method_stat[method_stat$EXP == exp_cate,]$upper_inner = bps$stats[5] #upper inner fence
  if(length(bps$out) > 0){ #adding outliers
    for(y in 1:length(bps$out)){
      pt =data.frame(x=exp_cate,y=bps$out[y]) 
      method_out=rbind(method_out,pt) 
    }
  }
  # calcute the outer fence
  bps = boxplot.stats(min_combined$Method_size[min_combined$EXP == exp_cate], coef=3) 
  method_stat[method_stat$EXP == exp_cate,]$lower_outer = bps$stats[1] #lower outer fence
  method_stat[method_stat$EXP == exp_cate,]$upper_outer = bps$stats[5] #upper outer fence
  if(length(bps$out) > 0){ #adding outliers
    for(y in 1:length(bps$out)){
      pt =data.frame(x=exp_cate,y=bps$out[y]) 
      method_extre_out=rbind(method_extre_out,pt) 
    }
  }
}

# get the mild outlier
method_mild_out = compare(method_out, method_extre_out)

#Drawing
method_size = ggplot(method_stat, aes(x=EXP, y=mean)) + 
  geom_errorbar(aes(ymin=lower_inner,ymax=upper_inner),linetype = 1,width = 0.3) +
  geom_crossbar(aes(y=median,ymin=Q1,ymax=Q3), linetype = 1, width=0.5, fill=c("#EBF5FF", "#79AEFF","#4775D1", "#04477C")) +
  geom_errorbar(aes(ymin=lower_outer,ymax=upper_outer),linetype = 3,width = 0.3) + 
  geom_point() 

if(length(method_out)>0) {
  method_size = method_size + geom_point(data=method_out,aes(x=x,y=y),shape=4)
}

method_size = method_size + 
  ylab("Method Size") + 
  xlab("Lexicon") + 
  scale_y_continuous(limits=c(0,120), breaks=c(0, 30, 60, 90, 120)) +
  theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +
  theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
  theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) +
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=13, face="bold")) +
  theme(legend.position = "none") + 
  annotate("segment", x=0.7, xend=0.9, y=118, yend=118, size=0.5, linetype=1, color="#404040") + 
  annotate("segment", x=0.7, xend=0.9, y=113, yend=113, size=0.5, linetype=3, color="#404040") +  
  geom_point(x=0.8, y=108, size=2, shape=4, color="#404040") + 
  geom_point(x=0.8, y=103, size=2,color="#404040") + 
  annotate("text", x=1.0, y=118, label="inner fence", size=4, hjust = 0.0, color="#404040") +
  annotate("text", x=1.0, y=113, label="outer fence", size=4, hjust = 0.0, color="#404040") +
  annotate("text", x=1.0, y=108, label="mild outlier", size=4, hjust = 0.0, color="#404040") +
  annotate("text", x=1.0, y=103, label="mean", size=4, hjust = 0.0, color="#404040") 


######Draw the figures of minset size

#prepare the statistical data
minset_stat = ddply(min_threshed_combined,~EXP,summarise,
                    mean=mean(Minset_size),
                    sd=sd(Minset_size),
                    lower_inner=min(Minset_size),
                    upper_inner=max(Minset_size),
                    lower_outer= min(Minset_size),   
                    upper_outer= max(Minset_size), 
                    median=median(Minset_size),
                    Q1=summary(Minset_size)['1st Qu.'],
                    Q3=summary(Minset_size)['3rd Qu.']
)

#prepare the outliers
minset_out = data.frame() #initialising storage for outliers
minset_extre_out = data.frame() #initialising storage for extreme outliers
for(exp in 1:length((levels(factor(min_threshed_combined$EXP))))){
  exp_cate = paste("MIN", exp, sep="")
  # calcute the inner fence
  bps = boxplot.stats(min_threshed_combined$Minset_size[min_threshed_combined$EXP == exp_cate], coef=1.5) 
  minset_stat[minset_stat$EXP == exp_cate,]$lower_inner = bps$stats[1] #lower inner fence
  minset_stat[minset_stat$EXP == exp_cate,]$upper_inner = bps$stats[5] #upper inner fence
  if(length(bps$out) > 0){ #adding outliers
    for(y in 1:length(bps$out)){
      pt =data.frame(x=exp_cate,y=bps$out[y]) 
      minset_out=rbind(minset_out,pt) 
    }
  }
  # calcute the outer fence
  bps = boxplot.stats(min_threshed_combined$Minset_size[min_threshed_combined$EXP == exp_cate], coef=3) 
  minset_stat[minset_stat$EXP == exp_cate,]$lower_outer = bps$stats[1] #lower outer fence
  minset_stat[minset_stat$EXP == exp_cate,]$upper_outer = bps$stats[5] #upper outer fence
  if(length(bps$out) > 0){ #adding outliers
    for(y in 1:length(bps$out)){
      pt =data.frame(x=exp_cate,y=bps$out[y]) 
      minset_extre_out=rbind(minset_extre_out,pt) 
    }
  }
}

# get the mild outlier
method_mild_out = compare(minset_out, minset_extre_out)

#Drawing
minset_size = ggplot(minset_stat, aes(x=EXP, y=mean)) + 
  geom_errorbar(aes(ymin=lower_inner,ymax=upper_inner),linetype = 1,width = 0.3) +
  geom_crossbar(aes(y=median,ymin=Q1,ymax=Q3), linetype = 1, width=0.5, fill=c("#EBF5FF", "#79AEFF","#4775D1", "#04477C")) +
  geom_errorbar(aes(ymin=lower_outer,ymax=upper_outer),linetype = 3,width = 0.3) + 
  geom_point() 

if(length(minset_out)>0) {
  minset_size = minset_size + geom_point(data=minset_out,aes(x=x,y=y),shape=4)
}

minset_size = minset_size + 
  ylab("Method Size") + 
  xlab("Lexicon") +
  scale_y_continuous(limits=c(0,12), breaks=c(0, 3, 6, 9, 12)) +
  theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +
  theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
  theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) +
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=13, face="bold")) + 
  theme(legend.position = "none")

pdf("min50_min14.pdf", width=8, height=6)

multiplot(method_size, minset_size, cols=2)

dev.off()




# #clean the data whose method size is 0
# min1$EXP = "MIN1"
# min2$EXP = "MIN2"
# min3$EXP = "MIN3"
# min4$EXP = "MIN4"
# min_combined = rbind(min1, min2, min3, min4)
# #clean the data whose method size is 0
# min_combined = subset(min_combined, Method_size>0)
# #clean the data whose minset size is 0
# min_threshed_combined = subset(min_combined, Minset_size>0)
# 
# ######Draw the figures of method size
# 
# #prepare the statistical data
# min_sum = ddply(min_combined,~EXP,summarise,
#                      mean=mean(Method_size),
#                      sd=sd(Method_size),
#                      min=min(Method_size),
#                      max=max(Method_size),
#                      median=median(Method_size),
#                      Q1=summary(Method_size)['1st Qu.'],
#                      Q3=summary(Method_size)['3rd Qu.']
# )
# 
# #prepare the outliers
# method_size_out = data.frame() #initialising storage for outliers
# for(exp in 1:length((levels(factor(min_combined$EXP))))){
#   exp_cate = paste("MIN", exp, sep="")
#   bps = boxplot.stats(min_combined$Method_size[min_combined$EXP == exp_cate], coef=3) 
#   min_sum[min_sum$EXP == exp_cate,]$min = bps$stats[1] #lower wisker
#   min_sum[min_sum$EXP == exp_cate,]$max = bps$stats[5] #upper wisker
#   if(length(bps$out) > 0){ #adding outliers
#     for(y in 1:length(bps$out)){
#       pt =data.frame(x=exp_cate,y=bps$out[y]) 
#       method_size_out=rbind(method_size_out,pt) 
#     }
#   }
# }
# 
# #Drawing
# method_size = ggplot(min_sum, aes(x=EXP, y=mean)) + 
#       geom_errorbar(aes(ymin=min,ymax=max),linetype = 1,width = 0.2) +
#       geom_crossbar(aes(y=median,ymin=Q1,ymax=Q3), linetype = 1,fill='white') +
#       geom_errorbar(aes(ymin=mean-sd,ymax=mean+sd),linetype = 3,width = 0.25) + 
#       geom_point() 
#       if(length(method_size_out)>0)
#         method_size = method_size + geom_point(data=method_size_out,aes(x=x,y=y),shape=4)
#       method_size = method_size + 
#       theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +
#       ylab("Method Size") + 
#       xlab("Lexicon") +  
#       theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
#       theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) +
#       theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
#       theme(axis.text = element_text(size=13, face="bold")) + 
#       theme(legend.position = "none")
# 
# 
# ######Draw the figures of minset size
# 
# #prepare the statistical data
# minset_sum = ddply(min_threshed_combined,~EXP,summarise,
#                 mean=mean(Minset_size),
#                 sd=sd(Minset_size),
#                 min=min(Minset_size),
#                 max=max(Minset_size),
#                 median=median(Minset_size),
#                 Q1=summary(Minset_size)['1st Qu.'],
#                 Q3=summary(Minset_size)['3rd Qu.']
# )
# 
# #prepare the outliers
# minset_size_out = data.frame() #initialising storage for outliers
# for(exp in 1:length((levels(factor(min_threshed_combined$EXP))))){
#   exp_cate = paste("MIN", exp, sep="")
#   bps = boxplot.stats(min_threshed_combined$Minset_size[min_threshed_combined$EXP == exp_cate], coef=3) 
#   minset_sum[minset_sum$EXP == exp_cate,]$min = bps$stats[1] #lower wisker
#   minset_sum[minset_sum$EXP == exp_cate,]$max = bps$stats[5] #upper wisker
#   if(length(bps$out) > 0){ #adding outliers
#     for(y in 1:length(bps$out)){
#       pt =data.frame(x=exp_cate,y=bps$out[y]) 
#       minset_size_out=rbind(minset_size_out,pt) 
#     }
#   }
# }
# 
# #Drawing
# minset_size = ggplot(minset_sum, aes(x=EXP, y=mean)) + 
#   geom_errorbar(aes(ymin=min,ymax=max),linetype = 1,width = 0.2) +
#   geom_crossbar(aes(y=median,ymin=Q1,ymax=Q3), linetype = 1,fill='white') +
#   geom_errorbar(aes(ymin=mean-sd,ymax=mean+sd),linetype = 3,width = 0.25) + 
#   geom_point() 
# if(length(minset_size_out)>0)
#   minset_size = minset_size + geom_point(data=minset_size_out,aes(x=x,y=y),shape=4)
# minset_size = minset_size + 
#   theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +
#   ylab("Method Size") + 
#   xlab("Lexicon") +  
#   theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
#   theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) +
#   theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
#   theme(axis.text = element_text(size=13, face="bold")) + 
#   theme(legend.position = "none")


# method_size = ggplot(min_combined, aes(x=EXP, y=Method_size, fill=EXP)) +
#   stat_boxplot(geom ='errorbar') + 
#   #geom_boxplot(outlier.shape = NA) +
#   geom_boxplot() 

#    
  #scale_y_continuous(limits=c(0,120), breaks=c(0, 30, 60, 90, 120)) +
  #scale_y_continuous(limits=c(0,40), breaks=c(0, 10, 20, 30, 40)) + 
  #scale_fill_manual(values=c("#EBF5FF", "#79AEFF","#4775D1", "#04477C"))  + 
# method_size= method_size +
#   theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +
#   ylab("Method Size") + 
#   xlab("Lexicon") +  
#   theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
#   theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) +
#   theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
#   theme(axis.text = element_text(size=13, face="bold")) + 
#   theme(legend.position = "none")
 
# minset_size = ggplot(min_threshed_combined, aes(x=EXP, y=Minset_size,fill=EXP)) +
#   stat_boxplot(geom ='errorbar', width=0.3) + 
#   geom_boxplot(outlier.shape = NA) + 
#   #scale_y_continuous(limits=c(0,12), breaks=c(0, 3, 6, 9, 12)) +
#   scale_y_continuous(limits=c(0,8), breaks=c(0, 2, 4, 6, 8)) + 
#   scale_fill_manual(values=c("#EBF5FF", "#79AEFF","#4775D1", "#04477C"))  + 
#   theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +
#   ylab("Minset Size") + 
#   xlab("Lexicon") +
#   theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
#   theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) +
#   theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
#   theme(axis.text = element_text(size=13, face="bold")) +
#   theme(legend.position = "none")



