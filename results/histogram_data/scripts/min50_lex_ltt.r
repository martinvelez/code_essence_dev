#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')

#read the data
lex1 = read.table(file = "lex1.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
lex1 = subset(lex1, Method_size>0)
#clean the data whose minset size is 0
lex1_threshed = subset(lex1,Minset_size>0)

ltt = read.table(file = "ltt.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
ltt = subset(ltt, Method_size>0)
#clean the data whose minset size is 0
ltt_threshed = subset(ltt,Minset_size>0)

lex1.size.max = max(lex1$Method_size)
lex1.size.min = min(lex1$Method_size)
lex1.size.mean = round(mean(lex1$Method_size),1)
lex1.size.median = median(lex1$Method_size)
lex1.size.mode = names(sort(-table(lex1$Method_size)))[1]

lex1_threshed.minsize.max = max(lex1_threshed$Minset_size)
lex1_threshed.minsize.min = min(lex1_threshed$Minset_size)
lex1_threshed.minsize.mean = round(mean(lex1_threshed$Minset_size),1)
lex1_threshed.minsize.median = median(lex1_threshed$Minset_size)
lex1_threshed.minsize.mode = names(sort(-table(lex1_threshed$Minset_size)))[1]

lex1_threshed.ratio.max = format(max(lex1_threshed$Minset_relative_size), nsmall=3)
lex1_threshed.ratio.min = round(min(lex1_threshed$Minset_relative_size),3)
lex1_threshed.ratio.mean = round(mean(lex1_threshed$Minset_relative_size),3)
lex1_threshed.ratio.median = round(median(lex1_threshed$Minset_relative_size), 3)
#lex1.ratio.mode = names(sort(-table(lex1$Minset_relative_size)))[1]
lex1_threshed.ratio.mode = 0.039

ltt.size.max = max(ltt$Method_size)
ltt.size.min = min(ltt$Method_size)
ltt.size.mean = round(mean(ltt$Method_size),1)
ltt.size.median = median(ltt$Method_size)
ltt.size.mode = names(sort(-table(ltt$Method_size)))[1]

ltt_threshed.minsize.max = max(ltt_threshed$Minset_size)
ltt_threshed.minsize.min = min(ltt_threshed$Minset_size)
ltt_threshed.minsize.mean = round(mean(ltt_threshed$Minset_size),1)
ltt_threshed.minsize.median = median(ltt_threshed$Minset_size)
ltt_threshed.minsize.mode = names(sort(-table(ltt_threshed$Minset_size)))[1]

ltt_threshed.ratio.max = round(max(ltt_threshed$Minset_relative_size),3)
ltt_threshed.ratio.min = format(round(min(ltt_threshed$Minset_relative_size),3), nsmall=3)
ltt_threshed.ratio.mean = round(mean(ltt_threshed$Minset_relative_size),3)
ltt_threshed.ratio.median = round(median(ltt_threshed$Minset_relative_size), 3)
ltt_threshed.ratio.mode = names(sort(-table(round(ltt_threshed$Minset_relative_size,3))))[1]


#histogram of lex1
lex1.size = ggplot(lex1, aes(x = Method_size)) + 
	geom_histogram(binwidth=10, alpha=0.7, position="identity") + 
	scale_x_continuous(limits=c(0,1210), breaks=c(0, 400, 800, 1200)) + 
	scale_y_continuous(limits=c(0,3100), breaks=c(0, 1000, 2000, 3000)) + 
	theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
	theme(axis.title.x=element_blank()) + 
	ylab("Count") + 
	theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) + 
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=780, xend=1200, y=3000, yend=3000, size=0.5, color="#404040") +
	annotate("segment", x=780, xend=1200, y=1900, yend=1900, size=0.5, color="#404040") +
	annotate("segment", x=780, xend=780, y=1900, yend=3000, size=0.5, color="#404040") +
	annotate("segment", x=1200, xend=1200, y=1900, yend=3000, size=0.5, color="#404040") +
	annotate("text", x=800, y=2850, label="Min", size=5, hjust=0, color="#404040") + 
	annotate("text", x=800, y=2650, label="Mean", size=5, hjust=0, color="#404040") +
	annotate("text", x=800, y=2450, label="Median", size=5, hjust=0, color="#404040") +
	annotate("text", x=800, y=2250, label="Max", size=5, hjust=0, color="#404040") +
	annotate("text", x=800, y=2050, label="Mode", size=5, hjust=0, color="#404040") +
	annotate("text", x=1190, y=2850, label=lex1.size.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=1190, y=2650, label=lex1.size.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=1190, y=2450, label=lex1.size.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=1190, y=2250, label=lex1.size.max, size=5, hjust=1, color="#404040") + 
	annotate("text", x=1190, y=2050, label=lex1.size.mode, size=5, hjust=1, color="#404040") + 
	annotate("text", x=800, y=3100, label="LEX1", size=5, hjust=0, vjust=0.2, color="#404040")  
			
lex1.minsize = ggplot(lex1_threshed, aes(x = Minset_size)) + 
	geom_histogram(binwidth=1, alpha=0.7, position="identity") +
	scale_x_continuous(limits=c(0,14), breaks=c(0,4,8,12)) + 
	scale_y_continuous(limits=c(0,6200), breaks=c(0, 2000, 4000, 6000)) +			
	theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
	theme(axis.title=element_blank()) + 
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=9.8, xend=14, y=6000, yend=6000, size=0.5, color="#404040") +
	annotate("segment", x=9.8, xend=14, y=3800, yend=3800, size=0.5, color="#404040") +
	annotate("segment", x=9.8, xend=9.8, y=3800, yend=6000, size=0.5, color="#404040") +
	annotate("segment", x=14, xend=14, y=3800, yend=6000, size=0.5, color="#404040") +
	annotate("text", x=10, y=5700, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=10, y=5300, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=10, y=4900, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=10, y=4500, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=10, y=4100, label="Mode", size=5, hjust=0, color="#404040") + 
	annotate("text", x=13.8, y=5700, label=lex1_threshed.minsize.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=13.8, y=5300, label=lex1_threshed.minsize.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=13.8, y=4900, label=lex1_threshed.minsize.median, size=5, hjust=1, color="#404040")+
	annotate("text", x=13.8, y=4500, label=lex1_threshed.minsize.max, size=5, hjust=1, color="#404040") +
	annotate("text", x=13.8, y=4100, label=lex1_threshed.minsize.mode, size=5, hjust=1, color="#404040") +
	annotate("text", x=10, y=6200, label="LEX1", size=5, hjust=0, vjust=0.2, color="#404040") 
			
lex1.ratio = ggplot(lex1_threshed, aes(x = Minset_relative_size)) + 
	geom_histogram(binwidth=0.005, alpha=0.7, position="identity") + 
	scale_x_continuous(limits=c(0,0.4), breaks=c(0, 0.1, 0.2, 0.3, 0.4)) + 
	scale_y_continuous(limits=c(0,1250), breaks=c(0, 400, 800, 1200)) +
	theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
	theme(axis.title=element_blank()) + 
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=0.245, xend=0.40, y=1200, yend=1200, size=0.5, color="#404040") +
	annotate("segment", x=0.245, xend=0.40, y=760, yend=760, size=0.5, color="#404040") +
	annotate("segment", x=0.245, xend=0.245, y=760, yend=1200, size=0.5, color="#404040") +
	annotate("segment", x=0.40, xend=0.40, y=760, yend=1200, size=0.5, color="#404040") +
	annotate("text", x=0.25, y=1140, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=0.25, y=1060, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.25, y=980, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.25, y=900, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.25, y=820, label="Mode", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.39, y=1140, label=lex1_threshed.ratio.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=1060, label=lex1_threshed.ratio.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=980, label=lex1_threshed.ratio.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=900, label=lex1_threshed.ratio.max, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=820, label=lex1_threshed.ratio.mode, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.25, y=1250, label="LEX1", size=5, hjust=0, vjust=0.2, color="#404040")

#histogram of ltt
ltt.size = ggplot(ltt, aes(x = Method_size)) + 
	geom_histogram(binwidth=1, alpha=0.7, position="identity") +
	scale_x_continuous(limits=c(0,61), breaks=c(0, 20, 40, 60)) +
	scale_y_continuous(limits=c(0,930), breaks=c(0,300,600,900)) + 
	xlab("Method Size (Threshed)") + 
	theme(axis.title.x = element_text(size=15, face="bold", vjust=0.7, colour="#8C8C8C")) +
	ylab("Count") + 
  theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) + 		
	theme(plot.margin=unit(c(-0.1,0.1,0.1,0), "cm")) + 
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) + 
	annotate("segment", x=39, xend=60, y=900, yend=900, size=0.5, color="#404040") +
	annotate("segment", x=39, xend=60, y=575, yend=575, size=0.5, color="#404040") +
	annotate("segment", x=39, xend=39, y=575, yend=900, size=0.5, color="#404040") +
	annotate("segment", x=60, xend=60, y=575, yend=900, size=0.5, color="#404040") +
	annotate("text", x=40, y=860, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=40, y=800, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=40, y=740, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=40, y=680, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=40, y=620, label="Mode", size=5, hjust=0, color="#404040") + 
	annotate("text", x=59, y=860, label=ltt.size.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=59, y=800, label=ltt.size.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=59, y=740, label=ltt.size.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=59, y=680, label=ltt.size.max, size=5, hjust=1, color="#404040") +
	annotate("text", x=59, y=620, label=ltt.size.mode, size=5, hjust=1, color="#404040") +
	annotate("text", x=40, y=930, label="LTT", size=5, hjust=0, vjust=0.2, color="#404040")
			
ltt.minsize = ggplot(ltt_threshed, aes(x = Minset_size)) + 
	geom_histogram(binwidth=1, alpha=0.7, position="identity") + 
	scale_x_continuous(limits=c(0,14), breaks=c(0,4,8,12)) + 
	scale_y_continuous(limits=c(0,31), breaks=c(0,10,20,30)) + 
	theme(axis.title.y = element_blank()) +
	xlab("Minset Size") + 
	theme(axis.title.x = element_text(size=15, face="bold", vjust=0.7, colour="#8C8C8C")) +
	theme(plot.margin = unit(c(-0.1,0.1,0.1,0), "cm")) +
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=9.8, xend=14, y=19.0, yend=19.0, size=0.5, color="#404040") +
	annotate("segment", x=9.8, xend=14, y=30, yend=30, size=0.5, color="#404040") +
	annotate("segment", x=9.8, xend=9.8, y=19, yend=30, size=0.5, color="#404040") +
	annotate("segment", x=14, xend=14, y=19, yend=30, size=0.5, color="#404040") +
	annotate("text", x=10, y=28.6, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=10, y=26.6, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=10, y=24.6, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=10, y=22.6, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=10, y=20.6, label="Mode", size=5, hjust=0, color="#404040") + 
	annotate("text", x=13.8, y=28.6, label=ltt_threshed.minsize.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=13.8, y=26.6, label=ltt_threshed.minsize.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=13.8, y=24.6, label=ltt_threshed.minsize.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=13.8, y=22.6, label=ltt_threshed.minsize.max, size=5, hjust=1, color="#404040") + 
	annotate("text", x=13.8, y=20.6, label=ltt_threshed.minsize.mode, size=5, hjust=1, color="#404040") + 
	annotate("text", x=10, y=31, label="LTT", size=5, hjust=0, vjust=0.2, color="#404040")

			
ltt.ratio = ggplot(ltt_threshed, aes(x = Minset_relative_size)) + 
	geom_histogram(binwidth=0.005, alpha=0.7, position="identity") + 
	scale_x_continuous(limits=c(0,0.4), breaks=c(0,0.1, 0.2, 0.3, 0.4)) + 
	scale_y_continuous(limits=c(0,9.3), breaks=c(0,3,6,9)) + 
	theme(plot.margin=unit(c(-0.1,0.1,0.1,0), "cm")) + 
	theme(axis.title.y = element_blank()) +
	xlab("Minset Ratio") + 
	theme(axis.title.x = element_text(size=15, face="bold", vjust=0.7, colour="#8C8C8C")) +
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=0.245, xend=0.40, y=5.75, yend=5.75, size=0.5, color="#404040") +
	annotate("segment", x=0.245, xend=0.40, y=9, yend=9, size=0.5, color="#404040") +
	annotate("segment", x=0.245, xend=0.245, y=5.75, yend=9, size=0.5, color="#404040") +
	annotate("segment", x=0.40, xend=0.40, y=5.75, yend=9, size=0.5, color="#404040") +
	annotate("text", x=0.25, y=8.6, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=0.25, y=8.0, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.25, y=7.4, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.25, y=6.8, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.25, y=6.2, label="Mode", size=5, hjust=0, color="#404040") +
	annotate("text", x=0.39, y=8.6, label=ltt_threshed.ratio.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=8.0, label=ltt_threshed.ratio.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=7.4, label=ltt_threshed.ratio.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=6.8, label=ltt_threshed.ratio.max, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=6.2, label=ltt_threshed.ratio.mode, size=5, hjust=1, color="#404040") + 
	annotate("text", x=0.25, y=9.3, label="LTT", size=5, hjust=0, vjust=0.2, color="#404040")

			
#output the plots
#png(filename="lex1_ltt_10000.png", type="cairo",units="in", width=14, height=7, res=100)		
pdf("lex1_ltt_10000.pdf", width=14, height=8)
#3*2 chart
multiplot(lex1.size, ltt.size, lex1.minsize, ltt.minsize, lex1.ratio, ltt.ratio, cols=3)
#print(lex1.minsize)

dev.off()

