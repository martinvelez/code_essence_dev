#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')


#read the data
yield_data = read.table(file = "../sec4-3/min50_min14_m_yield.txt",
                        col.name = c("EXP", "Number", "Methods"))
yield_data$Methods = factor(yield_data$Methods, 
                             levels=c("Have_more_than_10_supersets", "Have_10_or_fewer_supersets", "Have_minsets"))
yield = ggplot(yield_data, aes(x=EXP, y=Number, fill=Methods)) + 
	geom_bar(stat="identity", width=0.4) + 
  ylab("Count") + 
  xlab("Lexicon") + 
  scale_fill_manual(values=c("#EBF5FF", "#79AEFF", "#04477C"))  + 
  scale_y_continuous(limits=c(0,11500), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme(plot.margin=unit(c(0.1,0,0.1,0.2), "cm")) + 
  theme(legend.position=c(0.5, 0.925), legend.direction="vertical") +
  theme(axis.title.y=element_text(vjust=0.3)) +
  theme(axis.title=element_text(size=15, face="bold", colour="#8C8C8C")) + 
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=12, face="bold")) +
  theme(legend.text=element_text(size=11, face="bold")) + 
  theme(legend.title=element_blank()) +
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Non-Threshable (67.36%)", x=1.3, y=6625.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=1.2, xend=1.4, y=9990, yend=9990, size=0.5) + 
  annotate("segment", x=1.2, xend=1.4, y=3262, yend=3262, size=0.5) +
  annotate("segment", x=1.3, xend=1.3, y=4200, yend=3362, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.3, xend=1.3, y=9000, yend=9890, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Non-Threshable (64.18%)", x=2.3, y=6789.5, angle=90, vjust=0.5,size=4.2) + 
  annotate("segment", x=2.2, xend=2.4, y=9998, yend=9998, size=0.5) + 
  annotate("segment", x=2.2, xend=2.4, y=3582, yend=3582, size=0.5) +
  annotate("segment", x=2.3, xend=2.3, y=4400, yend=3682, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.3, xend=2.3, y=9200, yend=9898, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Threshable (49.38%)", x=3.3, y=2469, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=3.2, xend=3.4, y=4938, yend=4938, size=0.5) + 
  annotate("segment", x=3.2, xend=3.4, y=0, yend=0, size=0.5) +
  annotate("segment", x=3.3, xend=3.3, y=600, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=3.3, xend=3.3, y=4350, yend=4838, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Threshable (53.63%)", x=4.3, y=2681.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=4.2, xend=4.4, y=5363, yend=5363, size=0.5) + 
  annotate("segment", x=4.2, xend=4.4, y=0, yend=0, size=0.5) +
  annotate("segment", x=4.3, xend=4.3, y=800, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=4.3, xend=4.3, y=4600, yend=5263, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) 
	
duplicates_data = read.table(file = "../sec4-3/min50_min14_m_duplicates.txt",
                             col.name = c("EXP", "Number", "Methods"))

duplicates = ggplot() + 
	geom_bar(data=duplicates_data, aes(x=EXP, y=Number, fill=Methods), 
           stat="identity", width=0.4) +  
  scale_fill_manual(values=c("#04477C", "#EBF5FF")) +
  xlab("Lexicon") + 
  scale_y_continuous(limits=c(0,11500), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme(plot.margin=unit(c(0.1,0.1,0.1,0.2), "cm")) + 
  theme(legend.position=c(0.5, 0.945), legend.direction="vertical") +
  theme(axis.title.y=element_blank()) + 
  theme(axis.title=element_text(size=15, face="bold", colour="#8C8C8C")) + 
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=12, face="bold")) +
  theme(legend.text=element_text(size=11, face="bold")) +
  theme(legend.title=element_blank()) +
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Do Not Have Minset (81.22%)", x=1.3, y=5933, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=1.2, xend=1.4, y=9990, yend=9990, size=0.5) + 
  annotate("segment", x=1.2, xend=1.4, y=1877, yend=1877, size=0.5) +
  annotate("segment", x=1.3, xend=1.3, y=3200, yend=1997, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.3, xend=1.3, y=8600, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Do Not Have Minset (78.98%)", x=2.3, y=6050, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=2.2, xend=2.4, y=9998, yend=9998, size=0.5) + 
  annotate("segment", x=2.2, xend=2.4, y=2103, yend=2103, size=0.5) +
  annotate("segment", x=2.3, xend=2.3, y=3300, yend=2203, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.3, xend=2.3, y=8700, yend=9898, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Do Not Have Minset (69.53%)", x=3.3, y=6523.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=3.2, xend=3.4, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=3.2, xend=3.4, y=3048, yend=3048, size=0.5) +
  annotate("segment", x=3.3, xend=3.3, y=3800, yend=3148, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=3.3, xend=3.3, y=9200, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Do Not Have Minset (66.59%)", x=4.3, y=6670.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=4.2, xend=4.4, y=10000, yend=10000, size=0.5) + 
  annotate("segment", x=4.2, xend=4.4, y=3342, yend=3342, size=0.5) +
  annotate("segment", x=4.3, xend=4.3, y=3900, yend=3442, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=4.3, xend=4.3, y=9400, yend=9900, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) 
  

#png(filename="10000_random_yield.png", type="cairo",units="in", width=9, height=7, res=100)
pdf("min50_min14_m_yield.pdf", width=8, height=6)
#print(yield)
multiplot(yield, duplicates, cols=2)

dev.off()