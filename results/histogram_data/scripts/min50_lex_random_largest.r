#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')

######Drawing the figures for 10000 random selected methods######

random = read.table(file = "../sec4-1/min50_lex.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
random = subset(random, Method_size>0)
#clean the data whose minset size is 0
random_threshed = subset(random,Minset_size>0)

random.size.max = max(random$Method_size)
random.size.min = min(random$Method_size)
random.size.mean = round(mean(random$Method_size),1)
random.size.median = median(random$Method_size)
random.size.mode = names(sort(-table(random$Method_size)))[1]

random_threshed.minsize.max = max(random_threshed$Minset_size)
random_threshed.minsize.min = min(random_threshed$Minset_size)
random_threshed.minsize.mean = round(mean(random_threshed$Minset_size),1)
random_threshed.minsize.median = median(random_threshed$Minset_size)
random_threshed.minsize.mode = names(sort(-table(random_threshed$Minset_size)))[1]

random_threshed.ratio.max = format(round(max(random_threshed$Minset_relative_size),3), nsmall=3)
random_threshed.ratio.min = round(min(random_threshed$Minset_relative_size),4)
random_threshed.ratio.mean = round(mean(random_threshed$Minset_relative_size),3)
random_threshed.ratio.median = round(median(random_threshed$Minset_relative_size), 3)
#random_threshed.ratio.mode = names(sort(-table(random_threshed$Minset_relative_size)))[1]
random_threshed.ratio.mode = 0.071

#histogram of random
random.size = ggplot(random, aes(x = Method_size)) + 
  geom_histogram(aes(fill=..count..), binwidth=10, alpha=0.7, position="identity") + 
  scale_fill_gradient("Count", low = "#122A43", high = "#46AAF6") +
  scale_x_continuous(limits=c(0,4100), breaks=c(0, 1000, 2000, 3000, 4000)) + 
  scale_y_continuous(limits=c(0,3100), breaks=c(0, 1000, 2000, 3000)) + 
  ylab("Count") +  
  theme(plot.margin=unit(c(0.60,0.1,0.1,0), "cm")) +  
  theme(axis.title.x = element_blank()) +
  theme(axis.title.y = element_text(size=13, face="bold", colour="#8C8C8C")) + 
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=11, face="bold")) +
  theme(legend.position = "none") + 
  annotate("segment", x=1600, xend=4000, y=3000, yend=3000, size=0.5, color="#5C5C5C") +
  annotate("segment", x=1600, xend=4000, y=1850, yend=1850, size=0.5, color="#5C5C5C") +
  annotate("segment", x=1600, xend=1600, y=1850, yend=3000, size=0.5, color="#5C5C5C") +
  annotate("segment", x=4000, xend=4000, y=1850, yend=3000, size=0.5, color="#5C5C5C") +
  annotate("text", x=1700, y=2875, label="Min", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=1700, y=2650, label="Mean", size=4.3, hjust=0, color="#5C5C5C") +
  annotate("text", x=1700, y=2425, label="Median", size=4.3, hjust=0, color="#5C5C5C") +
  annotate("text", x=1700, y=2200, label="Max", size=4.3, hjust=0, color="#5C5C5C") +
  annotate("text", x=1700, y=1975, label="Mode", size=4.3, hjust=0, color="#5C5C5C") +
  annotate("text", x=3900, y=2875, label=random.size.min, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=3900, y=2650, label=random.size.mean, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=3900, y=2425, label=random.size.median, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=3900, y=2200, label=random.size.max, size=4.3, hjust=1, color="#5C5C5C") + 
  annotate("text", x=3900, y=1975, label=random.size.mode, size=4.3, hjust=1, color="#5C5C5C") 
  #annotate("text", x=1600, y=3100, label="LEX", size=4.3, hjust=0, vjust=0.3, color="#5C5C5C")  

random.minsize = ggplot(random_threshed, aes(x = Minset_size)) + 
  geom_histogram(aes(fill=..count..),binwidth=1, alpha=0.7, position="identity") +
  scale_fill_gradient("Count", low = "#122A43", high = "#46AAF6") +
  ggtitle("10000 Random Sample Methods (LEX)") + 
  scale_x_continuous(limits=c(0,8.2), breaks=c(0,2,4,6,8)) + 
  scale_y_continuous(limits=c(0,6200), breaks=c(0, 2000, 4000, 6000)) +	
  theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
  theme(plot.title = element_text(size = 11, hjust=0.9, face="bold")) + 
  theme(axis.title=element_blank()) + 
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=11, face="bold")) +
  theme(legend.position = "none") +
  annotate("segment", x=4, xend=8, y=6000, yend=6000, size=0.5, color="#5C5C5C") +
  annotate("segment", x=4, xend=8, y=3700, yend=3700, size=0.5, color="#5C5C5C") +
  annotate("segment", x=4, xend=4, y=3700, yend=6000, size=0.5, color="#5C5C5C") +
  annotate("segment", x=8, xend=8, y=3700, yend=6000, size=0.5, color="#5C5C5C") +
  annotate("text", x=4.2, y=5750, label="Min", size=4.3, hjust=0, color="#5C5C5C") +
  annotate("text", x=4.2, y=5300, label="Mean", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=4.2, y=4850, label="Median", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=4.2, y=4400, label="Max", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=4.2, y=3950, label="Mode", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=7.8, y=5750, label=random_threshed.minsize.min, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=7.8, y=5300, label=random_threshed.minsize.mean, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=7.8, y=4850, label=random_threshed.minsize.median, size=4.3, hjust=1, color="#5C5C5C")+
  annotate("text", x=7.8, y=4400, label=random_threshed.minsize.max, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=7.8, y=3950, label=random_threshed.minsize.mode, size=4.3, hjust=1, color="#5C5C5C")
  #annotate("text", x=4.0, y=6200, label="LEX", size=4.3, hjust=0, vjust=0.3, color="#5C5C5C") 

random.ratio = ggplot(random_threshed, aes(x = Minset_relative_size)) + 
  geom_histogram(aes(fill=..count..),binwidth=0.005, alpha=0.7, position="identity") + 
  scale_fill_gradient("Count", low = "#122A43", high = "#46AAF6") +
  scale_x_continuous(limits=c(0,0.4), breaks=c(0, 0.1, 0.2, 0.3, 0.4)) + 
  scale_y_continuous(limits=c(0,1240), breaks=c(0, 400, 800, 1200)) +
  theme(plot.margin=unit(c(0.60,0.2,0.1,0), "cm")) + 
  theme(axis.title=element_blank()) + 
  theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
  theme(axis.text = element_text(size=11, face="bold")) +
  theme(legend.position = "none") +
  annotate("segment", x=0.16, xend=0.40, y=1200, yend=1200, size=0.5, color="#5C5C5C") +
  annotate("segment", x=0.16, xend=0.40, y=740, yend=740, size=0.5, color="#5C5C5C") +
  annotate("segment", x=0.16, xend=0.16, y=740, yend=1200, size=0.5, color="#5C5C5C") +
  annotate("segment", x=0.40, xend=0.40, y=740, yend=1200, size=0.5, color="#5C5C5C") +
  annotate("text", x=0.17, y=1150, label="Min", size=4.3, hjust=0, color="#5C5C5C") +
  annotate("text", x=0.17, y=1060, label="Mean", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=0.17, y=970, label="Median", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=0.17, y=880, label="Max", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=0.17, y=790, label="Mode", size=4.3, hjust=0, color="#5C5C5C") + 
  annotate("text", x=0.39, y=1150, label=random_threshed.ratio.min, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=0.39, y=1060, label=random_threshed.ratio.mean, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=0.39, y=970, label=random_threshed.ratio.median, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=0.39, y=880, label=random_threshed.ratio.max, size=4.3, hjust=1, color="#5C5C5C") +
  annotate("text", x=0.39, y=790, label=random_threshed.ratio.mode, size=4.3, hjust=1, color="#5C5C5C") 
  #annotate("text", x=0.16, y=1240, label="LEX", size=4.3, hjust=0, vjust=0.3, color="#5C5C5C")


######Drawing the figures for 1000 largest methods######

#read the data
largest = read.table(file = "../sec4-1/min50_lex_largest.sql", 
                  col.name = c("Method_size", "Minset_size", "Minset_relative_size"))

# largest = read.table(file = "largest_largest.sql", 
#                   col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
largest = subset(largest, Method_size>0)
#clean the data whose minset size is 0
largest_threshed = subset(largest,Minset_size>0)

largest.size.max = max(largest$Method_size)
largest.size.min = min(largest$Method_size)
largest.size.mean = round(mean(largest$Method_size),1)
largest.size.median = median(largest$Method_size)
largest.size.mode = names(sort(-table(largest$Method_size)))[1]

largest_threshed.minsize.max = max(largest_threshed$Minset_size)
largest_threshed.minsize.min = min(largest_threshed$Minset_size)
largest_threshed.minsize.mean = round(mean(largest_threshed$Minset_size),1)
largest_threshed.minsize.median = median(largest_threshed$Minset_size)
largest_threshed.minsize.mode = names(sort(-table(largest_threshed$Minset_size)))[1]

largest_threshed.ratio.max = round(max(largest_threshed$Minset_relative_size),3)
largest_threshed.ratio.min = round(min(largest_threshed$Minset_relative_size),4)
#largest_threshed.ratio.min = "1.2e-4"
largest_threshed.ratio.mean = round(mean(largest_threshed$Minset_relative_size),3)
largest_threshed.ratio.median = round(median(largest_threshed$Minset_relative_size), 3)
#largest_threshed.ratio.mode = names(sort(-table(largest_threshed$Minset_relative_size)))[1]
largest_threshed.ratio.mode = names(sort(-table(round(largest_threshed$Minset_relative_size, 4))))[1]
#largest_threshed.ratio.mode = "2.4e-4"


#histogram of largest
largest.size = ggplot(largest, aes(x = Method_size)) + 
			geom_histogram(aes(fill=..count..), binwidth=10, alpha=1, position="identity") + 
      scale_fill_gradient("Count", low = "#122A43", high = "#46AAF6") +
			scale_x_continuous(limits=c(0,8100), breaks=c(0, 2000, 4000, 6000, 8000)) + 
			scale_y_continuous(limits=c(0,31), breaks=c(0, 10, 20, 30)) +  
			xlab("Method Size (Threshed)") + 
			ylab("Count") + 
      theme(plot.margin=unit(c(0.23,0.1,0.1,0), "cm")) + 
      theme(axis.title.x = element_text(size=13, face="bold",vjust=0.2, colour="#8C8C8C")) +
			theme(axis.title.y = element_text(size=13, face="bold", colour="#8C8C8C")) + 
			theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
			theme(axis.text = element_text(size=11, face="bold")) + 
      theme(legend.position = "none") + 
 			annotate("segment", x=3200, xend=8000, y=30, yend=30, size=0.5, color="#5C5C5C") +
			annotate("segment", x=3200, xend=8000, y=18.5, yend=18.5, size=0.5, color="#5C5C5C") +
			annotate("segment", x=3200, xend=3200, y=18.5, yend=30, size=0.5, color="#5C5C5C") +
			annotate("segment", x=8000, xend=8000, y=18.5, yend=30, size=0.5, color="#5C5C5C") +
			annotate("text", x=3400, y=28.75, label="Min", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=3400, y=26.50, label="Mean", size=4.3, hjust=0, color="#5C5C5C") +
			annotate("text", x=3400, y=24.25, label="Median", size=4.3, hjust=0, color="#5C5C5C") +
			annotate("text", x=3400, y=22.00, label="Max", size=4.3, hjust=0, color="#5C5C5C") +
			annotate("text", x=3400, y=19.75, label="Mode", size=4.3, hjust=0, color="#5C5C5C") +
			annotate("text", x=7800, y=28.75, label=largest.size.min, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7800, y=26.50, label=largest.size.mean, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7800, y=24.25, label=largest.size.median, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7800, y=22.00, label=largest.size.max, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7800, y=19.75, label=largest.size.mode, size=4.3, hjust=1, color="#5C5C5C") 
			#annotate("text", x=3200, y=31, label="LEX", size=4.3, hjust=0, vjust=0.3, color="#5C5C5C")  
			
largest.minsize = ggplot(largest_threshed, aes(x = Minset_size)) + 
			geom_histogram(aes(fill=..count..), binwidth=1, alpha=0.7, position="identity") +
      scale_fill_gradient("Count", low = "#122A43", high = "#46AAF6") +
			scale_x_continuous(limits=c(0,8.1), breaks=c(0, 2, 4, 6, 8)) + 
			scale_y_continuous(limits=c(0,930), breaks=c(0, 300, 600, 900)) +			
			ggtitle("1000 Largest Methods (LEX)") + 
			xlab("Minset Size") + 
      theme(plot.margin=unit(c(-0.3,0.1,0.1,0), "cm")) + 
      theme(plot.title = element_text(size=11, hjust=0.5, face="bold")) + 
			theme(axis.title.x = element_text(size=13, face="bold", vjust=0.2, colour="#8C8C8C")) +
			theme(axis.title.y = element_blank()) + 
			theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
			theme(axis.text = element_text(size=11, face="bold")) +
      theme(legend.position = "none") +
			annotate("segment", x=4, xend=8, y=900, yend=900, size=0.5, color="#5C5C5C") +
			annotate("segment", x=4, xend=8, y=555, yend=555, size=0.5, color="#5C5C5C") +
			annotate("segment", x=4, xend=4, y=555, yend=900, size=0.5, color="#5C5C5C") +
			annotate("segment", x=8, xend=8, y=555, yend=900, size=0.5, color="#5C5C5C") +
			annotate("text", x=4.2, y=862.5, label="Min", size=4.3, hjust=0, color="#5C5C5C") +
			annotate("text", x=4.2, y=795.0, label="Mean", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=4.2, y=727.5, label="Median", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=4.2, y=660.0, label="Max", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=4.2, y=592.5, label="Mode", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=7.8, y=862.5, label=largest_threshed.minsize.min, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7.8, y=795.0, label=largest_threshed.minsize.mean, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7.8, y=727.5, label=largest_threshed.minsize.median, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=7.8, y=660.0, label=largest_threshed.minsize.max, size=4.3, hjust=1, color="#5C5C5C") + 
			annotate("text", x=7.8, y=592.5, label=largest_threshed.minsize.mode, size=4.3, hjust=1, color="#5C5C5C") 
			#annotate("text", x=4.0, y=930, label="LEX", size=4.3, hjust=0, vjust=0.3, color="#5C5C5C") 
			
largest.ratio = ggplot(largest_threshed, aes(x = Minset_relative_size)) + 
			geom_histogram(aes(fill=..count..), binwidth=0.001, alpha=0.7, position="identity") + 
      scale_fill_gradient("Count", low = "#122A43", high = "#46AAF6") +
			scale_x_continuous(limits=c(0,0.162), breaks=c(0, 0.04, 0.08, 0.12, 0.16)) + 
			scale_y_continuous(limits=c(0,310), breaks=c(0, 100, 200, 300)) +
			theme(plot.margin=unit(c(0.23,0.2,0.1,0), "cm")) + 
			xlab("Minset Ratio") + 
			theme(axis.title.x = element_text(size=13, face="bold", vjust=0.2, colour="#8C8C8C")) + 
			theme(axis.title.y = element_blank()) + 
			theme(axis.text.y = element_text(angle=90, hjust=0.5)) +
			theme(axis.text = element_text(size=11, face="bold")) +
      theme(legend.position = "none") +
			annotate("segment", x=0.064, xend=0.16, y=300, yend=300, size=0.5, color="#5C5C5C") +
			annotate("segment", x=0.064, xend=0.16, y=185, yend=185, size=0.5, color="#5C5C5C") +
			annotate("segment", x=0.064, xend=0.064, y=185, yend=300, size=0.5, color="#5C5C5C") +
			annotate("segment", x=0.16, xend=0.16, y=185, yend=300, size=0.5, color="#5C5C5C") +
			annotate("text", x=0.068, y=287.5, label="Min", size=4.3, hjust=0, color="#5C5C5C") +
			annotate("text", x=0.068, y=265.0, label="Mean", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=0.068, y=242.5, label="Median", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=0.068, y=220.0, label="Max", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=0.068, y=197.5, label="Mode", size=4.3, hjust=0, color="#5C5C5C") + 
			annotate("text", x=0.156, y=287.5, label=largest_threshed.ratio.min, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=0.156, y=265.0, label=largest_threshed.ratio.mean, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=0.156, y=242.5, label=largest_threshed.ratio.median, size=4.3, hjust=1, color="#5C5C5C") +
			annotate("text", x=0.156, y=220.0, label=largest_threshed.ratio.max, size=4.3, hjust=1, color="#5C5C5C") + 
			annotate("text", x=0.156, y=197.5, label=largest_threshed.ratio.mode, size=4.3, hjust=1, color="#5C5C5C") 
			#annotate("text", x=0.064, y=310, label="LEX", size=4.3, hjust=0, vjust=0.3, color="#5C5C5C")
			
#output the plots
#png(filename="largest_1000_largest.png", type="cairo",units="in", width=14, height=3.5, res=100)
pdf("min50_lex_random_largest.pdf", width=8, height=6)	
#3*1 chart
multiplot(random.size, largest.size, 
          random.minsize, largest.minsize, random.ratio, largest.ratio, cols=3)
#print(largest.minsize)

dev.off()

