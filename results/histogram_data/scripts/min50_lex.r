#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')

#read the data
lex1 = read.table(file = "../sec4-1/min50_lex.sql", col.name = c("Method_size", "Minset_size", "Minset_relative_size"))
lex1 = subset(lex1, Method_size>0)
#clean the data whose minset size is 0
lex1_threshed = subset(lex1,Minset_size>0)

lex1.size.max = max(lex1$Method_size)
lex1.size.min = min(lex1$Method_size)
lex1.size.mean = round(mean(lex1$Method_size),1)
lex1.size.median = median(lex1$Method_size)
lex1.size.mode = names(sort(-table(lex1$Method_size)))[1]

lex1_threshed.minsize.max = max(lex1_threshed$Minset_size)
lex1_threshed.minsize.min = min(lex1_threshed$Minset_size)
lex1_threshed.minsize.mean = round(mean(lex1_threshed$Minset_size),1)
lex1_threshed.minsize.median = median(lex1_threshed$Minset_size)
lex1_threshed.minsize.mode = names(sort(-table(lex1_threshed$Minset_size)))[1]

lex1_threshed.ratio.max = format(round(max(lex1_threshed$Minset_relative_size),3), nsmall=3)
lex1_threshed.ratio.min = round(min(lex1_threshed$Minset_relative_size),4)
lex1_threshed.ratio.mean = round(mean(lex1_threshed$Minset_relative_size),3)
lex1_threshed.ratio.median = round(median(lex1_threshed$Minset_relative_size), 3)
#lex1_threshed.ratio.mode = names(sort(-table(lex1_threshed$Minset_relative_size)))[1]
lex1_threshed.ratio.mode = 0.071

#histogram of lex1
lex1.size = ggplot(lex1, aes(x = Method_size)) + 
	geom_histogram(binwidth=10, alpha=0.7, position="identity") + 
	scale_x_continuous(limits=c(0,4100), breaks=c(0, 1000, 2000, 3000, 4000)) + 
	scale_y_continuous(limits=c(0,3100), breaks=c(0, 1000, 2000, 3000)) + 
	theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) +  
	ylab("Count") + 
  xlab("Method Size (Threshed)") + 
  theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
	theme(axis.title.y = element_text(size=15, face="bold", colour="#8C8C8C")) + 
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=2500, xend=4000, y=3000, yend=3000, size=0.5, color="#404040") +
	annotate("segment", x=2500, xend=4000, y=2000, yend=2000, size=0.5, color="#404040") +
	annotate("segment", x=2500, xend=2500, y=2000, yend=3000, size=0.5, color="#404040") +
	annotate("segment", x=4000, xend=4000, y=2000, yend=3000, size=0.5, color="#404040") +
	annotate("text", x=2600, y=2875, label="Min", size=5, hjust=0, color="#404040") + 
	annotate("text", x=2600, y=2687.5, label="Mean", size=5, hjust=0, color="#404040") +
	annotate("text", x=2600, y=2500, label="Median", size=5, hjust=0, color="#404040") +
	annotate("text", x=2600, y=2312.5, label="Max", size=5, hjust=0, color="#404040") +
	annotate("text", x=2600, y=2125, label="Mode", size=5, hjust=0, color="#404040") +
	annotate("text", x=3900, y=2875, label=lex1.size.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=3900, y=2687.5, label=lex1.size.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=3900, y=2500, label=lex1.size.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=3900, y=2312.5, label=lex1.size.max, size=5, hjust=1, color="#404040") + 
	annotate("text", x=3900, y=2125, label=lex1.size.mode, size=5, hjust=1, color="#404040") +
 	annotate("text", x=2600, y=3100, label="LEX", size=5, hjust=0, vjust=0.5, color="#404040")  
			
lex1.minsize = ggplot(lex1_threshed, aes(x = Minset_size)) + 
	geom_histogram(binwidth=1, alpha=0.7, position="identity") +
	scale_x_continuous(limits=c(0,8.2), breaks=c(0,2,4,6,8)) + 
	scale_y_continuous(limits=c(0,6200), breaks=c(0, 2000, 4000, 6000)) +			
	theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
	theme(axis.title.y=element_blank()) + 
  xlab("Minset Size") + 
  theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=5.0, xend=8, y=6000, yend=6000, size=0.5, color="#404040") +
	annotate("segment", x=5.0, xend=8, y=4000, yend=4000, size=0.5, color="#404040") +
	annotate("segment", x=5.0, xend=5.0, y=4000, yend=6000, size=0.5, color="#404040") +
	annotate("segment", x=8, xend=8, y=4000, yend=6000, size=0.5, color="#404040") +
	annotate("text", x=5.2, y=5750, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=5.2, y=5375, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=5.2, y=5000, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=5.2, y=4625, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=5.2, y=4250, label="Mode", size=5, hjust=0, color="#404040") + 
	annotate("text", x=7.8, y=5750, label=lex1_threshed.minsize.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=7.8, y=5375, label=lex1_threshed.minsize.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=7.8, y=5000, label=lex1_threshed.minsize.median, size=5, hjust=1, color="#404040")+
	annotate("text", x=7.8, y=4625, label=lex1_threshed.minsize.max, size=5, hjust=1, color="#404040") +
	annotate("text", x=7.8, y=4250, label=lex1_threshed.minsize.mode, size=5, hjust=1, color="#404040") +
	annotate("text", x=5.2, y=6200, label="LEX", size=5, hjust=0, vjust=0.5, color="#404040") 
			
lex1.ratio = ggplot(lex1_threshed, aes(x = Minset_relative_size)) + 
	geom_histogram(binwidth=0.005, alpha=0.7, position="identity") + 
	scale_x_continuous(limits=c(0,0.4), breaks=c(0, 0.1, 0.2, 0.3, 0.4)) + 
	scale_y_continuous(limits=c(0,1240), breaks=c(0, 400, 800, 1200)) +
	theme(plot.margin=unit(c(0.1,0.1,0.1,0), "cm")) + 
	theme(axis.title.y=element_blank()) + 
  xlab("Minset Ratio") + 
  theme(axis.title.x = element_text(size=15, face="bold", vjust=0.2, colour="#8C8C8C")) +
	theme(axis.text.y = element_text(angle=90, hjust=0.5)) + 
	theme(axis.text = element_text(size=13, face="bold")) +
	annotate("segment", x=0.25, xend=0.40, y=1200, yend=1200, size=0.5, color="#404040") +
	annotate("segment", x=0.25, xend=0.40, y=800, yend=800, size=0.5, color="#404040") +
	annotate("segment", x=0.25, xend=0.25, y=800, yend=1200, size=0.5, color="#404040") +
	annotate("segment", x=0.40, xend=0.40, y=800, yend=1200, size=0.5, color="#404040") +
	annotate("text", x=0.26, y=1150, label="Min", size=5, hjust=0, color="#404040") +
	annotate("text", x=0.26, y=1075, label="Mean", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.26, y=1000, label="Median", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.26, y=925, label="Max", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.26, y=850, label="Mode", size=5, hjust=0, color="#404040") + 
	annotate("text", x=0.39, y=1150, label=lex1_threshed.ratio.min, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=1075, label=lex1_threshed.ratio.mean, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=1000, label=lex1_threshed.ratio.median, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=925, label=lex1_threshed.ratio.max, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.39, y=850, label=lex1_threshed.ratio.mode, size=5, hjust=1, color="#404040") +
	annotate("text", x=0.26, y=1240, label="LEX", size=5, hjust=0, vjust=0.5, color="#404040")

			
#output the plots
#png(filename="lex1_ltt_10000.png", type="cairo",units="in", width=14, height=7, res=100)		
pdf("min50_lex.pdf", width=14, height=4.5)
#3*2 chart
multiplot(lex1.size, lex1.minsize, lex1.ratio, cols=3)
#print(lex1.minsize)

dev.off()

