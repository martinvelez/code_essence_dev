#set the path of the data
setwd("D:/Working/Papers/code_essence/dev/results/histogram_data/scripts")

library(ggplot2)
library(grid)
library(scales)
source('multiplot.r')


#read the data
yield_data = read.table(file = "../sec4-3/min50_min4_m_filter_yield.txt",
                        col.name = c("Maximum_Method_Size", "Number", "Methods"))
yield_data = transform(yield_data, Maximum_Method_Size = as.character(Maximum_Method_Size))

yield_data$Methods = factor(yield_data$Methods, 
                             levels=c("Have_more_than_10_supersets", "Have_10_or_fewer_supersets", "Have_minsets"))
yield_data$Maximum_Method_Size = factor(yield_data$Maximum_Method_Size, 
                            levels=c("72028", "36104", "18007", "9003", "4501", "2250", "1125", "562", "281", "140", "70"))            

yield = ggplot(yield_data, aes(x=Maximum_Method_Size, y=Number, fill=Methods)) + 
	geom_bar(stat="identity", width=0.4) + 
  ylab("Count") + 
  xlab("Maximum Method Size") + 
  theme_bw() + 
  scale_fill_manual(values=c("#B0E2FF", "#79AEFF", "#04477C"))  + 
  scale_y_continuous(limits=c(0,11500), breaks=c(0, 2500, 5000, 7500, 10000)) +
  theme(legend.position=c(0.5, 0.955), legend.direction="horizontal") +
  theme(plot.margin=unit(c(0.1,0.2,0.2,0.3), "cm")) + 
  theme(panel.border = element_rect(colour = "black")) +
  theme(axis.title.y=element_text(vjust=0.3, size=15, face="bold")) +
  theme(axis.title.x=element_text(vjust=0.2, size=15, face="bold")) +
  theme(axis.text.y=element_text(angle=90, hjust=0.5)) +
  theme(axis.text=element_text(size=12, face="bold")) +
  theme(legend.text=element_text(size=11, face="bold")) + 
  theme(legend.title=element_blank()) + 
  theme(legend.background=element_rect(fill="transparent")) +
  annotate("text", label="Yield (53.63%)", x=1.35, y=2681.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=1.2, xend=1.5, y=5363, yend=5363, size=0.5) + 
  annotate("segment", x=1.2, xend=1.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=1.35, xend=1.35, y=1300, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=1.35, xend=1.35, y=4000, yend=5263, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Yield (53.63%)", x=2.35, y=2681.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=2.2, xend=2.5, y=5363, yend=5363, size=0.5) + 
  annotate("segment", x=2.2, xend=2.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=2.35, xend=2.35, y=1300, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=2.35, xend=2.35, y=4000, yend=5263, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Yield (53.63%)", x=3.35, y=2681.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=3.2, xend=3.5, y=5363, yend=5363, size=0.5) + 
  annotate("segment", x=3.2, xend=3.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=3.35, xend=3.35, y=1300, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=3.35, xend=3.35, y=4000, yend=5150, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Yield (53.70%)", x=4.35, y=2684.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=4.2, xend=4.5, y=5369, yend=5369, size=0.5) + 
  annotate("segment", x=4.2, xend=4.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=4.35, xend=4.35, y=1350, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=4.35, xend=4.35, y=4000, yend=5269, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("text", label="Yield (54.01%)", x=5.35, y=2698, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=5.2, xend=5.5, y=5396, yend=5396, size=0.5) + 
  annotate("segment", x=5.2, xend=5.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=5.35, xend=5.35, y=1400, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=5.35, xend=5.35, y=4050, yend=5296, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Yield (54.99%)", x=6.35, y=2743, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=6.2, xend=6.5, y=5486, yend=5486, size=0.5) + 
  annotate("segment", x=6.2, xend=6.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=6.35, xend=6.35, y=1400, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=6.35, xend=6.35, y=4100, yend=5386, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Yield (57.32%)", x=7.35, y=2840, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=7.2, xend=7.5, y=5680, yend=5680, size=0.5) + 
  annotate("segment", x=7.2, xend=7.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=7.35, xend=7.35, y=1500, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=7.35, xend=7.35, y=4150, yend=5580, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Yield (61.74%)", x=8.35, y=2973, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=8.2, xend=8.5, y=5946, yend=5946, size=0.5) + 
  annotate("segment", x=8.2, xend=8.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=8.35, xend=8.35, y=1600, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=8.35, xend=8.35, y=4300, yend=5846, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Yield (67.10%)", x=9.35, y=2925.5, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=9.2, xend=9.5, y=5851, yend=5851, size=0.5) + 
  annotate("segment", x=9.2, xend=9.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=9.35, xend=9.35, y=1600, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=9.35, xend=9.35, y=4300, yend=5751, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="Yield (73.03%)", x=10.35, y=2357, angle=90, vjust=0.5, size=4.2) + 
  annotate("segment", x=10.2, xend=10.5, y=4714, yend=4714, size=0.5) + 
  annotate("segment", x=10.2, xend=10.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=10.35, xend=10.35, y=1050, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=10.35, xend=10.35, y=3700, yend=4614, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
  annotate("text", label="79.68%", x=11.35, y=955, angle=90, vjust=0.5, size=3.5) + 
  annotate("segment", x=11.2, xend=11.5, y=1910, yend=1910, size=0.5) + 
  annotate("segment", x=11.2, xend=11.5, y=0, yend=0, size=0.5) +
  annotate("segment", x=11.35, xend=11.35, y=350, yend=100, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) + 
  annotate("segment", x=11.35, xend=11.35, y=1550, yend=1810, 
           arrow=arrow(angle=20, length=unit(0.3,"cm"))) +
#png(filename="10000_random_yield.png", type="cairo",units="in", width=9, height=7, res=100)
pdf("min50_min4_m_filter_yield.pdf", width=8, height=6)

print(yield)
#multiplot(yield, duplicates, cols=2)

dev.off()