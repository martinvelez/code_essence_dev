select * from methods where id=7956613;

# open file

# note path and signature

# quickly summarize method purpose

# get minset
select * from minsets_1 where mid=7956613;

# see features
select * from features_1 where id=72107;

# get multiset minset
select * from minsets_101 where mid=7956613; 

# see features
select * from features_101 where id=72107;



# additional inspection if necessary
select fid, feature from (select * from mf_1 where mid=7018018) AS t left outer join features_1 on t.fid=features_1.id order by feature;

select fid, array_agg(mid) as mids from mf_1 where fid IN (select fid from mf_1 where mid=7018018) group by fid;

select fid, size, feature from (select fid, array_length(array_agg(mid),1) as size from mf_1 where fid IN (select fid from mf_1 where mid=7018018) group by fid order by size) AS t left outer join features_1 on t.fid=features_1.id;
