SELECT 'minsets_1';
SELECT count(*) AS nodups_fewss FROM minsets_1 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_1 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_1 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_1 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_1 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_2';
SELECT count(*) AS nodups_fewss FROM minsets_2 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_2 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_2 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_2 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_2 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_3';
SELECT count(*) AS nodups_fewss FROM minsets_3 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_3 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_3 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_3 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_3 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_4';
SELECT count(*) AS nodups_fewss FROM minsets_4 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_4 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_4 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_4 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_4 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_5';
SELECT count(*) AS nodups_fewss FROM minsets_5 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_5 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_5 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_5 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_5 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_6';
SELECT count(*) AS nodups_fewss FROM minsets_6 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_6 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_6 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_6 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_6 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_7';
SELECT count(*) AS nodups_fewss FROM minsets_7 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_7 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_7 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_7 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_7 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_8';
SELECT count(*) AS nodups_fewss FROM minsets_8 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_8 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_8 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_8 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_8 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_9';
SELECT count(*) AS nodups_fewss FROM minsets_9 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_9 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_9 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_9 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_9 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_10';
SELECT count(*) AS nodups_fewss FROM minsets_10 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_10 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_10 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_10 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_10 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_101';
SELECT count(*) AS nodups_fewss FROM minsets_101 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_101 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_101 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_101 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_101 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_102';
SELECT count(*) AS nodups_fewss FROM minsets_102 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_102 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_102 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_102 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_102 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_103';
SELECT count(*) AS nodups_fewss FROM minsets_103 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_103 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_103 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_103 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_103 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_104';
SELECT count(*) AS nodups_fewss FROM minsets_104 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_104 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_104 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_104 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_104 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_105';
SELECT count(*) AS nodups_fewss FROM minsets_105 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_105 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_105 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_105 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_105 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_106';
SELECT count(*) AS nodups_fewss FROM minsets_106 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_106 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_106 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_106 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_106 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_107';
SELECT count(*) AS nodups_fewss FROM minsets_107 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_107 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_107 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_107 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_107 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_108';
SELECT count(*) AS nodups_fewss FROM minsets_108 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_108 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_108 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_108 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_108 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_109';
SELECT count(*) AS nodups_fewss FROM minsets_109 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_109 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_109 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_109 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_109 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_110';
SELECT count(*) AS nodups_fewss FROM minsets_110 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_110 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_110 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_110 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_110 where dups_size>0 and strict_supersets_size>10;

SELECT 'minsets_1010';
SELECT count(*) AS nodups_fewss FROM minsets_1010 where dups_size=0 and strict_supersets_size>10;
SELECT count(*) AS nodups_manyss FROM minsets_1010 where dups_size=0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_noss FROM minsets_1010 where dups_size>0 and strict_supersets_size=0;
SELECT count(*) AS dups_fewss FROM minsets_1010 where dups_size>0 and strict_supersets_size>=1 and strict_supersets_size<=10;
SELECT count(*) AS dups_manyss FROM minsets_1010 where dups_size>0 and strict_supersets_size>10;
