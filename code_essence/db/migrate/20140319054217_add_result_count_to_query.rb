class AddResultCountToQuery < ActiveRecord::Migration
  def change
    add_column :queries, :result_count, :integer
  end
end
