class DropDatesFromLexFeatures < ActiveRecord::Migration
  def change
		remove_column :lex_features, :created_at
		remove_column :lex_features, :updated_at
  end
end
