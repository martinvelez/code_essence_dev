class AddIndexToLexFeatures < ActiveRecord::Migration
  def change
		add_index :lex_features, :functions_size
  end
end
