class CreateLexMinsets < ActiveRecord::Migration
  def change
    create_table :lex_minsets do |t|
      t.integer :minset, array: true, default: []
      t.integer :size

      t.timestamps
    end
  end
end
