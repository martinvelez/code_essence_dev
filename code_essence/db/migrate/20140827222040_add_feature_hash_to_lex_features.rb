class AddFeatureHashToLexFeatures < ActiveRecord::Migration
  def change
    add_column :lex_features, :feature_hash, :string
  end
end
