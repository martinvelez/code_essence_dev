class CreateLexFeatures < ActiveRecord::Migration
  def change
    create_table :lex_features do |t|
      t.text :feature

      t.timestamps
    end
  end
end
