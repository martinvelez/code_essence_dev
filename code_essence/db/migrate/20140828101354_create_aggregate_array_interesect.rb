class CreateAggregateArrayInteresect < ActiveRecord::Migration
  def up
		execute "create or replace function array_intersect(a1 int[], a2 int[]) returns int[] as
$$
declare
    ret int[];
begin
    -- The reason for the kludgy NULL handling comes later.
    -- RAISE NOTICE 'a1 = %', a1;
    -- RAISE NOTICE 'a2 = %', a2;
    if a1 is null then
    	-- RAISE NOTICE 'a1 is null';
        return a2;
    -- elseif a2 is null then
    --    RAISE NOTICE 'a2 is null';
    --    return a1;
    end if;
    if array_length(a1,1) = 0 then
    	return '{}'::integer[];
    end if;
    select array_agg(e) into ret
    from (
        select unnest(a1)
        intersect
        select unnest(a2)
    ) as dt(e);
    if ret is null then
    	return '{}'::integer[];
    end if;
    return ret;
end;
$$ language plpgsql;"
  end
	
	def down
		execute "DROP FUNCTION array_intersect"
	end
end
