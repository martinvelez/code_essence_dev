class CreateMin4Features < ActiveRecord::Migration
  def change
    create_table :min4_features do |t|
      t.text :feature
      t.integer :line_start
      t.integer :line_end

      t.timestamps
    end
  end
end
