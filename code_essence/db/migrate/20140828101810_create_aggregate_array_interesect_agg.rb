class CreateAggregateArrayInteresectAgg < ActiveRecord::Migration
  def up 
    execute "CREATE AGGREGATE array_intersect_agg(int[]) (
	sfunc = array_intersect,
	stype = int[]
);"
  end

	def down
		execute "DROP AGGREGATE array_intersect_agg(int[])" 
	end
end
