class AddIndexToFunctionLexFeatures < ActiveRecord::Migration
  def change
		add_index :function_lex_features, :function_id
  end
end
