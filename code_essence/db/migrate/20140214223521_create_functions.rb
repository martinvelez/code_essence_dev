class CreateFunctions < ActiveRecord::Migration
  def change
    create_table :functions do |t|
      t.text :name
      t.integer :java_file_id
      t.integer :start_line
      t.integer :end_line

      t.timestamps
    end
  end
end
