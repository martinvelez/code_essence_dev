class CreateJavaFiles < ActiveRecord::Migration
  def change
    create_table :java_files do |t|
      t.string :location

      t.timestamps
    end
  end
end
