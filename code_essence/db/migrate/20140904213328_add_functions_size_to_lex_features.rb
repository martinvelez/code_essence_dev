class AddFunctionsSizeToLexFeatures < ActiveRecord::Migration
  def change
    add_column :lex_features, :functions_size, :integer
  end
end
