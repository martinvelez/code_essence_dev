class AddIndexToLexFeature < ActiveRecord::Migration
  def up 
		execute "CREATE INDEX feature_idx ON lex_features (md5(feature))"
  end
	
	def down
		execute "DROP INDEX feature_idx" 
  end

end
