class RemoveFeatureIdsFromFunctions < ActiveRecord::Migration
  def change
		remove_column :functions, :feature_ids
  end
end
