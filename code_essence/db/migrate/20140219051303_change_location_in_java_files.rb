class ChangeLocationInJavaFiles < ActiveRecord::Migration
  def change
		change_column :java_files, :location, :text
  end
end
