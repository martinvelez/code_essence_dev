class CreateFunctionLexFeatures < ActiveRecord::Migration
  def change
    create_table :function_lex_features, id: false do |t|
			t.references :function, primary_key: true
			t.integer :feature_ids, array: true, default: []
      #t.timestamps
    end
  end
end
