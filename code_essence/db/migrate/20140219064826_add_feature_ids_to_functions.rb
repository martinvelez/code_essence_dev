class AddFeatureIdsToFunctions < ActiveRecord::Migration
  def change
		add_column :functions, :feature_ids, :integer, array: true, default: []
  end
end
