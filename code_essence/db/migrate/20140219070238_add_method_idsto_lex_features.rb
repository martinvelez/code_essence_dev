class AddMethodIdstoLexFeatures < ActiveRecord::Migration
  def change
		add_column :lex_features, :function_ids, :integer, array: true, default: []
  end
end
