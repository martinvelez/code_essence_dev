class AddIndexToLexMinsets < ActiveRecord::Migration
  def change
		add_index :lex_minsets, :function_id
  end
end
