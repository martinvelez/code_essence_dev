class RemoveLineStartLineEndFromMin4Features < ActiveRecord::Migration
  def change
  	remove_column :min4_features, :line_start, :integer
  	remove_column :min4_features, :line_end, :integer
  end
end
