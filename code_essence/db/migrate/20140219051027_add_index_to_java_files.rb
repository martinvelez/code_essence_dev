class AddIndexToJavaFiles < ActiveRecord::Migration
  def change
		add_index :java_files, :location, :unique => true
  end
end
