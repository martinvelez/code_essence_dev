class RemoveDatesFromFunctions < ActiveRecord::Migration
  def change
		remove_column :functions, :created_at
		remove_column :functions, :updated_at
  end
end
