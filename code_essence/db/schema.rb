# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140905220408) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "fm", id: false, force: true do |t|
    t.integer "feature_id"
    t.integer "function_ids", array: true
  end

  create_table "function_lex_features", id: false, force: true do |t|
    t.integer "function_id"
    t.integer "feature_ids", default: [], array: true
  end

  add_index "function_lex_features", ["function_id"], name: "index_function_lex_features_on_function_id", using: :btree

  create_table "functions", force: true do |t|
    t.text    "name"
    t.integer "java_file_id"
    t.integer "start_line"
    t.integer "end_line"
  end

  create_table "java_files", force: true do |t|
    t.text     "location"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "java_files", ["location"], name: "index_java_files_on_location", unique: true, using: :btree

  create_table "lex_features", force: true do |t|
    t.text    "feature"
    t.integer "function_ids",   default: [], array: true
    t.string  "feature_hash"
    t.integer "functions_size"
  end

  add_index "lex_features", ["functions_size"], name: "index_lex_features_on_functions_size", using: :btree

  create_table "lex_minsets", force: true do |t|
    t.integer  "minset",      default: [], array: true
    t.integer  "size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "function_id"
  end

  add_index "lex_minsets", ["function_id"], name: "index_lex_minsets_on_function_id", using: :btree

  create_table "min4_features", force: true do |t|
    t.text     "feature"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "queries", force: true do |t|
    t.text     "query"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "result_count"
  end

end
