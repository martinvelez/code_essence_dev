require 'test_helper'

class LexFeaturesControllerTest < ActionController::TestCase
  setup do
    @lex_feature = lex_features(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lex_features)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lex_feature" do
    assert_difference('LexFeature.count') do
      post :create, lex_feature: { feature: @lex_feature.feature }
    end

    assert_redirected_to lex_feature_path(assigns(:lex_feature))
  end

  test "should show lex_feature" do
    get :show, id: @lex_feature
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lex_feature
    assert_response :success
  end

  test "should update lex_feature" do
    patch :update, id: @lex_feature, lex_feature: { feature: @lex_feature.feature }
    assert_redirected_to lex_feature_path(assigns(:lex_feature))
  end

  test "should destroy lex_feature" do
    assert_difference('LexFeature.count', -1) do
      delete :destroy, id: @lex_feature
    end

    assert_redirected_to lex_features_path
  end
end
