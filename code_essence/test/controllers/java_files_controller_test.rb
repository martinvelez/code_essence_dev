require 'test_helper'

class JavaFilesControllerTest < ActionController::TestCase
  setup do
    @java_file = java_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:java_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create java_file" do
    assert_difference('JavaFile.count') do
      post :create, java_file: { location: @java_file.location }
    end

    assert_redirected_to java_file_path(assigns(:java_file))
  end

  test "should show java_file" do
    get :show, id: @java_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @java_file
    assert_response :success
  end

  test "should update java_file" do
    patch :update, id: @java_file, java_file: { location: @java_file.location }
    assert_redirected_to java_file_path(assigns(:java_file))
  end

  test "should destroy java_file" do
    assert_difference('JavaFile.count', -1) do
      delete :destroy, id: @java_file
    end

    assert_redirected_to java_files_path
  end
end
