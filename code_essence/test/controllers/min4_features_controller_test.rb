require 'test_helper'

class Min4FeaturesControllerTest < ActionController::TestCase
  setup do
    @min4_feature = min4_features(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:min4_features)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create min4_feature" do
    assert_difference('Min4Feature.count') do
      post :create, min4_feature: { feature: @min4_feature.feature, line_end: @min4_feature.line_end, line_start: @min4_feature.line_start }
    end

    assert_redirected_to min4_feature_path(assigns(:min4_feature))
  end

  test "should show min4_feature" do
    get :show, id: @min4_feature
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @min4_feature
    assert_response :success
  end

  test "should update min4_feature" do
    patch :update, id: @min4_feature, min4_feature: { feature: @min4_feature.feature, line_end: @min4_feature.line_end, line_start: @min4_feature.line_start }
    assert_redirected_to min4_feature_path(assigns(:min4_feature))
  end

  test "should destroy min4_feature" do
    assert_difference('Min4Feature.count', -1) do
      delete :destroy, id: @min4_feature
    end

    assert_redirected_to min4_features_path
  end
end
