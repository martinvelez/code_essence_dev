require 'test_helper'

class FunctionLexFeaturesControllerTest < ActionController::TestCase
  setup do
    @function_lex_feature = function_lex_features(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:function_lex_features)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create function_lex_feature" do
    assert_difference('FunctionLexFeature.count') do
      post :create, function_lex_feature: {  }
    end

    assert_redirected_to function_lex_feature_path(assigns(:function_lex_feature))
  end

  test "should show function_lex_feature" do
    get :show, id: @function_lex_feature
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @function_lex_feature
    assert_response :success
  end

  test "should update function_lex_feature" do
    patch :update, id: @function_lex_feature, function_lex_feature: {  }
    assert_redirected_to function_lex_feature_path(assigns(:function_lex_feature))
  end

  test "should destroy function_lex_feature" do
    assert_difference('FunctionLexFeature.count', -1) do
      delete :destroy, id: @function_lex_feature
    end

    assert_redirected_to function_lex_features_path
  end
end
