require 'test_helper'

class LexMinsetsControllerTest < ActionController::TestCase
  setup do
    @lex_minset = lex_minsets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lex_minsets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lex_minset" do
    assert_difference('LexMinset.count') do
      post :create, lex_minset: { minset: @lex_minset.minset, size: @lex_minset.size }
    end

    assert_redirected_to lex_minset_path(assigns(:lex_minset))
  end

  test "should show lex_minset" do
    get :show, id: @lex_minset
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lex_minset
    assert_response :success
  end

  test "should update lex_minset" do
    patch :update, id: @lex_minset, lex_minset: { minset: @lex_minset.minset, size: @lex_minset.size }
    assert_redirected_to lex_minset_path(assigns(:lex_minset))
  end

  test "should destroy lex_minset" do
    assert_difference('LexMinset.count', -1) do
      delete :destroy, id: @lex_minset
    end

    assert_redirected_to lex_minsets_path
  end
end
