require 'ruby-progressbar'

namespace :lex_features do

	desc 'Update the function_ids array for each lex feature'
	task :update_function_ids => :environment do 
		lex_feature_functions = {}
		function_lex_features = FunctionLexFeature.all
		#puts function_lex_features.size

		pbar = ProgressBar.create(total: function_lex_features.size, format: '%E %a <%B> %p%% %t') 
		function_lex_features.each do |function_lex_feature|	
			function_id = function_lex_feature.function_id
			function_lex_feature.feature_ids.each do |feature_id|
				if lex_feature_functions.has_key?(feature_id)
					lex_feature_functions[feature_id] << feature_id
				else
					lex_feature_functions[feature_id] = [function_id]
				end	
			end
			pbar.increment
		end
		pbar.finish

		pbar = ProgressBar.create(total: function_lex_features.size, format: '%E %a <%B> %p%% %t') 
		#LexFeature.transaction do 
			lex_feature_functions.each do |feature_id, function_ids|
				LexFeature.update(:id => feature_id, :function_ids => functions_ids)
				pbar.increment
			end
		#end # transaction
		pbar.finish
	end # task

end # namespace

