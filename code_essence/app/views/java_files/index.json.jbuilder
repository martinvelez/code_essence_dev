json.array!(@java_files) do |java_file|
  json.extract! java_file, :id, :location
  json.url java_file_url(java_file, format: :json)
end
