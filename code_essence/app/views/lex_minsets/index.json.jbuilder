json.array!(@lex_minsets) do |lex_minset|
  json.extract! lex_minset, :id, :minset, :size
  json.url lex_minset_url(lex_minset, format: :json)
end
