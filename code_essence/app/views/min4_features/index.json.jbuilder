json.array!(@min4_features) do |min4_feature|
  json.extract! min4_feature, :id, :feature, :line_start, :line_end
  json.url min4_feature_url(min4_feature, format: :json)
end
