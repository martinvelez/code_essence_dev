json.array!(@lex_features) do |lex_feature|
  json.extract! lex_feature, :id, :feature
  json.url lex_feature_url(lex_feature, format: :json)
end
