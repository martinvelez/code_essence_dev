json.array!(@function_lex_features) do |function_lex_feature|
  json.extract! function_lex_feature, :id
  json.url function_lex_feature_url(function_lex_feature, format: :json)
end
