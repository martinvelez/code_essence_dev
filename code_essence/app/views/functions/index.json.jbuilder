json.array!(@functions) do |function|
  json.extract! function, :id, :name, :java_file_id, :start_line, :end_line
  json.url function_url(function, format: :json)
end
