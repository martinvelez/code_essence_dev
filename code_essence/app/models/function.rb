class Function < ActiveRecord::Base
	belongs_to :java_file
	has_one :function_lex_feature
	has_many :lex_minset

	self.per_page = 100

end
