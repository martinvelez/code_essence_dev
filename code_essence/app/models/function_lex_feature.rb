class FunctionLexFeature < ActiveRecord::Base
	belongs_to :function
	self.primary_key = :function_id
end
