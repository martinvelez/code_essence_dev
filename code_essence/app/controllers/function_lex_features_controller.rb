class FunctionLexFeaturesController < ApplicationController
  before_action :set_function_lex_feature, only: [:show, :edit, :update, :destroy]

  # GET /function_lex_features
  # GET /function_lex_features.json
  def index
		@search = FunctionLexFeature.search(params[:q])
    @function_lex_features = @search.result.order('function_id').paginate(:page => params[:page])
  end

  # GET /function_lex_features/1
  # GET /function_lex_features/1.json
  def show
		@feature_ids = @function_lex_feature.feature_ids.paginate(:page => params[:page])
  end

  # GET /function_lex_features/new
  def new
    @function_lex_feature = FunctionLexFeature.new
  end

  # GET /function_lex_features/1/edit
  def edit
  end

  # POST /function_lex_features
  # POST /function_lex_features.json
  def create
    @function_lex_feature = FunctionLexFeature.new(function_lex_feature_params)

    respond_to do |format|
      if @function_lex_feature.save
        format.html { redirect_to @function_lex_feature, notice: 'Function lex feature was successfully created.' }
        format.json { render action: 'show', status: :created, location: @function_lex_feature }
      else
        format.html { render action: 'new' }
        format.json { render json: @function_lex_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /function_lex_features/1
  # PATCH/PUT /function_lex_features/1.json
  def update
    respond_to do |format|
      if @function_lex_feature.update(function_lex_feature_params)
        format.html { redirect_to @function_lex_feature, notice: 'Function lex feature was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @function_lex_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /function_lex_features/1
  # DELETE /function_lex_features/1.json
  def destroy
    @function_lex_feature.destroy
    respond_to do |format|
      format.html { redirect_to function_lex_features_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_function_lex_feature
      @function_lex_feature = FunctionLexFeature.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def function_lex_feature_params
      params[:function_lex_feature]
    end
end
