class JavaFilesController < ApplicationController
  before_action :set_java_file, only: [:show, :edit, :update, :destroy]

  # GET /java_files
  # GET /java_files.json
  def index
    @search = JavaFile.search(params[:q])
    @java_files = @search.result.order('id').paginate(:page => params[:page])
  end


  # GET /java_files/1
  # GET /java_files/1.json
  def show
  end

  # GET /java_files/new
  def new
    @java_file = JavaFile.new
  end

  # GET /java_files/1/edit
  def edit
  end

  # POST /java_files
  # POST /java_files.json
  def create
    @java_file = JavaFile.new(java_file_params)

    respond_to do |format|
      if @java_file.save
        format.html { redirect_to @java_file, notice: 'Java file was successfully created.' }
        format.json { render action: 'show', status: :created, location: @java_file }
      else
        format.html { render action: 'new' }
        format.json { render json: @java_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /java_files/1
  # PATCH/PUT /java_files/1.json
  def update
    respond_to do |format|
      if @java_file.update(java_file_params)
        format.html { redirect_to @java_file, notice: 'Java file was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @java_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /java_files/1
  # DELETE /java_files/1.json
  def destroy
    @java_file.destroy
    respond_to do |format|
      format.html { redirect_to java_files_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_java_file
      @java_file = JavaFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def java_file_params
      params.require(:java_file).permit(:location)
    end
end
