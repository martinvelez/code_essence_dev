class Min4FeaturesController < ApplicationController
  before_action :set_min4_feature, only: [:show, :edit, :update, :destroy]

  # GET /min4_features
  # GET /min4_features.json
  def index
    @search = Min4Feature.search(params[:q])
    @min4_features = @search.result.order('id').paginate(:page => params[:page])
  end

  # GET /min4_features/1
  # GET /min4_features/1.json
  def show
  end

  # GET /min4_features/new
  def new
    @min4_feature = Min4Feature.new
  end

  # GET /min4_features/1/edit
  def edit
  end

  # POST /min4_features
  # POST /min4_features.json
  def create
    @min4_feature = Min4Feature.new(min4_feature_params)

    respond_to do |format|
      if @min4_feature.save
        format.html { redirect_to @min4_feature, notice: 'Min4 feature was successfully created.' }
        format.json { render action: 'show', status: :created, location: @min4_feature }
      else
        format.html { render action: 'new' }
        format.json { render json: @min4_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /min4_features/1
  # PATCH/PUT /min4_features/1.json
  def update
    respond_to do |format|
      if @min4_feature.update(min4_feature_params)
        format.html { redirect_to @min4_feature, notice: 'Min4 feature was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @min4_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /min4_features/1
  # DELETE /min4_features/1.json
  def destroy
    @min4_feature.destroy
    respond_to do |format|
      format.html { redirect_to min4_features_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_min4_feature
      @min4_feature = Min4Feature.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def min4_feature_params
      params.require(:min4_feature).permit(:feature, :line_start, :line_end)
    end
end
