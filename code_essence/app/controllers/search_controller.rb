require 'digest/md5'

class SearchController < ApplicationController
  def index
		per_page = 10
		@query = tokenize(params[:features]) # parse query string into

		@bad_features = []
		
		feature_hashes_str = query_to_hash(@query)
		@feature_ids = analyze(@query, feature_hashes_str)
		@method_ids, @methods, @result_count = *retrieve_functions(feature_hashes_str)
		# What if methods are empty?
		if @method_ids.size == 0
					
		end
		@java_files = memoize_files(@methods)
		@methods = memoize_minsets(@method_ids, @methods)
		@features = memoize_features(@methods)
		@methods = set_function_features(@method_ids, @methods) 
		@methods = set_jaccard_indices(@feature_ids, @methods)		
		@methods = Hash[@methods.sort_by { |k,v| [-1 * v["jaccard_minset"], -1 * v["jaccard_method"]] }]
		@method_ids = @methods.keys
		# Store Query
		@method_ids = @method_ids.paginate(:page => params[:page], :per_page => per_page) 
		Query.create(:query => params[:features], :result_count => @result_count)	
		if @result_count > 100
			flash.now[:notice] = "There are #{@result_count} methods matching your query. We display only 100."
		end
	end


	def get_file_contents
		location = params["location"]
		start_line = params["start_line"].to_i
		end_line = params["end_line"].to_i
		contents = File.readlines(location)[(start_line-1)..(end_line-1)].join	
		render :json => CodeRay.scan(contents, :java).div(:line_numbers => :table, :line_number_start => start_line).to_json	
	end


	def tokenize(str)
			tokens = []	
			tokens = str.split(/\s(?=(?:[^"]|"[^"]*")*$)/)	
			return tokens
	end


	def jaccard_index(a, b)
		intersection_size = (a & b).size
		if intersection_size == 0
			return 0.0
		else
			union_size = (a | b).size
			return (intersection_size.to_f / union_size) * 100.0	
		end
	end


	def query_to_hash(query)
		feature_hashes = []
		@query.each do |keyword|
			feature_hashes << Digest::MD5.hexdigest(keyword)	
		end
		feature_hashes_str = feature_hashes.inspect.gsub('[','').gsub(']','').gsub('"',"'")
	end


	def analyze(query, feature_hashes_str)
		intended_features = []
		keywords = rank_keywords(query)
		intended_features = abstract_keywords(keywords)

		feature_ids = LexFeature.where("md5(feature) IN (#{feature_hashes_str})").pluck(:id)

		return feature_ids
	end


	def rank_keywords(keywords)
		# TODO: 
		# Rank by prevalence or rarity
		# Rank by analogizing to part-of-speech
		return keywords
	end


	def abstract_keywords(keywords)
		# TODO:
		# attempt to map each keyword to its word in MIN4
		return keywords
	end


	def retrieve_functions(feature_hashes_str)
		result_count = 0
		methods = {}
		method_ids = LexFeature.where("md5(feature) IN (#{feature_hashes_str})").pluck("array_intersect_agg(function_ids)")[0]

		if !method_ids.nil?
			puts "@method_ids.size = #{method_ids.size}"
			result_count = method_ids.size
			method_ids = method_ids.first(100)# Too many methods?
		else
			method_ids = []
		end

		functions = Function.where(:id => method_ids)
		functions.each do |function|
			methods[function.id] = {}
			methods[function.id]["function"] = function 
		end

		return method_ids, methods, result_count	
	end


	def memoize_files(methods)
		java_files = {}
		java_file_ids = []
		#puts methods.inspect
		methods.each do |id, h|
			java_file_ids << h["function"].java_file_id
		end
		JavaFile.where(:id => java_file_ids).each do |java_file|
			java_files[java_file.id] = java_file.location	
		end
		return java_files
	end


	def memoize_minsets(method_ids, methods)
		minsets = LexMinset.where(:function_id => method_ids).select(:function_id, :minset)
		minsets.each do |minset|
			methods[minset.function_id]["minset"] = minset.minset
		end
		return methods
	end


	def memoize_features(methods)
		features = {}
		feature_ids = []
		methods.each do |id, h|
			if !h["minset"].nil?
				feature_ids.concat(h["minset"])
			end
		end
		LexFeature.where(:id => feature_ids).select(:id, :feature).each do |feature|
			features[feature.id] = feature.feature 		
		end
		return features
	end


	def set_function_features(method_ids, methods)
		FunctionLexFeature.where(:function_id => method_ids).select(:function_id, :feature_ids).each do |f_l_f|
			methods[f_l_f.function_id]["set"] = f_l_f.feature_ids
		end
		return methods
	end
	

	def set_jaccard_indices(feature_ids, methods)
		methods.each do |id, hash| 
			methods[id]["jaccard_minset"] = -100.0	
			methods[id]["jaccard_method"] = -100.0	
			if !methods[id]["minset"].nil?
				methods[id]["jaccard_minset"] = jaccard_index(feature_ids, methods[id]["minset"])
			end
			methods[id]["jaccard_method"] = jaccard_index(feature_ids, methods[id]["set"])
		end
		return methods
	end

	def get_example_queries
		queries = Query.uniq.limit(20).where("result_count > 0").pluck(:query)
		render :json => queries.shuffle.to_json
	end

	def get_random_query
		query = ''
		num_of_words = Random.new.rand(1..5)
		result = FunctionLexFeature.order("RANDOM()").limit(1).pluck(:feature_ids)
		if !result.nil?
			feature_ids = result[0].sample(num_of_words)
			features = LexFeature.where(:id => feature_ids).pluck(:feature)
			query = features.join(' ')		
		end
		render :json => query.to_json
	end

	def get_suggestions
		letter = params[:input]
		set = LexFeature.where("functions_size > ?", 3).first(1000)

		#Rails.logger.debug(set)

		render :json => set.to_json
	end
end
