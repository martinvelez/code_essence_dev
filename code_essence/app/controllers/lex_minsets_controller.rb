class LexMinsetsController < ApplicationController
  before_action :set_lex_minset, only: [:show, :edit, :update, :destroy]

  # GET /lex_minsets
  # GET /lex_minsets.json
  def index
		@search = LexMinset.search(params[:q])
    @lex_minsets = @search.result.order('function_id').paginate(:page => params[:page])
  end

  # GET /lex_minsets/1
  # GET /lex_minsets/1.json
  def show
  end

  # GET /lex_minsets/new
  def new
    @lex_minset = LexMinset.new
  end

  # GET /lex_minsets/1/edit
  def edit
  end

  # POST /lex_minsets
  # POST /lex_minsets.json
  def create
    @lex_minset = LexMinset.new(lex_minset_params)

    respond_to do |format|
      if @lex_minset.save
        format.html { redirect_to @lex_minset, notice: 'Lex minset was successfully created.' }
        format.json { render action: 'show', status: :created, location: @lex_minset }
      else
        format.html { render action: 'new' }
        format.json { render json: @lex_minset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lex_minsets/1
  # PATCH/PUT /lex_minsets/1.json
  def update
    respond_to do |format|
      if @lex_minset.update(lex_minset_params)
        format.html { redirect_to @lex_minset, notice: 'Lex minset was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lex_minset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lex_minsets/1
  # DELETE /lex_minsets/1.json
  def destroy
    @lex_minset.destroy
    respond_to do |format|
      format.html { redirect_to lex_minsets_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lex_minset
      @lex_minset = LexMinset.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lex_minset_params
      params.require(:lex_minset).permit(:function_id, :minset, :size)
    end
end
