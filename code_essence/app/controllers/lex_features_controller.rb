class LexFeaturesController < ApplicationController
  before_action :set_lex_feature, only: [:show, :edit, :update, :destroy]

  # GET /lex_features
  # GET /lex_features.json
  def index
    @search = LexFeature.search(params[:q])
    @lex_features = @search.result.order('id').paginate(:page => params[:page])
  end

  # GET /lex_features/1
  # GET /lex_features/1.json
  def show
		@function_ids = @lex_feature.function_ids.sort.paginate(:page => params[:page], :per_page => 1000)
  end
	
  # GET /lex_features/new
  def new
    @lex_feature = LexFeature.new
  end

  # GET /lex_features/1/edit
  def edit
  end

  # POST /lex_features
  # POST /lex_features.json
  def create
    @lex_feature = LexFeature.new(lex_feature_params)

    respond_to do |format|
      if @lex_feature.save
        format.html { redirect_to @lex_feature, notice: 'Lex feature was successfully created.' }
        format.json { render action: 'show', status: :created, location: @lex_feature }
      else
        format.html { render action: 'new' }
        format.json { render json: @lex_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lex_features/1
  # PATCH/PUT /lex_features/1.json
  def update
    respond_to do |format|
      if @lex_feature.update(lex_feature_params)
        format.html { redirect_to @lex_feature, notice: 'Lex feature was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lex_feature.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lex_features/1
  # DELETE /lex_features/1.json
  def destroy
    @lex_feature.destroy
    respond_to do |format|
      format.html { redirect_to lex_features_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lex_feature
      @lex_feature = LexFeature.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lex_feature_params
      params.require(:lex_feature).permit(:feature)
    end
end
