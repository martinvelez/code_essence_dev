require 'open3'
require 'csv'

class SummarizerController < ApplicationController
	SUMMARIZER_DIR = "public/summarizer"


	# Given the source code of a Java method, this action should extract features
	# from the given method, and return the minset 
	# The Javascript callback method should then do the job of updating the
	# summary
  def summarize 
		minset = []
		lexicon = 'LEX' # TODO: handle more lexicons
		projdirname = create_project(params[:source_code])
		fe_status = run_feature_extractor("#{SUMMARIZER_DIR}/#{projdirname}") 
		if fe_status.success?
			# get features from MethodFeature.csv					
			features = parse_mf_file(projdirname, lexicon)
			feature_ids = []
			features.each do |feature|
				if lexicon == 'LEX'
					fid = LexFeature.find_by(feature: feature).id
					# if feature does not exists in DB, then it is unique to this method
					if fid.nil? 
						puts "LexFeature ID was not found for: #{feature}"
						render :json => [feature] 	
					else
						feature_ids << fid	
					end
				else
					#TODO: more lexicons
				end
			end

			puts feature_ids.inspect
			# The given method does not contain any unique features.  All features were
			# found in DB.  So now compute the minset using the feature_ids.
			minset = minset(feature_ids) 

		else
			#TODO: what if FE fails?
		end

		render :json => minset
	end


	def create_project(source_code)
		# for now, assume that the user input is a method wrapped in a CodeEssence 
		# class
		# create project folder
		projdirname = "project_#{Time.now.to_i}"
		projdirpath = "#{SUMMARIZER_DIR}/#{projdirname}"
		Dir.mkdir(projdirpath) unless File.exists?(projdirpath)
		# write source code to file, put file in project folder
		File.open("#{projdirpath}/CodeEssence.java", "w") do |f| 
			f.write(source_code)
		end

		return projdirname 
	end


	def run_feature_extractor(projdirpath)
		cmd = "java -jar #{SUMMARIZER_DIR}/FeatureExtractor.jar \
			#{SUMMARIZER_DIR}/conf/config.properties \
			#{projdirpath} \
			#{projdirpath}/output"
		stdout, stderr, status = Open3.capture3(cmd)
		return status
	end


	def parse_mf_file(projdirname, lexicon)
		features = []
		outputdir = "#{SUMMARIZER_DIR}/#{projdirname}/output/#{projdirname}"
		CSV.foreach("#{outputdir}/MethodFeature.csv") do |row|
			if lexicon == 'LEX'
				if body_feature?(row)
					feature = get_lex_feature(row)
					features.append(feature)
				end
			else 
				#TODO
			end
		end
		return features
	end

	def get_lex_feature(row)
		puts row.inspect
		return row[2]
	end


	def body_feature?(row)
		return row[1] == "1"
	end


	def minset(fids)
		minset = []
		fm = {}	
		fids.each do |fid|	
					
		end
		return minset
	end
	
end
