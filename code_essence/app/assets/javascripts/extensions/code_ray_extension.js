
//	*** Adds line number toggling as well as highlighting to the CodeRay plugin ***

var CodeRayExtender = function(config) {
	var obj = this, config;
	var start_block, end_block;

/* -------------------
 *	Constructor()
 * -------------------
 */

	this.construct = function(config) {
		// *** base-id will be appended with an index to obtain the coderay container ***
		
		if(config != undefined) obj.config = config;
		else obj.config = {
			'base-id' : 'file-content-',
			'search-keys' : $('#search-input').find('input').attr('value'),
			'min-line-shown' : 4,
			'options' : {
				'query-highlight' : {
					'on' : true,
					'color' : 'lightblue'
				},
				'minset-highlight' : {
					'on' : true,
					'color' : 'yellow'
				}
			}
		}

		obj.start_block = Math.floor(obj.config['min-line-shown'] / 2);
		obj.end_block = Math.floor(obj.config['min-line-shown'] / 2);
		if(obj.config['min-line-shown'] % 2 == 1) obj.start_block++;
	}
	this.construct(config);

	this.show_file_contents = function(index, file_path, start_line, end_line) {
		if($('#' + obj.config['base-id'] + index).is(':empty')) {
			var path = 'http://jarvis.cs.ucdavis.edu/code_essence/search/get_file_contents';

			if(location.hostname === 'localhost') path = "http://localhost:3000/search/get_file_contents";
			var params = "?location=" + file_path + "&start_line=" + start_line + "&end_line=" + end_line;

			// Call the backend for file contents
			$.get(path + params).success(function(data) {
				obj.file_contents_callback(data, index);
			})
		}
	}

	this.file_contents_callback = function(data, index) {
		var minset_toggle_ele = '<div id="minset_toggle" style="float:right; margin:0 10px 5px 0;">' + 
		'<span style="padding:5px;">minset</span>' + 
		'<button class="btn btn-default" style="width: 25px;font-size:10px; padding:2px;">' + 
		'<span class="glyphicon glyphicon-chevron-right"></span></button></div>';

		var $result = $('#' + obj.config['base-id'] + index);

		$result.html(data);
		$result.prepend(minset_toggle_ele);

		var $line_num_pre = $result.find('.line-numbers').find('pre');
		var min_line_num = this.get_min_line_num($result, index);
		var min_code = this.get_min_code($result);

		$line_num_pre.html(min_line_num);
		$line_num_pre.css('overflow-x', 'hidden');

		if(min_code == undefined) {
			if(obj.config['options']['minset-highlight']['on']) obj.showMinset(index);
			if(obj.config['options']['query-highlight']['on']) obj.showUserQuery(index, obj.config['search-keys'].split(' '));
		}
		else $result.find('.code').find('pre').html(min_code);
		
		var content = {
			'full' : data,
			'min-line' : min_line_num,
			'min-code' : min_code
		}

		obj.codeExpandListener(index, content);	
	}

/* -----------------------------------------------------------------
 *	Gets the minimized line numbers from CodeRay plugin element
 * -----------------------------------------------------------------
 */

	this.get_min_line_num = function($target, result_index) {
		var $line_num = $target.find('.line-numbers').find('a');
		var code_len = $line_num.length;
		var min_line_num = "", i = 0;

		if(code_len - 1 <= obj.config['min-line-shown']) return;

		$line_num.each(function() {
			var expand_ele = '<button id="expand_' + result_index + '" class="btn btn-default expand_code"' +
				'style="font-size:9px; padding:1px 3px; border-radius:1px;">' +
				'<span class="glyphicon glyphicon-plus"></span></button>\n';

			if(i < obj.start_block || i > code_len - 1 - obj.end_block) min_line_num = min_line_num + $(this)[0].outerHTML + '\n';
			if(i == obj.start_block) min_line_num = min_line_num + expand_ele;					
			i++;
		});
		
		return min_line_num;
	}

/* -------------------------------------------------------
 *	Gets the minimized code from CodeRay plugin element
 * -------------------------------------------------------
 */

	this.get_min_code = function($target) {
		var	$code = $target.find('.code');
		var min_code = $code.find('pre').html();
		var tmp = min_code, line_to_hide = 0;
		var $line_num = $target.find('.line-numbers').find('a');
		var code_len = $line_num.length;

		if(code_len - 1 <= obj.config['min-line-shown']) return;
	
		for(var n = 0; n < obj.start_block; n++) line_to_hide = min_code.indexOf('\n', line_to_hide + 1);
		min_code = min_code.substr(0, line_to_hide);
		for(var n = 0; n < obj.end_block + 1; n++) {
			line_to_hide = tmp.lastIndexOf('\n');
			tmp = tmp.substr(0, line_to_hide - 1);
		} 
		
		var tok_ar = $code.find('pre').html().substr(line_to_hide).split('\n');
		var trailing_code = "";

		for(var n = 1; n < obj.end_block + 1; n++) trailing_code = trailing_code + '\n' + tok_ar[n];
		
		return min_code + '\n\t...' + trailing_code;
	}

	this.showMinset = function(index) {
		var minset = [];
		var $button = $('#' + obj.config['base-id'] + index).find('.line-numbers').find('button#expand_' + index).trigger('click');
		var code = $('#' + obj.config['base-id'] + index).find('.code').find('pre').html();

		$('div#minset-' + index).find('a').each(function() {
			minset.push($(this).text());
		});		
		
		//Generate new code
		code = code.substr(0, code.indexOf('\n')) + obj.highlightCode(code.substr(code.indexOf('\n')), minset, {
			'color' : obj.config['options']['minset-highlight']['color']
		})
		
		//Apply new code
		$('#' + obj.config['base-id'] + index).find('.code').find('pre').html(code)
	}

	this.showUserQuery = function(index, query_ar) {
		$('#' + obj.config['base-id'] + index).each(function() {
			var $code = $(this).find('.code').find('pre');
			var code = $code.html();
			
			//Generate new code
			code = code.substr(0, code.indexOf('\n')) + obj.highlightCode(code.substr(code.indexOf('\n')), query_ar, {
				'color' : obj.config['options']['query-highlight']['color'],
			});

			//Apply new code
			$('#' + obj.config['base-id'] + index).find('.code').find('pre').html(code)
		});

	}

	this.highlightCode = function(code, keys, options) {
		if(code == undefined || keys == undefined) return;
		if(options == undefined) var options = { 'color' : 'yellow'};

	//Replace html tags with markers
		var tag_ar = code.match(/<.*?>/g);
		var pure_code = '', marker = ' ##### ';
		var ele_index = 0;

		for(var i in tag_ar) {
			var ele_index = code.indexOf(tag_ar[i], ele_index);
			var comp1 = code.substr(0, ele_index);
			var comp3 = code.substr(ele_index + tag_ar[i].length);
	
			code = comp1 + marker  + comp3;
		}

	//Highlight the keys 	
		for(var i in keys) {
			var node_text = keys[i];
			var expression = '';	

			//Construct regex
			for(var i in node_text) {
				if(i == node_text.length - 1) expression = expression + obj.htmlspecialchars(node_text[i]);
				else expression = expression + obj.htmlspecialchars(node_text[i]) + '(' + marker + '){0,2}'; 
			}

			//Find element location and length
			ele_index = code.search(expression);

			while(ele_index != -1) {
				var ele_length = code.match(expression)[0].length;

				//Reconstruct the code
				var comp1 = code.substr(0, ele_index);
				var tok_ar = code.substr(ele_index, ele_length).split(/ /);
				var comp2 = '';
				
				//console.log(tok_ar)
				
				for(var i in tok_ar) {
					var tok = tok_ar[i];
					if(tok !== marker.trim()) comp2 = comp2 + '<span style="background:' + options['color'] + ';">' + tok + '</span>';
					else comp2 = comp2 + marker;
					
					//console.log(comp2)
				}

				var comp3 = code.substr(ele_index + ele_length);
				
				/*
				console.log('Ele index: ' + ele_index);	
				console.log(code.match(expression))
				console.log('Start: ' + code.match(/ ##### /g).length );
				console.log(code.substr(ele_index, ele_length));
				*/
				
				code = comp1 + comp2 + comp3;
				
				/*
				console.log('Comp1: ' + comp1.match(/ ##### /g).length );
				if(comp2.match(/ ##### /g) != undefined)console.log('Comp2: ' + comp2.match(/ ##### /g).length );
				console.log('Comp3: ' + comp3.match(/ ##### /g).length );

				console.log('End: ' + code.match(/ ##### /g).length );
				*/

				ele_index = comp3.search(expression);
				if(ele_index == -1) break;
				else ele_index = comp1.length + comp2.length + ele_index;
			}
		}

	//Replace markers with html tags
		var i = 0;
		ele_index = code.indexOf(marker);
	
		while(ele_index != -1) {
			var comp1 = code.substr(0, ele_index);
			var comp2 = tag_ar[i];
			var comp3 = code.substr(ele_index + marker.length);
			
			code = comp1 + comp2 + comp3;
			//console.log(comp1)
			//console.log(comp2)

			ele_index = comp3.indexOf(marker);
			if(ele_index == -1) break;
			else ele_index = comp1.length + comp2.length + ele_index;
			i++;
		}
		
		/*
		console.log(i)
		console.log(tag_ar.length)
		*/

		return code;
	}

	this.htmlspecialchars = function(c) {
		var c_ar = {
			'<' : '&lt;',
			'>' : '&gt;',
		}

		if(c_ar[c] == undefined) return c;
		else return c_ar[c]; 
	}

/* ---------------
 *	Listeners
 * ---------------
 */

	this.codeExpandListener = function(index, content) {
		$('button#expand_' + index).on('click', function(event) {
			var $target = $('#' + obj.config['base-id'] + index);
			var collapse_ele = '<button id="collapse_' + index + '" class="btn btn-default"' +
				'style="margin-top:2px; font-size:9px; padding:1px 3px; border-radius:1px;">' +
				'<span class="glyphicon glyphicon-minus"></span></button>\n';
			
			$target.html(content['full']);
			$target.find('pre').css('overflow-x', 'hidden');
			$('<br>' + collapse_ele).insertAfter($target.find('pre a').last());
			
			if(obj.config['options']['minset-highlight']['on']) obj.showMinset(index);
			if(obj.config['options']['query-highlight']['on']) obj.showUserQuery(index, obj.config['search-keys'].split(' '));
			obj.codeCollapseListener(index, content);
		});
	}

	this.codeCollapseListener = function(index, content) {
		$('button#collapse_' + index).on('click', function(event) {
			event.preventDefault();
		
			var $target = $('#' + obj.config['base-id'] + index);
			$target.find('pre').css('overflow-x', 'hidden');
			$target.find('.line-numbers').find('pre').html(content['min-line']);
			$target.find('.code').find('pre').html(content['min-code']);

			obj.codeExpandListener(index, content);
		});
	}
}
